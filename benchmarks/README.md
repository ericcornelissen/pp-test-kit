<!-- SPDX-License-Identifier: CC-BY-4.0 -->

# `pp-test-kit` Benchmarks

This folder contains benchmarks of the `pp-test-kit` against related work and
similar tools. There are benchmarks available for:

- [`pp-test-kit` v. GHunter](./ghunter/README.md)
- [`pp-test-kit` v. Silent Spring](./silentspring/README.md)
