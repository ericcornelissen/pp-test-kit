// SPDX-License-Identifier: ISC

import * as assert from "node:assert/strict";
import * as child_process from "node:child_process";
import * as path from "node:path";
import * as process from "node:process";
import * as url from "node:url";

import * as fc from "fast-check";
import * as ppTestKit from "pp-test-kit/manual";

const errors = [];
const arbitrary = {
	/**
	 * The options arbitrary generates options objects for all child_process APIs
	 * so the record properties are an intersection of each API's options.
	 *
	 * @param {boolean} all Require all options to be configured.
	 * @returns {import('fast-check').Arbitrary} A fast-check arbitrary.
	 */
	options: (all) =>
		fc.record(
			{
				argv0: fc.string(),
				cwd: fc.constant("."),
				detached: fc.boolean(),
				encoding: fc.constantFrom("buffer", "utf-8", "utf8"),
				env: fc.dictionary(fc.string(), fc.string()),
				gid: fc.integer({ min: 0, max: 65536 }),
				input: fc.string(),
				killSignal: fc.integer({ min: 1, max: 31 }),
				maxBuffer: fc.integer({ min: 0 }),
				serialization: fc.constantFrom("advanced", "json"),
				shell: fc.string(),
				signal: fc.constant(new AbortController()),
				silent: fc.boolean(),
				stdio: fc.oneof(
					// NOTE: the 'overlapped' value is omitted because it causes an error
					// in V8 when used by 'execSync'.
					// See: https://github.com/nodejs/node/issues/48476
					fc.constantFrom("ignore", "inherit", "pipe"),
					fc.array(
						fc.constantFrom(
							"ignore",
							"inherit",
							"ipc",
							"pipe",
							null,
							undefined,
							0,
							1,
							2,
						),
						{ minLength: 3, maxLength: 5 },
					),
				),
				timeout: fc.integer({ min: 0 }),
				uid: fc.integer({ min: 0, max: 65536 }),
				windowsHide: fc.boolean(),
				windowsVerbatimArguments: fc.boolean(),
			},
			{
				requiredKeys: all ? undefined : [],
			},
		),
};

{
	const got = process.version;
	const want = "v16.13.1";
	assert.equal(
		got,
		want,
		`The Node.js version must be ${want} (got ${got}) because that's what Silent Spring used`,
	);
}

console.log("child_process.exec");
{
	let error = null;

	const command = "echo 'Hello world!'";

	try {
		await fc.assert(
			fc.asyncProperty(
				arbitrary.options().map((options) => ppTestKit.wrap(options)),
				(options) =>
					new Promise((resolve, reject) => {
						try {
							child_process.exec(command, options, () => {
								try {
									ppTestKit.check(options);
									resolve();
								} catch (error) {
									reject(error);
								}
							});
						} catch (_) {
							ppTestKit.check(options);
							resolve();
						}
					}),
			),
		);
	} catch (err) {
		error = err;
		errors.push({ api: "exec", error });
	}

	console.log(" ", error === null ? "PASS" : "FAIL");
}

console.log("child_process.execFile");
{
	let error = null;

	const command = "echo";
	const args = ["Hello world!"];

	try {
		await fc.assert(
			fc.asyncProperty(
				arbitrary.options().map((options) => ppTestKit.wrap(options)),
				(options) =>
					new Promise((resolve, reject) => {
						try {
							child_process.execFile(command, args, options, () => {
								try {
									ppTestKit.check(options);
									resolve();
								} catch (error) {
									reject(error);
								}
							});
						} catch (_) {
							ppTestKit.check(options);
							resolve();
						}
					}),
			),
		);
	} catch (err) {
		error = err;
		errors.push({ api: "execFile", error });
	}

	console.log(" ", error === null ? "PASS" : "FAIL");
}

console.log("child_process.execFileSync");
{
	let error = null;

	const command = "echo";
	const args = ["Hello world!"];

	try {
		fc.assert(
			fc.property(
				arbitrary.options().map((options) => ppTestKit.wrap(options)),
				(options) => {
					try {
						child_process.execFileSync(command, args, options);
					} catch (_) {}
					ppTestKit.check(options);
				},
			),
		);
	} catch (err) {
		error = err;
		errors.push({ api: "execFileSync", error });
	}

	console.log(" ", error === null ? "PASS" : "FAIL");
}

console.log("child_process.execFileSync (all props)");
{
	let error = null;

	const command = "echo";
	const args = ["Hello world!"];

	try {
		fc.assert(
			fc.property(
				arbitrary.options(true).map((options) => ppTestKit.wrap(options)),
				(options) => {
					try {
						child_process.execFileSync(command, args, options);
					} catch (_) {}
					ppTestKit.check(options);
				},
			),
		);
	} catch (err) {
		error = err;
		errors.push({ api: "execFileSync (all props)", error });
	}

	console.log(" ", error === null ? "PASS" : "FAIL");
}

console.log("child_process.execSync");
{
	let error = null;

	const command = "echo 'Hello world!'";

	try {
		fc.assert(
			fc.property(
				arbitrary.options().map((options) => ppTestKit.wrap(options)),
				(options) => {
					try {
						child_process.execSync(command, options);
					} catch (_) {}
					ppTestKit.check(options);
				},
			),
		);
	} catch (err) {
		error = err;
		errors.push({ api: "execSync", error });
	}

	console.log(" ", error === null ? "PASS" : "FAIL");
}

console.log("child_process.fork");
{
	let error = null;

	const command = `${path.dirname(url.fileURLToPath(import.meta.url))}/fork.js`;
	const args = [];

	try {
		await fc.assert(
			fc.asyncProperty(
				arbitrary.options().map((options) => ppTestKit.wrap(options)),
				(options) =>
					new Promise((resolve, reject) => {
						try {
							const child = child_process.fork(command, args, options);
							child.on("exit", () => {
								try {
									ppTestKit.check(options);
									resolve();
								} catch (error) {
									reject(error);
								}
							});
						} catch (_) {
							ppTestKit.check(options);
							resolve();
						}
					}),
			),
		);
	} catch (err) {
		error = err;
		errors.push({ api: "fork", error });
	}

	console.log(" ", error === null ? "PASS" : "FAIL");
}

console.log("child_process.spawn");
{
	let error = null;

	const command = "echo";
	const args = ["Hello world!"];

	try {
		await fc.assert(
			fc.asyncProperty(
				arbitrary.options().map((options) => ppTestKit.wrap(options)),
				(options) =>
					new Promise((resolve, reject) => {
						try {
							const child = child_process.spawn(command, args, options);
							child.on("exit", () => {
								try {
									ppTestKit.check(options);
									resolve();
								} catch (error) {
									reject(error);
								}
							});
						} catch (_) {
							ppTestKit.check(options);
							resolve();
						}
					}),
			),
		);
	} catch (err) {
		error = err;
		errors.push({ api: "spawn", error });
	}

	console.log(" ", error === null ? "PASS" : "FAIL");
}

console.log("child_process.spawn (all props)");
{
	let error = null;

	const command = "echo";
	const args = ["Hello world!"];

	try {
		await fc.assert(
			fc.asyncProperty(
				arbitrary
					.options({ requiredKeys: undefined })
					.map((options) => ppTestKit.wrap(options)),
				(options) =>
					new Promise((resolve, reject) => {
						try {
							const child = child_process.spawn(command, args, options);
							child.on("exit", () => {
								try {
									ppTestKit.check(options);
									resolve();
								} catch (error) {
									reject(error);
								}
							});
						} catch (_) {
							ppTestKit.check(options);
							resolve();
						}
					}),
			),
		);
	} catch (err) {
		error = err;
		errors.push({ api: "spawn (all props)", error });
	}

	console.log(" ", error === null ? "PASS" : "FAIL");
}

console.log("child_process.spawnSync");
{
	let error = null;

	const cmd = "echo";
	const args = ["Hello world!"];

	try {
		fc.assert(
			fc.property(
				arbitrary.options().map((options) => ppTestKit.wrap(options)),
				(options) => {
					try {
						child_process.spawnSync(cmd, args, options);
					} catch (_) {}
					ppTestKit.check(options);
				},
			),
		);
	} catch (err) {
		error = err;
		errors.push({ api: "spawnSync", error });
	}

	console.log(" ", error === null ? "PASS" : "FAIL");
}

console.log("child_process.spawnSync (all props)");
{
	let error = null;

	const cmd = "echo";
	const args = ["Hello world!"];

	try {
		fc.assert(
			fc.property(
				arbitrary
					.options({ requiredKeys: undefined })
					.map((options) => ppTestKit.wrap(options)),
				(options) => {
					try {
						child_process.spawnSync(cmd, args, options);
					} catch (_) {}
					ppTestKit.check(options);
				},
			),
		);
	} catch (err) {
		error = err;
		errors.push({ api: "spawnSync (all props)", error });
	}

	console.log(" ", error === null ? "PASS" : "FAIL");
}

console.log("\n\n\n\n");
for (const { api, error } of errors) {
	console.log("====", "API:", api, "====");
	console.log(error);
	console.log("\n\n");
}
