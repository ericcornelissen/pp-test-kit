<!-- SPDX-License-Identifier: CC-BY-SA-4.0 -->

# `pp-test-kit` v. Silent Spring

This document provides a comparison of gadget detection capabilities between
`pp-test-kit` and [Silent Spring: Prototype Pollution Leads to Remote Code
Execution in Node.js] on the basis of gadgets identified by Silent Spring. We
compare if the tools finds the property used in the gadgets presented by Silent
Spring.

[silent spring: prototype pollution leads to remote code execution in node.js]: https://www.usenix.org/conference/usenixsecurity23/presentation/shcherbakov

## Experiments

### Requirements

- Node.js v16.13.1;
- pp-test-kit v0.4.0;
- Requirements for the [Silent Spring artifact];

[silent spring artifact]: https://github.com/KTH-LangSec/silent-spring

### Running

To obtain the results for `pp-test-kit` run the test files in this directory.
For example, in this directory run:

```shell
docker build --file Containerfile --tag pp-test-kit-silent-spring .
docker run -it --rm --name pp-test-kit-silent-spring pp-test-kit-silent-spring
$ node child_process.test.js
$ node vm.test.js
```

This will output the test results to stdout. Each `==== API: [...] ====` section
lists an assertion error that reports the number of detected properties followed
by a newline-separated list of detected properties. The list of properties is
the basis for the comparison presented. Note that if there is no section for a
given API it means there were 0 properties detected for it. Note: a JavaScript
error may be reported early, if this happens re-run the test until it does not.

For the result for Silent Spring we re-use the results from the [GHunter:
Universal Prototype Pollution Gadgets in JavaScript Runtimes] paper, refer to
the [silentspring4ghunter] repo for more details.

[ghunter: universal prototype pollution gadgets in javascript runtimes]: https://www.usenix.org/conference/usenixsecurity24/presentation/cornelissen
[silentspring4ghunter]: https://github.com/KTH-LangSec/silentspring4ghunter/tree/comparison-node-16

## Comparison

### By Property

| API                          | Property            | `pp-test-kit` | Silent Spring |
| ---------------------------- | ------------------- | ------------- | ------------- |
| `child_process.exec`         | `NODE_OPTIONS`      | Yes           | No            |
|                              | `shell`             | No (\*)       | Yes           |
| `child_process.execFile`     | `NODE_OPTIONS`      | Yes           | No            |
| `child_process.execFileSync` | `env`               | Yes           | Yes           |
|                              | `input`             | No (\*)       | Yes           |
|                              | `NODE_OPTIONS`      | Yes           | No            |
|                              | `shell`             | Yes           | Yes           |
| `child_process.execSync`     | `env`               | No (\*)       | Yes           |
|                              | `input`             | No (\*)       | Yes           |
|                              | `NODE_OPTIONS`      | Yes           | No            |
|                              | `shell`             | No (\*)       | Yes           |
| `child_process.fork`         | `env`               | No (\*)       | Yes           |
|                              | `NODE_OPTIONS`      | Yes           | No            |
| `child_process.spawn`        | `env`               | Yes           | Yes           |
|                              | `NODE_OPTIONS`      | Yes           | No            |
|                              | `shell`             | Yes           | Yes           |
| `child_process.spawnSync`    | `env`               | Yes           | Yes           |
|                              | `input`             | No (\*)       | Yes           |
|                              | `NODE_OPTIONS`      | Yes           | No            |
|                              | `shell`             | Yes           | Yes           |
| `import`                     | `main`              | No (\*\*)     | No            |
| `require`                    | `1`                 | No (\*\*)     | No            |
|                              | `exports`           | No (\*\*)     | No            |
|                              | `main`              | No (\*\*)     | Yes           |
| `vm.compileFunction`         | `contextExtensions` | Yes           | Yes           |

- (\*): Not detected because a [shallow copy of the options] is created before
  any property access occurs.
- (\*\*): Not detected because the prototype lookup does not occur on an object
  provided in the API.

[shallow copy of the options]: https://github.com/nodejs/node/blob/12029ce9e7f2f157c1a8ca60a7eab1c9c68aaa12/lib/child_process.js#L190-L192

### Precision and Recall

#### `pp-test-kit`

| API                          | Reported | True Positives | False Positives | False Negatives | Precision | Recall |     F1 |
| ---------------------------- | -------: | -------------: | --------------: | --------------: | --------: | -----: | -----: |
| `child_process.exec`         |      `1` |            `1` |             `0` |             `1` |    `1.00` | `0.50` | `0.67` |
| `child_process.execFile`     |      `1` |            `1` |             `0` |             `0` |    `1.00` | `1.00` | `1.00` |
| `child_process.execFileSync` |     `14` |            `3` |            `11` |             `1` |    `0.21` | `0.75` | `0.33` |
| `child_process.execSync`     |      `1` |            `1` |             `0` |             `3` |    `1.00` | `0.25` | `0.40` |
| `child_process.fork`         |      `1` |            `1` |             `0` |             `1` |    `1.00` | `0.50` | `0.67` |
| `child_process.spawn`        |     `14` |            `3` |            `11` |             `0` |    `0.21` | `1.00` | `0.35` |
| `child_process.spawnSync`    |     `14` |            `3` |            `11` |             `1` |    `0.21` | `0.75` | `0.33` |
| `import`                     |      `0` |            `0` |             `0` |             `1` |         - |      - |      - |
| `require`                    |      `0` |            `0` |             `0` |             `3` |         - |      - |      - |
| `vm.compileFunction`         |      `8` |            `1` |             `7` |             `0` |    `0.13` | `1.00` | `0.22` |
| **Total**                    |     `54` |           `14` |            `40` |            `11` |    `0.26` | `0.56` | `0.35` |

#### Silent Spring

| API                          | Reported | True Positives | False Positives | False Negatives | Precision | Recall |     F1 |
| ---------------------------- | -------: | -------------: | --------------: | --------------: | --------: | -----: | -----: |
| `child_process.exec`         |     `20` |            `1` |            `19` |             `1` |    `0.05` | `0.50` | `0.09` |
| `child_process.execFile`     |     `16` |            `0` |            `16` |             `1` |         - |      - |      - |
| `child_process.execFileSync` |     `21` |            `3` |            `18` |             `1` |    `0.14` | `0.75` | `0.24` |
| `child_process.execSync`     |     `13` |            `3` |            `10` |             `1` |    `0.23` | `0.75` | `0.35` |
| `child_process.fork`         |     `25` |            `1` |            `24` |             `1` |    `0.04` | `0.50` | `0.07` |
| `child_process.spawn`        |     `14` |            `2` |            `12` |             `1` |    `0.14` | `0.67` | `0.24` |
| `child_process.spawnSync`    |     `11` |            `3` |             `8` |             `1` |    `0.27` | `0.75` | `0.40` |
| `import`                     |      `0` |            `0` |             `0` |             `1` |         - |      - |      - |
| `require`                    |     `19` |            `2` |            `17` |             `1` |    `0.11` | `0.67` | `0.18` |
| `vm.compileFunction`         |      `4` |            `1` |             `3` |             `0` |    `0.25` | `1.00` | `0.40` |
| **Total**                    |    `143` |           `16` |           `127` |             `9` |    `0.11` | `0.64` | `0.19` |

## Notes

- Since the `import` and `require` APIs don't accept objects there are no
  `pp-test-kit` tests for these.
- True Negatives are not reported because we don't know what the negatives are.
- Accuracy is not reported because we don't know the True Negatives.
