// SPDX-License-Identifier: ISC

import * as assert from "node:assert/strict";
import * as process from "node:process";
import * as vm from "node:vm";

import * as fc from "fast-check";
import * as ppTestKit from "pp-test-kit/manual";

const errors = [];
const arbitrary = {
	/**
	 * The options arbitrary generates options objects for the compileFunction API
	 * of the 'node:vm' module.
	 *
	 * @returns {import('fast-check').Arbitrary} A fast-check arbitrary.
	 */
	options: () =>
		fc.record(
			{
				columnOffset: fc.integer({ min: 1 }),
				filename: fc.string(),
				lineOffset: fc.integer({ min: 1 }),
				produceCachedData: fc.boolean(),
			},
			{
				requiredKeys: [],
			},
		),
};

{
	const got = process.version;
	const want = "v16.13.1";
	assert.equal(
		got,
		want,
		`The Node.js version must be ${want} (got ${got}) because that's what Silent Spring used`,
	);
}

console.log("vm.compileFunction");
{
	let error = null;

	const code = "console.log('Hello world!')";
	const params = [];

	try {
		fc.assert(
			fc.property(
				arbitrary.options().map((options) => ppTestKit.wrap(options)),
				(options) => {
					vm.compileFunction(code, params, options);
					ppTestKit.check(options);
				},
			),
		);
	} catch (err) {
		error = err;
		errors.push({ api: "compileFunction", error });
	}

	console.log(" ", error === null ? "PASS" : "FAIL");
}

console.log("\n\n\n\n");
for (const { api, error } of errors) {
	console.log("====", "API:", api, "====");
	console.log(error);
	console.log("\n\n");
}
