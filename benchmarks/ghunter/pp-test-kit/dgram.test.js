// SPDX-License-Identifier: ISC

import * as assert from "node:assert/strict";
import * as dgram from "node:dgram";
import * as process from "node:process";
import { test } from "node:test";

import * as fc from "fast-check";
import * as ppTestKit from "pp-test-kit/manual";

const arbitrary = {
	/**
	 * The options arbitrary generates options objects for the createSocket
	 * function of the `node:dgram` module.
	 *
	 * @returns {import('fast-check').Arbitrary} A fast-check arbitrary.
	 */
	options: () =>
		fc.record(
			{
				type: fc.constantFrom("udp4", "udp6"),
				reuseAddr: fc.boolean(),
				ipv6Only: fc.boolean(),
				recvBufferSize: fc.integer({ min: 0 }),
				sendBufferSize: fc.integer({ min: 0 }),
			},
			{ requiredKeys: ["type"] },
		),
};

{
	const got = process.version;
	const want = "v21.0.0";
	assert.equal(
		got,
		want,
		`The Node.js version must be ${want} (got ${got}) because that's what GHunter used`,
	);
}

test("dgram.Socket.send", () => {
	fc.assert(
		fc.property(arbitrary.options(), fc.string(), (options, message) => {
			options = ppTestKit.wrap(options);
			const socket = dgram.createSocket(options);
			socket.send(message, 0, message.length, 1337);
			socket.close();
			ppTestKit.check(options);
		}),
	);
});
