// SPDX-License-Identifier: ISC

import * as assert from "node:assert/strict";
import * as fs from "node:fs";
import * as process from "node:process";
import { test } from "node:test";

import * as fc from "fast-check";
import * as ppTestKit from "pp-test-kit/manual";

const arbitrary = {
	/**
	 * The options arbitrary generates options objects for the createWriteStream
	 * function of the `node:fs` module.
	 *
	 * @returns {import('fast-check').Arbitrary} A fast-check arbitrary.
	 */
	options: () =>
		fc.record(
			{
				flags: fc.constantFrom(
					"a",
					"ax",
					"a+",
					"ax+",
					"as",
					"as+",
					"r",
					"rs",
					"r+",
					"rs+",
					"w",
					"wx",
					"w+",
					"wx+",
				),
				encoding: fc.constantFrom("buffer", "utf-8", "utf8"),
				mode: fc.integer({ min: 0o000, max: 0o777 }),
				autoClose: fc.boolean(),
				emitClose: fc.boolean(),
				start: fc.integer(),
				signal: fc.constant(new AbortController()),
				flush: fc.boolean(),
			},
			{ requiredKeys: [] },
		),
};

{
	const got = process.version;
	const want = "v21.0.0";
	assert.equal(
		got,
		want,
		`The Node.js version must be ${want} (got ${got}) because that's what GHunter used`,
	);
}

test("fs.createWriteStream", () => {
	fc.assert(
		fc.property(arbitrary.options(), (options) => {
			options = ppTestKit.wrap(options);
			fs.createWriteStream("./fs.test.txt", options);
			ppTestKit.check(options);
		}),
	);
});
