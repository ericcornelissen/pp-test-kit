// SPDX-License-Identifier: ISC

import * as assert from "node:assert/strict";
import * as buffer from "node:buffer";
import * as process from "node:process";
import { test } from "node:test";
import * as zlib from "node:zlib";

import * as fc from "fast-check";
import * as ppTestKit from "pp-test-kit/manual";

const arbitrary = {
	/**
	 * The options arbitrary generates options objects for zlib. See
	 * <https://nodejs.org/docs/latest-v21.x/api/zlib.html#class-options>.
	 *
	 * @returns {import('fast-check').Arbitrary} A fast-check arbitrary.
	 */
	options: () =>
		fc.record(
			{
				flush: fc.constantFrom(
					zlib.constants.Z_NO_FLUSH,
					zlib.constants.Z_FULL_FLUSH,
					zlib.constants.Z_SYNC_FLUSH,
					zlib.constants.Z_PARTIAL_FLUSH,
				),
				finishFlush: fc.constantFrom(zlib.constants.Z_FINISH),
				chunkSize: fc.integer({ min: 64, max: 32768 }),
				windowBits: fc.integer({ min: 9, max: 15 }),
				level: fc.constantFrom(
					zlib.constants.Z_NO_COMPRESSION,
					zlib.constants.Z_BEST_SPEED,
					zlib.constants.Z_BEST_COMPRESSION,
					zlib.constants.Z_DEFAULT_COMPRESSION,
				),
				memLevel: fc.integer({ min: 1, max: 9 }),
				strategy: fc.constantFrom(
					zlib.constants.Z_FILTERED,
					zlib.constants.Z_HUFFMAN_ONLY,
					zlib.constants.Z_RLE,
					zlib.constants.Z_FIXED,
					zlib.constants.Z_DEFAULT_STRATEGY,
				),
				info: fc.boolean(),
				maxOutputLength: fc.integer({
					min: 1,
					max: buffer.constants.MAX_LENGTH,
				}),
			},
			{
				requiredKeys: [],
			},
		),
};

{
	const got = process.version;
	const want = "v21.0.0";
	assert.equal(
		got,
		want,
		`The Node.js version must be ${want} (got ${got}) because that's what GHunter used`,
	);
}

test("zlib.createGzip().write", () => {
	fc.assert(
		fc.property(arbitrary.options(), fc.string(), (options, chunk) => {
			options = ppTestKit.wrap(options);
			zlib.createGzip(options).write(chunk);
			ppTestKit.check(options);
		}),
	);
});
