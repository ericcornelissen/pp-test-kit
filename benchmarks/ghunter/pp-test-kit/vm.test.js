// SPDX-License-Identifier: ISC

import * as assert from "node:assert/strict";
import * as process from "node:process";
import { test } from "node:test";
import * as vm from "node:vm";

import * as fc from "fast-check";
import * as ppTestKit from "pp-test-kit/manual";

const arbitrary = {
	/**
	 * The options arbitrary generates options objects for the SyntheticModule API
	 * of the 'node:vm' module.
	 *
	 * @returns {import('fast-check').Arbitrary} A fast-check arbitrary.
	 */
	options: () =>
		fc.record(
			{
				identifier: fc.string(),
				context: fc
					.tuple(
						fc.object(),
						fc.record(
							{
								name: fc.string(),
								origin: fc.string(),
								codeGeneration: fc.record(
									{
										strings: fc.boolean(),
										wasm: fc.boolean(),
									},
									{
										requiredKeys: [],
									},
								),
								microtaskMode: fc.constant("afterEvaluate"),
								importModuleDynamically: fc.constant(() => {}),
							},
							{
								requiredKeys: [],
							},
						),
					)
					.map(([contextObject, options]) =>
						vm.createContext(contextObject, options),
					),
			},
			{
				requiredKeys: [],
			},
		),
};

{
	const got = process.version;
	const want = "v21.0.0";
	assert.equal(
		got,
		want,
		`The Node.js version must be ${want} (got ${got}) because that's what GHunter used`,
	);
}

test("vm.SyntheticModule", () => {
	fc.assert(
		fc.property(
			fc.array(fc.string()),
			arbitrary.options().map((options) => ppTestKit.wrap(options)),
			(exportNames, options) => {
				new vm.SyntheticModule(exportNames, () => {}, options);
				ppTestKit.check(options);
			},
		),
	);
});
