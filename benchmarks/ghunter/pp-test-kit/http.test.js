// SPDX-License-Identifier: ISC

import * as assert from "node:assert/strict";
import * as http from "node:http";
import * as process from "node:process";
import { test } from "node:test";

import * as fc from "fast-check";
import * as ppTestKit from "pp-test-kit/manual";

const port = 1337;
const url = `http://localhost:${port}/`;

const arbitrary = {
	/**
	 * The options arbitrary generates options objects the `listen` API of the
	 * HTTP server.
	 *
	 * @returns {import('fast-check').Arbitrary} A fast-check arbitrary.
	 */
	listenOptions: () =>
		fc.record(
			{
				backlog: fc.integer(),
				exclusive: fc.boolean(),
				host: fc.string(),
				ipv6Only: fc.boolean(),
				port: arbitrary.port(),
				readableAll: fc.boolean(),
				signal: fc.constant(new AbortController()),
				writableAll: fc.boolean(),
			},
			{ requiredKeys: ["port"] },
		),

	/**
	 * The options arbitrary generates options objects for the request function
	 * (and related functions) of the `node:http` module.
	 *
	 * Various fields are omitted because they interfere with the test.
	 *
	 * @returns {import('fast-check').Arbitrary} A fast-check arbitrary.
	 */
	requestOptions: () =>
		fc.record(
			{
				agent: fc.constantFrom(false, undefined),
				defaultPort: arbitrary.port(),
				family: fc.constantFrom(4, 6),
				insecureHTTPParser: fc.boolean(),
				joinDuplicateHeaders: fc.boolean(),
				localAddress: fc.string(),
				path: fc.string(),
				protocol: fc.constantFrom("http:"),
				setHost: fc.boolean(),
				timeout: fc.integer({ min: 1 }),
				uniqueHeaders: fc.uniqueArray(fc.string()),
			},
			{ requiredKeys: [] },
		),

	/**
	 * The port arbitrary generates valid networking port values.
	 *
	 * @returns {import('fast-check').Arbitrary} A fast-check arbitrary.
	 */
	port: () => fc.integer({ min: 0, max: 65536 }),
};

{
	const got = process.version;
	const want = "v21.0.0";
	assert.equal(
		got,
		want,
		`The Node.js version must be ${want} (got ${got}) because that's what GHunter used`,
	);
}

{
	const server = http.createServer((_, res) => res.end());
	server.listen(port);
	setTimeout(() => server.close(), 1000);
}

test("http.get", async () => {
	await fc.assert(
		fc.asyncProperty(
			arbitrary.requestOptions().map(ppTestKit.wrap),
			(options) =>
				new Promise((resolve, reject) => {
					try {
						http.get(url, options, () => {
							try {
								ppTestKit.check(options);
								resolve();
							} catch (error) {
								reject(error);
							}
						});
					} catch (_) {
						ppTestKit.check(options);
						resolve();
					}
				}),
		),
	);
});

test("http.request", async () => {
	await fc.assert(
		fc.asyncProperty(
			arbitrary.requestOptions().map(ppTestKit.wrap),
			(options) =>
				new Promise((resolve, reject) => {
					try {
						const request = http.request(url, options, () => {
							try {
								ppTestKit.check(options);
								resolve();
							} catch (error) {
								reject(error);
							}
						});
						request.end();
					} catch (_) {
						ppTestKit.check(options);
						resolve();
					}
				}),
		),
	);
});

test("http.Server.listen", () => {
	fc.assert(
		fc.property(arbitrary.listenOptions().map(ppTestKit.wrap), (options) => {
			const server = http.createServer({}, () => {});
			server.listen(options);
			server.close();
			ppTestKit.check(options);
		}),
	);
});
