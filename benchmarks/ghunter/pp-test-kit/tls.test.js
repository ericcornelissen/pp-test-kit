// SPDX-License-Identifier: ISC

import * as assert from "node:assert/strict";
import * as net from "node:net";
import * as process from "node:process";
import { test } from "node:test";
import * as tls from "node:tls";

import * as fc from "fast-check";
import * as ppTestKit from "pp-test-kit/manual";

// const port = 1337;

const arbitrary = {
	/**
	 * The options arbitrary generates options objects for the TLSSocket.connect
	 * function of the `node:tls` module.
	 *
	 * Various fields are omitted because they interfere with the test.
	 *
	 * @returns {import('fast-check').Arbitrary} A fast-check arbitrary.
	 */
	options: () =>
		fc.record(
			{
				localAddress: fc.string(),
				noDelay: fc.boolean(),
				keepAlive: fc.boolean(),
				keepAliveInitialDelay: fc.integer({ min: 0 }),
				port: arbitrary.port(),
			},
			{
				requiredKeys: ["port"],
			},
		),

	/**
	 * The port arbitrary generates valid networking port values.
	 *
	 * @returns {import('fast-check').Arbitrary} A fast-check arbitrary.
	 */
	port: () => fc.integer({ min: 0, max: 65536 }),
};

{
	const got = process.version;
	const want = "v21.0.0";
	assert.equal(
		got,
		want,
		`The Node.js version must be ${want} (got ${got}) because that's what GHunter used`,
	);
}

test("tls.TLSSocket.connect", async () => {
	await fc.assert(
		fc.asyncProperty(arbitrary.options(), async (options) => {
			options = ppTestKit.wrap(options);
			const socket = new tls.TLSSocket(new net.Socket(), {});
			socket.connect(options);
			ppTestKit.check(options);
		}),
	);
});
