// SPDX-License-Identifier: ISC

import * as assert from "node:assert/strict";
import * as crypto from "node:crypto";
import * as process from "node:process";
import { test } from "node:test";

import * as fc from "fast-check";
import * as ppTestKit from "pp-test-kit/manual";

const arbitrary = {
	/**
	 * The `keyPair` arbitrary generates key pairs using
	 * `crypto.generateKeyPairSync`.
	 *
	 * @returns {import('fast-check').Arbitrary} A fast-check arbitrary.
	 */
	keyPair: () => fc.oneof(arbitrary.rsaKeyPair(), arbitrary.rsaPssKeyPair()),

	/**
	 * The `privateKey` arbitrary generates private keys using
	 * `crypto.generateKeyPairSync`.
	 *
	 * @returns {import('fast-check').Arbitrary} A fast-check arbitrary.
	 */
	privateKey: () => arbitrary.keyPair().map(({ privateKey }) => privateKey),

	/**
	 * The `privateKeyCreateOptions` arbitrary generates options for the
	 * `crypto.createPrivateKey` API.
	 *
	 * @returns {import('fast-check').Arbitrary} A fast-check arbitrary.
	 */
	privateKeyCreateOptions: () =>
		fc.record(
			{
				key: arbitrary.privateKey(),
				format: fc.constantFrom("pem", "der", "jwk"),
				type: fc.constantFrom("pkcs1", "pkcs8", "sec1"),
				passphrase: fc.string(),
				encoding: fc.constantFrom("buffer", "utf-8", "utf8"),
			},
			{
				requiredKeys: ["key"],
			},
		),

	/**
	 * The `publicKeyExportOptions` arbitrary generates options for the
	 * `PrivateKeyObject#export` API.
	 *
	 * @returns {import('fast-check').Arbitrary} A fast-check arbitrary.
	 */
	privateKeyExportOptions: () =>
		fc.record(
			{
				type: fc.constantFrom("pkcs1", "pkcs8", "sec1"),
				format: fc.constantFrom("pem", "der", "jwk"),
				cipher: fc.constantFrom(null, "aes-128-ccm", "aes-128-cbc"),
				passphrase: fc.string(),
			},
			{
				requiredKeys: ["type", "format", "cipher"],
			},
		),

	/**
	 * The `publicKey` arbitrary generates public keys using
	 * `crypto.generateKeyPairSync`.
	 *
	 * @returns {import('fast-check').Arbitrary} A fast-check arbitrary.
	 */
	publicKey: () => arbitrary.keyPair().map(({ publicKey }) => publicKey),

	/**
	 * The `publicKeyCreateOptions` arbitrary generates options for the
	 * `crypto.createPublicKey` API.
	 *
	 * @returns {import('fast-check').Arbitrary} A fast-check arbitrary.
	 */
	publicKeyCreateOptions: () =>
		fc.record(
			{
				key: arbitrary.publicKey(),
				format: fc.constantFrom("pem", "der", "jwk"),
				type: fc.constantFrom("pkcs1", "spki"),
				encoding: fc.constantFrom("buffer", "utf-8", "utf8"),
			},
			{
				requiredKeys: ["key", "type"],
			},
		),

	/**
	 * The `publicKeyExportOptions` arbitrary generates options for the
	 * `PublicKeyObject#export` API.
	 *
	 * @returns {import('fast-check').Arbitrary} A fast-check arbitrary.
	 */
	publicKeyExportOptions: () =>
		fc.record(
			{
				type: fc.constantFrom("pkcs1", "spki"),
				format: fc.constantFrom("pem", "der", "jwk"),
			},
			{
				requiredKeys: ["type", "format"],
			},
		),

	/**
	 * The `rsaKeyPair` arbitrary generates RSA key pairs using
	 * `crypto.generateKeyPairSync`.
	 *
	 * @returns {import('fast-check').Arbitrary} A fast-check arbitrary.
	 */
	rsaKeyPair: () =>
		fc
			.record({
				type: fc.constantFrom("rsa"),
				options: fc.record(
					{
						modulusLength: arbitrary.rsaModulesLength(),
						publicExponent: fc.constantFrom(0x10001),
						publicKeyEncoding: fc.record(
							{
								type: fc.constantFrom("pkcs1", "spki"),
								format: fc.constantFrom("pem", "der", "jwk"),
							},
							{
								requiredKeys: ["type", "format"],
							},
						),
						privateKeyEncoding: fc.record(
							{
								type: fc.constantFrom("pkcs1", "pkcs8"),
								format: fc.constantFrom("pem", "der", "jwk"),
							},
							{
								requiredKeys: ["type", "format"],
							},
						),
					},
					{
						requiredKeys: ["modulusLength"],
					},
				),
			})
			.map(({ type, options }) => crypto.generateKeyPairSync(type, options)),

	/**
	 * The `rsaModulesLength` arbitrary generates valid RSA key moduli for
	 * `crypto.generateKeyPairSync`.
	 *
	 * @returns {import('fast-check').Arbitrary} A fast-check arbitrary.
	 */
	rsaModulesLength: () => fc.integer({ min: 1024, max: 4096 }),

	/**
	 * The `rsaPssKeyPair` arbitrary generates RSA-PSS key pairs using
	 * `crypto.generateKeyPairSync`.
	 *
	 * @returns {import('fast-check').Arbitrary} A fast-check arbitrary.
	 */
	rsaPssKeyPair: () =>
		fc
			.record({
				type: fc.constantFrom("rsa-pss"),
				options: fc.record(
					{
						modulusLength: arbitrary.rsaModulesLength(),
						hashAlgorithm: fc.constantFrom("sha256", "sha384", "sha512"),
						mgf1HashAlgorithm: fc.constantFrom("sha256", "sha384", "sha512"),
						saltLength: fc.integer({ min: 1 }),
						paramEncoding: fc.constantFrom("named", "explicit"),
						publicKeyEncoding: fc.record(
							{
								type: fc.constantFrom("spki"),
								format: fc.constantFrom("pem", "der"),
							},
							{
								requiredKeys: ["type", "format"],
							},
						),
						privateKeyEncoding: fc.record(
							{
								type: fc.constantFrom("pkcs8"),
								format: fc.constantFrom("pem", "der"),
							},
							{
								requiredKeys: ["type", "format"],
							},
						),
					},
					{
						requiredKeys: ["modulusLength"],
					},
				),
			})
			.map(({ type, options }) => crypto.generateKeyPairSync(type, options)),
};

{
	const got = process.version;
	const want = "v21.0.0";
	assert.equal(
		got,
		want,
		`The Node.js version must be ${want} (got ${got}) because that's what GHunter used`,
	);
}

test("crypto.createPrivateKey", () => {
	fc.assert(
		fc.property(arbitrary.privateKeyCreateOptions(), (keyOptions) => {
			keyOptions = ppTestKit.wrap(keyOptions);

			try {
				crypto.createPrivateKey(keyOptions);
			} catch (_) {
				fc.pre(false);
			}

			ppTestKit.check(keyOptions);
		}),
	);
});

test("crypto.createPublicKey", () => {
	fc.assert(
		fc.property(arbitrary.publicKeyCreateOptions(), (keyOptions) => {
			keyOptions = ppTestKit.wrap(keyOptions);

			try {
				crypto.createPublicKey(keyOptions);
			} catch (_) {
				fc.pre(false);
			}

			ppTestKit.check(keyOptions);
		}),
	);
});

test("crypto.privateEncrypt", () => {
	fc.assert(
		fc.property(
			fc.record(
				{
					key: arbitrary.privateKey(),
					padding: fc.constantFrom(
						crypto.constants.RSA_NO_PADDING,
						crypto.constants.RSA_PKCS1_PADDING,
					),
				},
				{
					requiredKeys: ["key"],
				},
			),
			fc.string(),
			(privateKey, buffer) => {
				privateKey = ppTestKit.wrap(privateKey);

				try {
					crypto.privateEncrypt(privateKey, buffer);
				} catch (_) {
					fc.pre(false);
				}

				ppTestKit.check(privateKey);
			},
		),
	);
});

test("crypto.privateKey.export", () => {
	fc.assert(
		fc.property(
			arbitrary.privateKeyCreateOptions(),
			arbitrary.privateKeyExportOptions(),
			(keyOptions, exportOptions) => {
				exportOptions = ppTestKit.wrap(exportOptions);

				let key;
				try {
					key = crypto.createPrivateKey(keyOptions);
				} catch (_) {
					fc.pre(false);
				}

				try {
					key.export(exportOptions);
				} catch (_) {}

				ppTestKit.check(exportOptions);
			},
		),
	);
});

test("crypto.publicEncrypt", () => {
	fc.assert(
		fc.property(
			fc.record(
				{
					key: arbitrary.publicKey(),
					oaepHash: fc.constantFrom("sha1"),
					oaepLabel: fc.string(),
					passphrase: fc.string(),
					padding: fc.constantFrom(
						crypto.constants.RSA_NO_PADDING,
						crypto.constants.RSA_PKCS1_PADDING,
					),
					encoding: fc.constantFrom("buffer", "key", "oaepLabel", "passphrase"),
				},
				{
					requiredKeys: ["key"],
				},
			),
			fc.string(),
			(key, buffer) => {
				key = ppTestKit.wrap(key);

				try {
					crypto.publicEncrypt(key, buffer);
				} catch (_) {
					fc.pre(false);
				}

				ppTestKit.check(key);
			},
		),
	);
});

test("crypto.publicKey.export", () => {
	fc.assert(
		fc.property(
			arbitrary.publicKeyCreateOptions(),
			arbitrary.publicKeyExportOptions(),
			(keyOptions, exportOptions) => {
				exportOptions = ppTestKit.wrap(exportOptions);

				let key;
				try {
					key = crypto.createPublicKey(keyOptions);
				} catch (_) {
					fc.pre(false);
				}

				try {
					key.export(exportOptions);
				} catch (_) {}

				ppTestKit.check(exportOptions);
			},
		),
	);
});

test("crypto.subtle.export", async () => {
	await fc.assert(
		fc.asyncProperty(
			fc.record({
				name: fc.constantFrom("AES-CTR", "AES-CBC", "AES-GCM"),
				iv: fc
					.array(fc.integer({ min: 0, max: 256 }), {
						minLength: 16,
						maxLength: 16,
					})
					.map((array) => Buffer.from(array)),
				counter: fc
					.array(fc.integer({ min: 0, max: 256 }), {
						minLength: 16,
						maxLength: 16,
					})
					.map((array) => Buffer.from(array)),
				length: fc.oneof(fc.constantFrom(128, 192, 256)),
				tagLength: fc.constantFrom(32, 64, 96, 104, 112, 120, 128),
				hash: fc.constantFrom("SHA-1", "SHA-256", "SHA-384", "SHA-512"),
			}),
			fc.boolean(),
			fc.uniqueArray(
				fc.constantFrom(
					"encrypt",
					"decrypt",
					"sign",
					"verify",
					"deriveKey",
					"deriveBits",
					"wrapKey",
					"unwrapKey",
				),
				{ minLength: 1 },
			),
			fc.string().map((data) => Buffer.from(data)),
			async (algorithm, extractable, usages, data) => {
				let cryptoKey;
				try {
					cryptoKey = await crypto.subtle.generateKey(
						algorithm,
						extractable,
						usages,
					);
					cryptoKey = ppTestKit.wrap(cryptoKey);
					await crypto.subtle.encrypt(algorithm, cryptoKey, data);
				} catch (_) {
					fc.pre(false);
				}

				ppTestKit.check(cryptoKey);
			},
		),
	);
});
