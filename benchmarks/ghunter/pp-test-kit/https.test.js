// SPDX-License-Identifier: ISC

import * as assert from "node:assert/strict";
import * as https from "node:https";
import * as process from "node:process";
import { test } from "node:test";

import * as fc from "fast-check";
import * as ppTestKit from "pp-test-kit/manual";

const port = 1337;
const url = `https://localhost:${port}/`;

const arbitrary = {
	/**
	 * The options arbitrary generates options objects for the request function
	 * (and related functions) of the `node:https` module.
	 *
	 * Various fields are omitted because they interfere with the test.
	 *
	 * @returns {import('fast-check').Arbitrary} A fast-check arbitrary.
	 */
	options: () =>
		fc.record(
			{
				agent: fc.constantFrom(false, undefined),
				allowHalfOpen: fc.boolean(),
				defaultPort: arbitrary.port(),
				family: fc.constantFrom(4, 6),
				highWaterMark: fc.integer({ min: 0 }),
				honorCipherOrder: fc.boolean(),
				insecureHTTPParser: fc.boolean(),
				joinDuplicateHeaders: fc.boolean(),
				localAddress: fc.string(),
				minDHSize: fc.integer({ min: 0 }),
				passphrase: fc.string(),
				path: fc.string(),
				protocol: fc.constantFrom("https:"),
				servername: fc.string(),
				setHost: fc.boolean(),
				timeout: fc.integer({ min: 1 }),
				uniqueHeaders: fc.uniqueArray(fc.string()),

				// We'll only use disabled traces because otherwise it pollutes stdout.
				enableTrace: fc.constant(false),

				// We'll only use `rejectUnauthorized: false` because the server has a
				// self-signed certificate.
				rejectUnauthorized: fc.constant(false),
			},
			{
				requiredKeys: [
					// We'll always have this property (because the server has a
					// self-signed certificate). This does mean we can't find it
					// as a missing property access.
					"rejectUnauthorized",
				],
			},
		),

	/**
	 * The port arbitrary generates valid networking port values.
	 *
	 * @returns {import('fast-check').Arbitrary} A fast-check arbitrary.
	 */
	port: () => fc.integer({ min: 0, max: 65536 }),
};

{
	const got = process.version;
	const want = "v21.0.0";
	assert.equal(
		got,
		want,
		`The Node.js version must be ${want} (got ${got}) because that's what GHunter used`,
	);
}

{
	const options = {
		key: `-----BEGIN RSA PRIVATE KEY-----
MIIEpgIBAAKCAQEA11wz7hAhdN072sp2aTySdsla65aZT10L1o74JNBZmMyeDSq7
Lbbvqq8XuLlwogS0ACAQVEjL7GPVMgJwzRytdaSWdzr26dftPCL3m1h8azU/Bs1n
afHWhi3rx8RQnLfyusq0fThR9Xd3xfjDuaqhoa7Nc36XbG98XWkgxpm2R7wRpLyN
T9K2Ixvry9orZicTxv0Zyq+tD3CzSEch5OCApVZNaqEcIDgiN1zE/zqIe83v9+NY
pLo61yPiqqedCqZaKp037uwJCp/RL9g6bh7IFDGZcNFv3k0xjZB4BVm4BZMpSlbI
49hncVIJZNB7qWt5ilCzEbwOhgLt6Y3Yh8rgMwIDAQABAoIBAQCWK7oeX+skdXxe
RV2qZk1vPVsD+kCvYZ92nr0T1qETdmMjpU9eQjj/GRb+fXi30XW+vJ0GWLix/q9U
LvV/YWbnKLyvKVOxnhrUG0HzdhFUJI3tbV+WNce0SuMlqpPXpEFC1URkKNilxQek
6aF5ny0T9DNZPMXUHC1paXwsYFUF0SwtRalY/UheRvTLZgJJAS13SR4mDZ2MJNiy
NkLOaokGqI+U9euNsbZoJi1Oh4jOoA623NlVntPjfDc1Wzwsp/rMrJFiyfrBHlln
sjAcArcNrAR6GosX4kfQIKdHXZFMhRLm6vJSSbROIIoqOTPJF8kFYKqSQMQCT0sd
v8fVw35pAoGBAO7/LTrKcFth/VuqkyHFCvfIuXrHtEJon0MjsCN+IZu+WFsjjZ/7
mMOHqWPBb1oXBdarWFshdq5zVCcoA/hH1H6BAi4zOlU2vaQdeejbhlWWt5GVRl70
fa2RpyGmzTIbj6QDpv1GROQxhk4lsFp/4Jtd4wnI71P2fF+gTk+VxG1VAoGBAOau
iPMVeNgSHAT0f0ms6+tdyiRf3jo8afd9TSN4g0324Cd/hIzd5ysjf9WfN3xni0tQ
Ho8v1CKvt1xFCG9kgApHE+umLOEF5X1LwZzpPwp9PEN7tj0U6jis/spPqmToSrlB
hcqLVK3DRF268RADZtYCvbCeICUQIydeRzeqL1dnAoGBAORvkRq9flFKQvFIA+Pm
Q8p82BRsJ3bGK1vwE56JI7SKZkeOnwLIZ8aUt6p6rGUu1kzOb+CDBr5Ny6S1kb3R
YmEcDCp+moXykab46ZpRoX/TVFhZlu6RwEKTkm0O0tjPESeSuh0h+h0m3Rl62qpV
yiiWv7iNj3vwgAai4dcd7qcVAoGBAJtNwhG10QfXGVoUqWWWr7ZkDGV0zY5zgfwH
Ndm9ltj0bnDpHvofozSdz8CoUpjEtTjdFdXco3s+xbmvGEC2bqV8uOKbm/dbGufC
CvVy6uhK7DmanlR9Mjs57MeT3unUogobHNIB/96EahCYfRcE00Udz0uLCaUqOiCV
DmlhgEHdAoGBAIBgtzi2mZ7smMEvJqOSeRQT11PBLRasM0Ba/79oebSMFpbZ8mok
8PqfJvWYxISKYMWIHRygGbOmswhRDDrVN1E61NAisIwBc42tSTRafxfVfZ0LYa4I
c967Kr2Krio1rwUQptIhWqQQnLWhycvMKImLLE55J9mmDEf9Rat0TAQJ
-----END RSA PRIVATE KEY-----
`,
		cert: `-----BEGIN CERTIFICATE-----
MIIDATCCAemgAwIBAgIUb32MablwTzVJh3UQXwdun9pBoF0wDQYJKoZIhvcNAQEL
BQAwFDESMBAGA1UEAwwJbG9jYWxob3N0MB4XDTIyMDMxNTE2MzQwOFoXDTMyMDMx
MjE2MzQwOFowFDESMBAGA1UEAwwJbG9jYWxob3N0MIIBIjANBgkqhkiG9w0BAQEF
AAOCAQ8AMIIBCgKCAQEA11wz7hAhdN072sp2aTySdsla65aZT10L1o74JNBZmMye
DSq7Lbbvqq8XuLlwogS0ACAQVEjL7GPVMgJwzRytdaSWdzr26dftPCL3m1h8azU/
Bs1nafHWhi3rx8RQnLfyusq0fThR9Xd3xfjDuaqhoa7Nc36XbG98XWkgxpm2R7wR
pLyNT9K2Ixvry9orZicTxv0Zyq+tD3CzSEch5OCApVZNaqEcIDgiN1zE/zqIe83v
9+NYpLo61yPiqqedCqZaKp037uwJCp/RL9g6bh7IFDGZcNFv3k0xjZB4BVm4BZMp
SlbI49hncVIJZNB7qWt5ilCzEbwOhgLt6Y3Yh8rgMwIDAQABo0swSTALBgNVHQ8E
BAMCBaAwEwYDVR0lBAwwCgYIKwYBBQUHAwEwJQYDVR0RBB4wHIIJMTI3LjAuMC4x
gglsb2NhbGhvc3SHBH8AAAEwDQYJKoZIhvcNAQELBQADggEBAEEXVnMzax6vmGPY
TNNK6HJuN8xUUbEKVHKPDTwKbn2ZPQRGNOs4CJ5vVc1h2tU1mfgg20FZzPqZ+y9N
hIFIaESTpX881NXM87aE21Gqo4rS66lubcoEUHWeY1+/LcdD6Gh/Ur0V4CBORZjA
W/H1nASZQKQLMHIkriopbclMQPOsUYM11JxLsTH6hOPCJRM5cl1K1KsHWY2yJ656
Gw9IX0NBTak0flEAL/JQkFlm2PRFJhWShPBHsC8/AWBcvgJH/0noNnDS5vAtCWa6
rJO6SoHFJwWK3xCVU2YsC6No2DrAukyqEdN8arifJ/2hEjx1XAfockNsWzrT1mB5
EpLq2L0=
-----END CERTIFICATE-----
`,
	};

	const server = https.createServer(options, (_, res) => res.end());
	server.listen(port);
	setTimeout(() => server.close(), 1000);
}

test("https.get", async () => {
	await fc.assert(
		fc.asyncProperty(
			arbitrary.options(),
			(options) =>
				new Promise((resolve, reject) => {
					options = ppTestKit.wrap(options);
					try {
						https.get(url, options, () => {
							try {
								ppTestKit.check(options);
								resolve();
							} catch (error) {
								reject(error);
							}
						});
					} catch (_) {
						ppTestKit.check(options);
						resolve();
					}
				}),
		),
	);
});

test("https.request", async () => {
	await fc.assert(
		fc.asyncProperty(
			arbitrary.options(),
			(options) =>
				new Promise((resolve, reject) => {
					options = ppTestKit.wrap(options);
					try {
						const request = https.request(url, options, () => {
							try {
								ppTestKit.check(options);
								resolve();
							} catch (error) {
								reject(error);
							}
						});
						request.end();
					} catch (_) {
						ppTestKit.check(options);
						resolve();
					}
				}),
		),
	);
});
