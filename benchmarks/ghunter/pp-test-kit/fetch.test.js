// SPDX-License-Identifier: ISC

import * as assert from "node:assert/strict";
import * as process from "node:process";
import { test } from "node:test";

import * as fc from "fast-check";
import * as ppTestKit from "pp-test-kit/manual";

const arbitrary = {
	/**
	 * The options arbitrary generates options objects for the fetch API. Based on
	 * <https://developer.mozilla.org/en-US/docs/Web/API/RequestInit>.
	 *
	 * @returns {import('fast-check').Arbitrary} A fast-check arbitrary.
	 */
	options: () =>
		fc.record(
			{
				body: fc.string(),
				cache: fc.constantFrom(
					"default",
					"no-store",
					"reload",
					"no-cache",
					"force-cache",
					"only-if-cache",
				),
				credentials: fc.constantFrom("omit", "same-origin", "include"),
				headers: fc.dictionary(fc.string(), fc.string()),
				keepalive: fc.boolean(),
				method: fc.constantFrom(
					"CONNECT",
					"DELETE",
					"GET",
					"HEAD",
					"OPTIONS",
					"PATCH",
					"POST",
					"PUT",
					"TRACE",
				),
				mode: fc.constantFrom(
					"same-origin",
					"cors",
					"no-cors",
					"navigate",
					"websocket",
				),
				priority: fc.constantFrom("high", "low", "auto"),
				redirect: fc.constantFrom("follow", "error", "manual"),
				referrer: fc.oneof(fc.webUrl(), fc.constantFrom("", "about:client")),
				referrerPolicy: fc.constantFrom(
					"no-referrer",
					"no-referrer-when-downgrade",
					"origin",
					"origin-when-cross-origin",
					"same-origin",
					"strict-origin",
					"strict-origin-when-cross-origin",
					"unsafe-url",
				),
				signal: fc.constant(new AbortController()),
			},
			{ requiredKeys: [] },
		),
};

{
	const got = process.version;
	const want = "v21.0.0";
	assert.equal(
		got,
		want,
		`The Node.js version must be ${want} (got ${got}) because that's what GHunter used`,
	);
}

test("fetch", () => {
	const url = "localhost:1337";

	fc.assert(
		fc.property(arbitrary.options(), (options) => {
			options = ppTestKit.wrap(options);
			fetch(url, options);
			ppTestKit.check(options);
		}),
	);
});
