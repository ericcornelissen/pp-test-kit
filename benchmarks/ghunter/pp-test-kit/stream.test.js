// SPDX-License-Identifier: ISC

import * as assert from "node:assert/strict";
import * as process from "node:process";
import * as stream from "node:stream";
import { test } from "node:test";

import * as fc from "fast-check";
import * as ppTestKit from "pp-test-kit/manual";

const arbitrary = {
	/**
	 * The options arbitrary generates options objects for the Duplex
	 * constructor of the `node:stream` module.
	 *
	 * @returns {import('fast-check').Arbitrary} A fast-check arbitrary.
	 */
	options: () =>
		fc.record(
			{
				allowHalfOpen: fc.boolean(),
				readable: fc.boolean(),
				writable: fc.boolean(),
				readableObjectMode: fc.boolean(),
				writableObjectMode: fc.boolean(),
				readableHighWaterMark: fc.integer(),
				writableHighWaterMark: fc.integer(),
			},
			{ requiredKeys: [] },
		),
};

{
	const got = process.version;
	const want = "v21.0.0";
	assert.equal(
		got,
		want,
		`The Node.js version must be ${want} (got ${got}) because that's what GHunter used`,
	);
}

test("stream.Duplex", () => {
	fc.assert(
		fc.property(arbitrary.options(), (options) => {
			options = ppTestKit.wrap(options);
			new stream.Duplex(options);
			ppTestKit.check(options);
		}),
	);
});
