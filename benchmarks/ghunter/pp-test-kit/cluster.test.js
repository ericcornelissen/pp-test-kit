// SPDX-License-Identifier: ISC

import * as assert from "node:assert/strict";
import * as cluster from "node:cluster";
import * as process from "node:process";
import { test } from "node:test";

import * as fc from "fast-check";
import * as ppTestKit from "pp-test-kit/manual";

const arbitrary = {
	/**
	 * The env arbitrary generates environment variable objects.
	 *
	 * @returns {import('fast-check').Arbitrary} A fast-check arbitrary.
	 */
	env: () => fc.dictionary(fc.string(), fc.string()),
};

{
	const got = process.version;
	const want = "v21.0.0";
	assert.equal(
		got,
		want,
		`The Node.js version must be ${want} (got ${got}) because that's what GHunter used`,
	);
}

test("cluster.fork", () => {
	fc.assert(
		fc.property(arbitrary.env(), (env) => {
			env = ppTestKit.wrap(env);
			const worker = cluster.fork(env);
			worker.kill();
			ppTestKit.check(env);
		}),
	);
});
