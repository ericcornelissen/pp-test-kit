// SPDX-License-Identifier: MIT
// For the full license see: https://github.com/nodejs/node/blob/38d0e69347de4db532a3bb6bddf51ead9ff764f8/LICENSE

const crypto = require("crypto");

(async () => {
	let key = await crypto.subtle.generateKey(
		{ name: "AES-GCM", length: 128 },
		true,
		["encrypt", "decrypt"],
	);
	let algorithm = { name: "AES-GCM", iv: new Uint8Array(12) };
	crypto.subtle.encrypt(algorithm, key, new Uint8Array());
})();
