// SPDX-License-Identifier: ISC

const dgram = require("dgram");

const message = "foobar";
const socket = dgram.createSocket("udp4");
socket.send(message, 0, message.length, 1337);
socket.close();
