// SPDX-License-Identifier: ISC

const cluster = require("cluster");

const worker = cluster.fork();
worker.kill();
