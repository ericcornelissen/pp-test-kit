// SPDX-License-Identifier: ISC

const tls = require("tls");
const net = require("net");

const socket = new tls.TLSSocket(new net.Socket(), {});
socket.connect({ port: 1337 });
