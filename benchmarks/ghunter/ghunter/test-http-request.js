// SPDX-License-Identifier: MIT
// For the full license see: https://github.com/KTH-LangSec/server-side-prototype-pollution/blob/ab1b626590b4b48dcbc952186a5d4f2a79e3d80d/LICENSE

const http = require("http");

const req = http.request({ host: "localhost" });
req.end();
