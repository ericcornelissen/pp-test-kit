#!/bin/bash
# SPDX-License-Identifier: MIT
# For the full license see: https://github.com/KTH-LangSec/ghunter4node/blob/ba0c9f691ce6d4250a1917353585d965f1d561c4/LICENSE

./setup.sh s2s

cd ./node

mkdir -p fuzzing/
mv fuzzing/ fuzzing.bkp/

rm -rf fuzzing.pp-test-kit/

# Start time
start=$(date +%s)

# Check if the directory fuzzing/tmp and fuzzing/init exists
if [ -d "fuzzing/tmp" ]; then
    # Directory exists, remove all nested files
    rm -rf fuzzing/tmp
fi

# Create the dir tmp
mkdir -p fuzzing/tmp

if [ -d "fuzzing/init" ]; then
    # Directory exists, remove it
    rm -rf fuzzing/init
fi

run() {
    local filename=$1
    local postfix=$2

    ./tools/test.py "$filename" -t 20 -j 1
    mv fuzzing/tmp fuzzing/init
    mkdir fuzzing/tmp
    ./tools/test.py "$filename" -t 20 -j 1
    node ../src/analyze-taint-logs.js
    node ../src/analyze-taint-logs-step2-to-sarif.js '' --all
    node ../src/analyze-taint-compare.js
    mv fuzzing/init "fuzzing/init-$postfix"
    mv fuzzing/tmp "fuzzing/tmp-$postfix"
    mkdir fuzzing/tmp
}

cp ../src/pp-test-kit/* ./test/parallel/

run parallel/test-child-process-exec.js            cp-exec                      # 1
run parallel/test-child-process-execfile.js        cp-execfile                  # 2
run parallel/test-child-process-execfilesync.js    cp-execfilesync              # 3
run parallel/test-child-process-execsync.js        cp-execsync                  # 4
run parallel/test-child-process-fork.js            cp-fork                      # 5
run parallel/test-child-process-spawn.js           cp-spawn                     # 6
run parallel/test-child-process-spawnsync.js       cp-spawnsync                 # 7
run parallel/test-cluster-fork.js                  cluster-fork                 # 8
run parallel/test-crypto-createprivatekey.js       crypto-createprivatekey      # 9
run parallel/test-crypto-createpublickey.js        crypto-createpublickey       # 10
run parallel/test-crypto-privateencrypt.js         crypto-privateencrypt        # 11
run parallel/test-crypto-privatekey-export.js      crypto-privatekey-export     # 12
run parallel/test-crypto-publicencrypt.js          crypto-publicencrypt         # 13
run parallel/test-crypto-publickey-export.js       crypto-publickey-export      # 14
run parallel/test-crypto-subtle-encrypt.js         crypto-subtle-encrypt        # 15
run parallel/test-dgram-socket-send.js             dgram-socket-send            # 16
run parallel/test-fetch.js                         fetch                        # 17
run parallel/test-fs-createwritestream.js          fs-createwritestream         # 18
run parallel/test-https-get.js                     https-get                    # 19
run parallel/test-https-request.js                 https-request                # 20
run parallel/test-http-get.js                      http-get                     # 21
run parallel/test-http-request.js                  http-request                 # 22
run parallel/test-http-server-listen.js            http-server-listen           # 23
run parallel/test-import.js                        import                       # 24
run parallel/test-stream-duplex.js                 stream-duplex                # 25
run parallel/test-tls-tlssocket-connect.js         tls-tlssocket-connect        # 26
run parallel/test-vm-syntheticmodule.mjs           vm-syntheticmodule           # 27
run parallel/test-zlib-creategzip-write.js         zlib-creategzip-write        # 28

cd ./test/parallel/
rm \
    run-compare-pp-test-kit.sh \
    sample-fork.js \
    sample-import.mjs \
    test-child-process-exec.js \
    test-child-process-execfile.js \
    test-child-process-execfilesync.js \
    test-child-process-execsync.js \
    test-child-process-fork.js \
    test-child-process-spawn.js \
    test-child-process-spawnsync.js \
    test-cluster-fork.js \
    test-crypto-createprivatekey.js \
    test-crypto-createpublickey.js \
    test-crypto-privateencrypt.js \
    test-crypto-privatekey-export.js \
    test-crypto-publicencrypt.js \
    test-crypto-publickey-export.js \
    test-crypto-subtle-encrypt.js \
    test-dgram-socket-send.js \
    test-fetch.js \
    test-fs-createwritestream.js \
    test-https-get.js \
    test-https-request.js \
    test-http-get.js \
    test-http-request.js \
    test-http-server-listen.js \
    test-import.js \
    test-stream-duplex.js \
    test-tls-tlssocket-connect.js \
    test-vm-syntheticmodule.mjs \
    test-zlib-creategzip-write.js
cd ../..

mv fuzzing fuzzing.pp-test-kit
mv fuzzing.bkp fuzzing

# End time
end=$(date +%s)

# Calculate and report execution time
execution_time=$((end - start))
minutes=$((execution_time / 60))
seconds=$((execution_time % 60))
echo "Total execution time: $minutes mins $seconds secs"
cd ..
