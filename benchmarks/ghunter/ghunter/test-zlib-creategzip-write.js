// SPDX-License-Identifier: ISC

const zlib = require("zlib");

zlib.createGzip().write("foobar");
