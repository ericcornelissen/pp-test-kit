<!-- SPDX-License-Identifier: CC-BY-SA-4.0 -->

# `pp-test-kit` v. GHunter

This document provides a comparison of gadget detection capabilities between
`pp-test-kit` and [GHunter: Universal Prototype Pollution Gadgets in JavaScript
Runtimes] on the basis of gadgets identified by GHunter. We compare if the tools
find the property used in the gadgets presented by GHunter.

[ghunter: universal prototype pollution gadgets in javascript runtimes]: https://www.usenix.org/conference/usenixsecurity24/presentation/cornelissen

## Experiments

### Requirements

- Node.js v21.0.0;
- pp-test-kit v0.5.0;
- Requirements for the [GHunter artifact];

[ghunter artifact]: https://github.com/KTH-LangSec/ghunter4node

### Running

#### `pp-test-kit`

To obtain the results for `pp-test-kit` run the test files in the `pp-test-kit`
directory. For example, from this directory run:

```shell
benchmarks/ghunter $ cd pp-test-kit
benchmarks/ghunter/pp-test-kit $ docker build --file Containerfile --tag pp-test-kit-ghunter .
benchmarks/ghunter/pp-test-kit $ docker run -it --rm --name pp-test-kit-ghunter pp-test-kit-ghunter

# Inside the container
/experiment $ node --test --experimental-vm-modules *.test.js
```

The last command produces the results and should take no more than 10 minutes.
(Running `crypto.test.js` can sometimes take a lot of time.)

This will output the test results to stdout. Each `✖ [...] (X.Yms)` section
lists an assertion error that reports the number of detected properties followed
by a newline-separated list of detected properties. The list of properties is
the basis for the comparison presented. Note that if there is no section for a
given API it means there were 0 properties detected for it.

#### GHunter Experiments

To obtain the results for GHunter set up the project, copy the script and tests
defined in this benchmark into the project, and run the scripts. For example,
from this directory run:

```shell
benchmarks/ghunter $ cd ghunter
benchmarks/ghunter/ghunter $ docker pull ghcr.io/kth-langsec/ghunter4node@sha256:a2b09930d54d652f192d086a91186d5d9d94c14f2deae451b88e563fcb38231a
benchmarks/ghunter/ghunter $ docker run -dit --name ghunter4node ghcr.io/kth-langsec/ghunter4node@sha256:a2b09930d54d652f192d086a91186d5d9d94c14f2deae451b88e563fcb38231a
benchmarks/ghunter/ghunter $ docker exec -it ghunter4node /bin/bash

# Inside the container
/src $ ./run-basic_test.sh

# Outside the container
benchmarks/ghunter/ghunter $ docker cp ./run-compare-pp-test-kit.sh ghunter4node:/src/run-compare-pp-test-kit.sh
benchmarks/ghunter/ghunter $ docker cp . ghunter4node:/src/src/pp-test-kit/

# Inside the container
/src $ ./run-compare-pp-test-kit.sh | tee result.log
```

The last command produces the results and should take no more than 10 minutes.

This will output a summary of the test results to stdout. Each section like the
snippet below is the result for a particular API. The "gadget candidate count"
corresponds to the _Reported_ column in the stats table for GHunter while the
presence of a property as a key in the `<JSON>` tells you that GHunter detected
that property for the API as seen in the _By Property_ table. From there the
remaining stats can be derived.

The full test result for each API can be found in the `node/fuzzing.pp-test-kit`
directory in the numbered directories. The indexed directories correspond to the
numbers found in the `run-compare-pp-test-kit.sh` script.

```text
RESULTS (numbers):
        sinks reached (unfiltered)  :  <NUMBER>
     unique s2s pairs (unfiltered)  :  <NUMBER>

              infrastructure sinks  :  <NUMBER>
             buffer.byteLengthUtf8  :  <NUMBER>
             messaging.postMessage  :  <NUMBER>
                 buffer in fs.read  :  <NUMBER>

       unique s2s pairs (filtered)  :  <NUMBER>
{<JSON>}
gadget candidate count: <NUMBER>
```

The tests for this experiment were created as follows:

- First, the tests `test-child-*`, `test-import.js`, and `test-vm-*.mjs` are all
  taken directly from the test suite already present in the `src/ss21` directory
  in the GHunter artifact.
- Second, where the [server-side-prototype-pollution] has a proof of concept
  available for a given API, the test is derived from that proof of concept.
- Third, any remaining tests are custom made based on the related test for
  written for the experiments with `pp-test-kit`.

[server-side-prototype-pollution]: https://github.com/KTH-LangSec/server-side-prototype-pollution/tree/ab1b626590b4b48dcbc952186a5d4f2a79e3d80d

## Comparison

### By Property

| API                          | Property                       | `pp-test-kit` | GHunter |
| ---------------------------- | ------------------------------ | ------------- | ------- |
| `child_process.exec`         | `NODE_OPTIONS`                 | Yes           | Yes     |
| `child_process.execFile`     | `NODE_OPTIONS`                 | Yes           | Yes     |
| `child_process.execFileSync` | `env`                          | Yes           | Yes     |
|                              | `input`                        | No (\*)       | Yes     |
|                              | `NODE_OPTIONS`                 | Yes           | Yes     |
|                              | `shell`                        | Yes           | Yes     |
| `child_process.execSync`     | `input`                        | No (\*)       | Yes     |
|                              | `NODE_OPTIONS`                 | Yes           | Yes     |
| `child_process.fork`         | `NODE_OPTIONS`                 | Yes           | Yes     |
| `child_process.spawn`        | `env`                          | Yes           | Yes     |
|                              | `NODE_OPTIONS`                 | Yes           | Yes     |
|                              | `shell`                        | Yes           | Yes     |
| `child_process.spawnSync`    | `env`                          | Yes           | Yes     |
|                              | `input`                        | No (\*)       | Yes     |
|                              | `NODE_OPTIONS`                 | Yes           | Yes     |
|                              | `shell`                        | Yes           | Yes     |
| `cluster.fork`               | `NODE_OPTIONS`                 | No (\*)       | Yes     |
| `crypto.createPrivateKey`    | `type`                         | Yes           | Yes     |
|                              | `passphrase`                   | Yes           | Yes     |
| `crypto.createPublicKey`     | `type`                         | Yes           | Yes     |
|                              | `passphrase`                   | Yes           | Yes     |
| `crypto.privateEncrypt`      | `padding`                      | Yes           | Yes     |
| `crypto.privateKey.export`   | `kty`                          | No            | Yes     |
| `crypto.publicEncrypt`       | `padding`                      | Yes           | Yes     |
| `crypto.publicKey.export`    | `kty`                          | No            | Yes     |
| `crypto.subtle.encrypt`      | `kty`                          | No            | Yes     |
| `dgram.Socket.send`          | `address`                      | No (\*\*)     | Yes     |
| `fetch`                      | `body`                         | Yes           | No      |
|                              | `method`                       | Yes           | No      |
|                              | `referrer`                     | Yes           | Yes     |
|                              | `socketPath`                   | No (\*\*)     | Yes     |
| `fs.createWriteStream`       | `mode`                         | Yes           | No      |
| `https.get`                  | `hostname`                     | No (\*)       | Yes     |
|                              | `headers`                      | No (\*)       | Yes     |
|                              | `method`                       | No (\*)       | Yes     |
|                              | `NODE_TLS_REJECT_UNAUTHORIZED` | No (\*\*)     | No      |
|                              | `path`                         | No (\*)       | Yes     |
|                              | `port`                         | No (\*)       | No      |
| `https.request`              | `0`                            | No (\*\*)     | Yes     |
|                              | `hostname`                     | No (\*)       | Yes     |
|                              | `headers`                      | No (\*)       | Yes     |
|                              | `method`                       | No (\*)       | Yes     |
|                              | `NODE_TLS_REJECT_UNAUTHORIZED` | No (\*\*)     | No      |
|                              | `path`                         | No (\*)       | Yes     |
|                              | `port`                         | No (\*)       | No      |
| `http.get`                   | `hostname`                     | No (\*)       | Yes     |
|                              | `headers`                      | No (\*)       | Yes     |
|                              | `method`                       | No (\*)       | Yes     |
|                              | `path`                         | No (\*)       | Yes     |
|                              | `port`                         | No (\*)       | No      |
| `http.request`               | `hostname`                     | No (\*)       | Yes     |
|                              | `headers`                      | No (\*)       | Yes     |
|                              | `method`                       | No (\*)       | Yes     |
|                              | `path`                         | No (\*)       | Yes     |
|                              | `port`                         | No (\*)       | No      |
| `http.Server.listen`         | `backlog`                      | Yes           | Yes     |
| `import`                     | `source`                       | No (\*\*)     | Yes     |
| `stream.Duplex`              | `readableObjectMode`           | Yes           | Yes     |
| `tls.TLSSocket.connect`      | `path`                         | Yes           | Yes     |
| `vm.SyntheticModule`         | `sourceText`                   | No (\*\*)     | Yes     |
|                              | `lineOffset`                   | No (\*\*)     | No      |
|                              | `columnOffset`                 | No (\*\*)     | No      |
| `zlib.createGzip().write`    | `writableObjectMode`           | Yes           | Yes     |

- (\*): Not detected because a shallow copy of the object is created before any
  property access occurs.
- (\*\*): Not detected because the prototype lookup does not occur on an object
  provided in the API.

### Statistics

#### `pp-test-kit`

| API                          | Reported | True Positives | False Positives | False Negatives | Precision | Recall |     F1 |
| ---------------------------- | -------: | -------------: | --------------: | --------------: | --------: | -----: | -----: |
| `child_process.exec`         |      `1` |            `1` |             `0` |             `0` |    `1.00` | `1.00` | `1.00` |
| `child_process.execFile`     |      `2` |            `1` |             `1` |             `0` |    `0.50` | `1.00` | `0.67` |
| `child_process.execFileSync` |     `18` |            `3` |            `15` |             `1` |    `0.17` | `0.75` | `0.27` |
| `child_process.execSync`     |      `1` |            `1` |             `0` |             `1` |    `1.00` | `0.50` | `0.67` |
| `child_process.fork`         |      `1` |            `1` |             `0` |             `0` |    `1.00` | `1.00` | `1.00` |
| `child_process.spawn`        |     `15` |            `3` |            `12` |             `0` |    `0.20` | `1.00` | `0.33` |
| `child_process.spawnSync`    |     `15` |            `3` |            `12` |             `1` |    `0.20` | `0.75` | `0.32` |
| `cluster.fork`               |      `0` |            `0` |             `0` |             `1` |         - |      - |      - |
| `crypto.createPrivateKey`    |      `9` |            `2` |             `7` |             `0` |    `0.22` | `1.00` | `0.37` |
| `crypto.createPublicKey`     |      `9` |            `2` |             `7` |             `0` |    `0.22` | `1.00` | `0.37` |
| `crypto.privateEncrypt`      |     `13` |            `1` |            `12` |             `0` |    `0.08` | `1.00` | `0.14` |
| `crypto.privateKey.export`   |      `2` |            `0` |             `2` |             `1` |         - |      - |      - |
| `crypto.publicEncrypt`       |     `13` |            `1` |            `12` |             `0` |    `0.08` | `1.00` | `0.14` |
| `crypto.publicKey.export`    |      `0` |            `0` |             `0` |             `1` |         - |      - |      - |
| `crypto.subtle.encrypt`      |      `0` |            `0` |             `0` |             `1` |         - |      - |      - |
| `dgram.Socket.send`          |      `6` |            `0` |             `6` |             `1` |         - |      - |      - |
| `fetch`                      |     `15` |            `3` |            `12` |             `1` |    `0.20` | `0.75` | `0.32` |
| `fs.createWriteStream`       |      `4` |            `1` |             `3` |             `0` |    `0.25` | `1.00` | `0.40` |
| `https.get`                  |      `0` |            `0` |             `0` |             `6` |         - |      - |      - |
| `https.request`              |      `0` |            `0` |             `0` |             `7` |         - |      - |      - |
| `http.get`                   |      `0` |            `0` |             `0` |             `5` |         - |      - |      - |
| `http.request`               |      `0` |            `0` |             `0` |             `5` |         - |      - |      - |
| `http.Server.listen`         |      `8` |            `1` |             `7` |             `0` |    `0.13` | `1.00` | `0.22` |
| `import`                     |      `0` |            `0` |             `0` |             `1` |         - |      - |      - |
| `stream.Duplex`              |     `29` |            `1` |            `28` |             `0` |    `0.03` | `1.00` | `0.07` |
| `tls.TLSSocket.connect`      |     `10` |            `1` |             `9` |             `0` |    `0.10` | `1.00` | `0.18` |
| `vm.SyntheticModule`         |      `2` |            `0` |             `2` |             `3` |         - |      - |      - |
| `zlib.createGzip().write`    |     `14` |            `1` |            `13` |             `0` |    `0.07` | `1.00` | `0.13` |
| **Total**                    |    `187` |           `27` |           `160` |            `36` |    `0.14` | `0.43` | `0.22` |

#### GHunter

| API                             | Reported | True Positives | False Positives | False Negatives | Precision | Recall |     F1 |
| ------------------------------- | -------: | -------------: | --------------: | --------------: | --------: | -----: | -----: |
| `child_process.exec`            |      `2` |            `1` |             `1` |             `0` |    `0.50` | `1.00` | `0.67` |
| `child_process.execFile`        |      `2` |            `1` |             `1` |             `0` |    `0.50` | `1.00` | `0.67` |
| `child_process.execFileSync`    |      `7` |            `4` |             `3` |             `0` |    `0.57` | `1.00` | `0.73` |
| `child_process.execSync`        |      `3` |            `2` |             `1` |             `0` |    `0.67` | `1.00` | `0.80` |
| `child_process.fork`            |      `1` |            `1` |             `0` |             `0` |    `1.00` | `1.00` | `1.00` |
| `child_process.spawn`           |      `5` |            `3` |             `2` |             `0` |    `0.60` | `1.00` | `0.75` |
| `child_process.spawnSync`       |      `7` |            `4` |             `3` |             `0` |    `0.57` | `1.00` | `0.73` |
| `cluster.fork`                  |      `2` |            `1` |             `1` |             `0` |    `0.50` | `1.00` | `0.67` |
| `crypto.createPrivateKey` (\*)  |      `4` |            `2` |             `2` |             `0` |    `0.50` | `1.00` | `0.67` |
| `crypto.createPublicKey` (\*)   |      `4` |            `2` |             `2` |             `0` |    `0.50` | `1.00` | `0.67` |
| `crypto.privateEncrypt`         |      `7` |            `1` |             `6` |             `0` |    `0.14` | `1.00` | `0.25` |
| `crypto.privateKey.export` (\*) |      `1` |            `1` |             `0` |             `0` |    `1.00` | `1.00` | `1.00` |
| `crypto.publicEncrypt`          |      `7` |            `1` |             `6` |             `0` |    `0.14` | `1.00` | `0.25` |
| `crypto.publicKey.export` (\*)  |      `1` |            `1` |             `0` |             `0` |    `1.00` | `1.00` | `1.00` |
| `crypto.subtle.encrypt` (\*)    |      `1` |            `1` |             `0` |             `0` |    `1.00` | `1.00` | `1.00` |
| `dgram.Socket.send`             |      `1` |            `1` |             `0` |             `0` |    `1.00` | `1.00` | `1.00` |
| `fetch`                         |      `7` |            `2` |             `5` |             `2` |    `0.29` | `0.50` | `0.36` |
| `fs.createWriteStream`          |      `0` |            `0` |             `0` |             `1` |         - |      - |      - |
| `https.get`                     |     `28` |            `4` |            `24` |             `2` |    `0.14` | `0.67` | `0.24` |
| `https.request` (\*)            |     `29` |            `5` |            `24` |             `2` |    `0.17` | `0.71` | `0.28` |
| `http.get`                      |      `8` |            `4` |             `4` |             `1` |    `0.50` | `0.80` | `0.62` |
| `http.request`                  |      `8` |            `4` |             `4` |             `1` |    `0.50` | `0.80` | `0.62` |
| `http.Server.listen` (\*)       |      `3` |            `1` |             `2` |             `0` |    `0.33` | `1.00` | `0.50` |
| `import`                        |      `1` |            `1` |             `0` |             `0` |    `1.00` | `1.00` | `1.00` |
| `stream.Duplex` (\*)            |      `1` |            `1` |             `0` |             `0` |    `1.00` | `1.00` | `1.00` |
| `tls.TLSSocket.connect` (\*)    |     `18` |            `1` |            `17` |             `0` |    `1.00` | `1.00` | `1.00` |
| `vm.SyntheticModule`            |      `1` |            `1` |             `0` |             `2` |    `1.00` | `0.33` | `0.50` |
| `zlib.createGzip().write` (\*)  |      `1` |            `1` |             `0` |             `0` |    `1.00` | `1.00` | `1.00` |
| **Total**                       |    `160` |           `52` |           `108` |             `9` |    `0.32` | `0.85` | `0.47` |

- (\*): The "Reported" number is the count given by `run-compare-pp-test-kit.sh`
  plus 1 for every segfault reported for the API in the paper. This is because
  segfaults are guaranteed true positives and not reproduced by the comparison
  script.

## Notes

- Since the `import` API doesn't accept objects there are no `pp-test-kit` tests
  for it.
- True Negatives are not reported because we don't know what the negatives are.
- Accuracy is not reported because we don't know the True Negatives.
