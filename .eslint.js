// Configuration file for ESLint (https://eslint.org/)

import jsdoc from "eslint-plugin-jsdoc";
import regexp from "eslint-plugin-regexp";
import top from "@ericcornelissen/eslint-plugin-top";

export default [
	{
		name: "Meta",
		files: ["**/*"],
		linterOptions: {
			reportUnusedDisableDirectives: "error",
			reportUnusedInlineConfigs: "error",
		},
	},
	{
		name: "JSDoc",
		files: ["**/*.js"],
		plugins: { jsdoc },
		rules: {
			"jsdoc/check-access": ["off"],
			"jsdoc/check-alignment": ["error"],
			"jsdoc/check-examples": ["off"],
			"jsdoc/check-indentation": [
				"error",
				{
					excludeTags: ["example"],
				},
			],
			"jsdoc/check-line-alignment": [
				"error",
				"never",
				{
					customSpacings: {
						postDelimiter: 1,
						postName: 1,
						postTag: 1,
						postType: 1,
					},
					tags: ["param", "returns", "since", "throws", "version"],
				},
			],
			"jsdoc/check-param-names": [
				"error",
				{
					allowExtraTrailingParamDocs: false,
					checkDestructured: true,
					checkRestProperty: true,
					disableExtraPropertyReporting: false,
					enableFixer: false,
					useDefaultObjectProperties: true,
				},
			],
			"jsdoc/check-property-names": ["error"],
			"jsdoc/check-syntax": ["error"],
			"jsdoc/check-tag-names": [
				"error",
				{
					definedTags: [],
				},
			],
			"jsdoc/check-types": [
				"error",
				{
					exemptTagContexts: [],
					noDefaults: false,
					unifyParentAndChildTypeChecks: false,
				},
			],
			"jsdoc/empty-tags": [
				"error",
				{
					tags: ["constant"],
				},
			],
			"jsdoc/implements-on-classes": ["off"],
			"jsdoc/informative-docs": [
				"error",
				{
					aliases: [],
					excludedTags: ["returns"],
					uselessWords: ["a", "an", "i", "in", "of", "our", "s", "the"],
				},
			],
			"jsdoc/match-description": [
				"error",
				{
					mainDescription: "^[A-Z`\\d][\\s\\S]*.\n?$",
					tags: {
						param: "^[A-Z].*\\.$",
						returns: "^[A-Z`].*\\.$",
						since: "^[0-9]\\.[0-9]\\.[0-9]",
						throws: "^[A-Z].*\\.$",
						yields: "^[A-Z`].*\\.$",
					},
				},
			],
			"jsdoc/match-name": ["off"],
			"jsdoc/multiline-blocks": [
				"error",
				{
					noFinalLineText: true,
					noMultilineBlocks: false,
					noSingleLineBlocks: false,
					noZeroLineText: true,
					singleLineTags: [],
				},
			],
			"jsdoc/no-bad-blocks": [
				"error",
				{
					ignore: [],
					preventAllMultiAsteriskBlocks: true,
				},
			],
			"jsdoc/no-blank-block-descriptions": ["error"],
			"jsdoc/no-defaults": ["off"],
			"jsdoc/no-missing-syntax": ["off"],
			"jsdoc/no-multi-asterisks": [
				"error",
				{
					allowWhitespace: true,
					preventAtEnd: true,
					preventAtMiddleLines: true,
				},
			],
			"jsdoc/no-restricted-syntax": ["off"],
			"jsdoc/no-types": ["off"],
			"jsdoc/no-undefined-types": [
				"error",
				{
					definedTypes: [],
				},
			],
			"jsdoc/require-asterisk-prefix": ["error", "always"],
			"jsdoc/require-description-complete-sentence": [
				"error",
				{
					abbreviations: ["e.g.", "..."],
					newlineBeforeCapsAssumesBadSentenceEnd: false,
					tags: [],
				},
			],
			"jsdoc/require-description": [
				"error",
				{
					checkConstructors: false,
					checkGetters: false,
					checkSetters: false,
					descriptionStyle: "body",
					exemptedBy: [],
				},
			],
			"jsdoc/require-example": ["off"],
			"jsdoc/require-file-overview": ["off"],
			"jsdoc/require-hyphen-before-param-description": ["error", "never"],
			"jsdoc/require-jsdoc": [
				"error",
				{
					publicOnly: false,
					require: {
						ArrowFunctionExpression: false,
						ClassDeclaration: false,
						ClassExpression: false,
						FunctionDeclaration: true,
						FunctionExpression: false,
						MethodDefinition: false,
					},
				},
			],
			"jsdoc/require-param": ["error"],
			"jsdoc/require-param-description": ["error"],
			"jsdoc/require-param-name": ["error"],
			"jsdoc/require-param-type": ["error"],
			"jsdoc/require-property": ["error"],
			"jsdoc/require-property-description": ["error"],
			"jsdoc/require-property-name": ["error"],
			"jsdoc/require-property-type": ["error"],
			"jsdoc/require-returns": [
				"error",
				{
					checkConstructors: false,
					checkGetters: false,
					exemptedBy: [],
					forceRequireReturn: false,
					forceReturnsWithAsync: false,
				},
			],
			"jsdoc/require-returns-check": [
				"error",
				{
					exemptGenerators: true,
					exemptAsync: true,
					reportMissingReturnForUndefinedTypes: false,
				},
			],
			"jsdoc/require-returns-description": ["error"],
			"jsdoc/require-returns-type": ["error"],
			"jsdoc/require-throws": [
				"error",
				{
					exemptedBy: [],
				},
			],
			"jsdoc/require-yields": [
				"error",
				{
					exemptedBy: [],
					forceRequireNext: false,
					forceRequireYields: true,
					next: false,
					nextWithGeneratorTag: false,
					withGeneratorTag: false,
				},
			],
			"jsdoc/require-yields-check": [
				"error",
				{
					checkGeneratorsOnly: false,
					next: false,
				},
			],
			"jsdoc/sort-tags": [
				"error",
				{
					alphabetizeExtras: false,
					reportTagGroupSpacing: false,
					tagSequence: [
						{
							tags: [
								"example",
								"typedef",
								"template",
								"callback",
								"property",
								"type",
								"param",
								"returns",
								"throws",
								"since",
							],
						},
					],
				},
			],
			"jsdoc/tag-lines": [
				"error",
				"any",
				{
					endLines: 0,
					startLines: 1,
				},
			],
			"jsdoc/text-escaping": ["off"],
			"jsdoc/valid-types": ["error"],
		},
		settings: {
			jsdoc: {
				ignorePrivate: false,
				ignoreInternal: false,
				maxLines: 1,
				minLines: 0,
				mode: "typescript",
				preferredTypes: {
					"Array.<>": false,
					"Array<>": false,
					"Object.<>": false,
				},
				structuredTags: {
					throws: {
						required: ["type"],
					},
				},
				tagNamePreference: {
					constant: "constant",
					param: "param",
					returns: "returns",
					throws: "throws",
					yields: "yields",
				},
			},
		},
	},
	{
		name: "Regular expressions",
		files: ["**/*.js"],
		ignores: ["test/testcases/classes.js"],
		plugins: { regexp },
		rules: {
			"regexp/confusing-quantifier": ["error"],
			"regexp/control-character-escape": ["error"],
			"regexp/grapheme-string-literal": ["error"],
			"regexp/hexadecimal-escape": ["error", "never"],
			"regexp/letter-case": [
				"error",
				{
					caseInsensitive: "lowercase",
					controlEscape: "lowercase",
					hexadecimalEscape: "uppercase",
					unicodeEscape: "uppercase",
				},
			],
			"regexp/match-any": [
				"error",
				{
					allows: ["[^]", "dotAll"],
				},
			],
			"regexp/negation": ["error"],
			"regexp/no-contradiction-with-assertion": ["error"],
			"regexp/no-control-character": ["off"],
			"regexp/no-dupe-characters-character-class": ["error"],
			"regexp/no-dupe-disjunctions": [
				"error",
				{
					report: "all",
					reportExponentialBacktracking: "potential",
					reportUnreachable: "potential",
				},
			],
			"regexp/no-empty-alternative": ["error"],
			"regexp/no-empty-capturing-group": ["error"],
			"regexp/no-empty-character-class": ["error"],
			"regexp/no-empty-group": ["error"],
			"regexp/no-empty-lookarounds-assertion": ["error"],
			"regexp/no-empty-string-literal": ["error"],
			"regexp/no-escape-backspace": ["error"],
			"regexp/no-extra-lookaround-assertions": ["error"],
			"regexp/no-invalid-regexp": ["error"],
			"regexp/no-invisible-character": ["error"],
			"regexp/no-lazy-ends": [
				"error",
				{
					ignorePartial: false,
				},
			],
			"regexp/no-legacy-features": ["error"],
			"regexp/no-misleading-capturing-group": [
				"error",
				{
					reportBacktrackingEnds: true,
				},
			],
			"regexp/no-misleading-unicode-character": [
				"error",
				{
					fixable: false,
				},
			],
			"regexp/no-missing-g-flag": [
				"error",
				{
					strictTypes: true,
				},
			],
			"regexp/no-non-standard-flag": ["error"],
			"regexp/no-obscure-range": [
				"error",
				{
					allowed: "alphanumeric",
				},
			],
			"regexp/no-octal": ["error"],
			"regexp/no-optional-assertion": ["error"],
			"regexp/no-potentially-useless-backreference": ["error"],
			"regexp/no-standalone-backslash": ["error"],
			"regexp/no-super-linear-backtracking": [
				"error",
				{
					report: "potential",
				},
			],
			"regexp/no-super-linear-move": [
				"error",
				{
					ignoreSticky: false,
					report: "potential",
				},
			],
			"regexp/no-trivially-nested-assertion": ["error"],
			"regexp/no-trivially-nested-quantifier": ["error"],
			"regexp/no-unused-capturing-group": [
				"error",
				{
					allowNamed: false,
					fixable: false,
				},
			],
			"regexp/no-useless-assertions": ["error"],
			"regexp/no-useless-backreference": ["error"],
			"regexp/no-useless-character-class": ["error"],
			"regexp/no-useless-dollar-replacements": ["error"],
			"regexp/no-useless-escape": ["error"],
			"regexp/no-useless-flag": ["error"],
			"regexp/no-useless-lazy": ["error"],
			"regexp/no-useless-non-capturing-group": ["error"],
			"regexp/no-useless-quantifier": ["error"],
			"regexp/no-useless-range": ["error"],
			"regexp/no-useless-set-operand": ["error"],
			"regexp/no-useless-string-literal": ["error"],
			"regexp/no-useless-two-nums-quantifier": ["error"],
			"regexp/no-zero-quantifier": ["error"],
			"regexp/optimal-lookaround-quantifier": ["error"],
			"regexp/optimal-quantifier-concatenation": [
				"error",
				{
					capturingGroups: "report",
				},
			],
			"regexp/prefer-character-class": [
				"error",
				{
					minAlternatives: 2,
				},
			],
			"regexp/prefer-d": [
				"error",
				{
					insideCharacterClass: "range",
				},
			],
			"regexp/prefer-escape-replacement-dollar-char": ["error"],
			"regexp/prefer-lookaround": [
				"error",
				{
					lookbehind: true,
					strictTypes: true,
				},
			],
			"regexp/prefer-named-backreference": ["error"],
			"regexp/prefer-named-capture-group": ["off"],
			"regexp/prefer-named-replacement": ["error"],
			"regexp/prefer-plus-quantifier": ["error"],
			"regexp/prefer-predefined-assertion": ["error"],
			"regexp/prefer-quantifier": ["error"],
			"regexp/prefer-question-quantifier": ["error"],
			"regexp/prefer-range": [
				"error",
				{
					target: "alphanumeric",
				},
			],
			"regexp/prefer-regexp-exec": ["error"],
			"regexp/prefer-regexp-test": ["error"],
			"regexp/prefer-result-array-groups": [
				"error",
				{
					strictTypes: true,
				},
			],
			"regexp/prefer-set-operation": ["error"],
			"regexp/prefer-star-quantifier": ["error"],
			"regexp/prefer-unicode-codepoint-escapes": ["error"],
			"regexp/prefer-w": ["error"],
			"regexp/require-unicode-regexp": ["error"],
			"regexp/require-unicode-sets-regexp": ["off"],
			"regexp/simplify-set-operations": ["error"],
			"regexp/sort-alternatives": ["error"],
			"regexp/sort-character-class-elements": [
				"error",
				{
					order: ["\\s", "\\w", "\\d", "\\p", "*"],
				},
			],
			"regexp/sort-flags": ["error"],
			"regexp/strict": ["error"],
			"regexp/unicode-escape": ["error", "unicodeEscape"],
			"regexp/unicode-property": [
				"error",
				{
					generalCategory: "never",
					key: "long",
					property: {
						binary: "long",
						generalCategory: "long",
						script: "long",
					},
				},
			],
			"regexp/use-ignore-case": ["error"],
		},
	},
	{
		name: "Side effects",
		files: ["src/**/*.js"],
		plugins: { top },
		rules: {
			"top/no-top-level-side-effects": [
				"error",
				{
					allowedCalls: [],
					allowedNews: [],
					allowIIFE: false,
					commonjs: false,
				},
			],
			"top/no-top-level-variables": [
				"error",
				{
					allowed: ["ArrayExpression"],
					kind: ["const"],
				},
			],
		},
	},
	{
		ignores: ["_reports/", ".cache/", ".temp/", "node_modules/"],
	},
];
