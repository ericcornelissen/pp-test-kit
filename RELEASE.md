<!-- SPDX-License-Identifier: CC0-1.0 -->

# Release Guidelines

If you need to release a new version of _pp-test-kit_, follow the guidelines
found in this document.

To publish a new version follow these steps (using `v0.1.2` as an example):

1. Make sure that your local copy of the repository is up-to-date, sync:

   ```shell
   git checkout main
   git pull origin main
   npm clean-install
   ```

   or clone:

   ```shell
   git clone git@gitlab.com:ericcornelissen/pp-test-kit.git
   cd pp-test-kit
   npm clean-install
   ```

1. Pick a new version number in accordance with [Semantic Versioning]. For this
   example we'll use `v0.1.3`.

1. Update the version number in the package manifest and lockfile:

   ```shell
   npm version --no-git-tag-version v0.1.3
   ```

1. Update the changelog manually by add the following text after the
   `## [Unreleased]` line in `CHANGELOG.md`:

   ```markdown
   - _No changes yet_

   ## [0.1.3] - YYYY-MM-DD
   ```

   The date should follow the year-month-day format where single-digit months
   and days should be prefixed with a `0` (e.g. `2022-01-01`).

1. Commit the changes to a new release branch and push using:

   ```shell
   git checkout -b release-$(sha1sum package-lock.json | head -c 7)
   git add CHANGELOG.md package.json package-lock.json
   git commit -m "Version bump"
   git push origin release-$(sha1sum package-lock.json | head -c 7)
   ```

1. Create a Merge Request to merge the release branch into `main`.

1. Merge the Merge Request if the changes look OK and all continuous integration
   checks are passing.

1. Immediately after the Merge Request is merged, sync the `main` branch:

   ```shell
   git checkout main
   git pull origin main
   ```

1. Create a [git tag] for the new version and push:

   ```shell
   git tag v0.1.3
   git push origin v0.1.3
   ```

   **NOTE:** At this point, the continuous delivery automation should pick up
   the release and complete the process. If not, or only partially, continue
   following the remaining steps.

1. Publish to [npm]:

   ```shell
   npm clean-install
   npm publish
   ```

[git tag]: https://git-scm.com/book/en/v2/Git-Basics-Tagging
[npm]: https://www.npmjs.com/
[semantic versioning]: https://semver.org/spec/v2.0.0.html
