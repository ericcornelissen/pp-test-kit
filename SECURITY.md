<!-- SPDX-License-Identifier: CC0-1.0 -->

# Security Policy

The maintainers of the _pp-test-kit_ project take security issues seriously. We
appreciate your efforts to responsibly disclose your findings. Due to the
non-funded and open-source nature of the project, we take a best-efforts
approach when it comes to engaging with security reports.

This document should be considered expired after 2025-06-01. If you are
reading this after that date you should try to find an up-to-date version
in the official source repository.

## Supported Versions

The table below shows which versions of the project are currently supported
with security updates.

| Version | End-of-life |
| ------: | :---------- |
|   0.x.x | -           |

_This table only includes information on versions `<1.0.0`._

## Reporting a Vulnerability

To report a security issue in the latest version of a supported version range,
either (in order of preference):

- [Report it as a Confidential issue][new gitlab issue], or
- Send an email to [security@ericcornelissen.dev] with the terms "SECURITY" and
  "pp-test-kit" in the subject line.

Please do not open a regular issue or Pull Request in the public repository.

To report a security issue in an unsupported version of the project, or if the
latest version of a supported version range isn't affected, please report it
publicly. For example, as a regular issue in the public repository. If in doubt,
report the issue privately.

[new gitlab issue]: https://gitlab.com/ericcornelissen/pp-test-kit/-/issues/new
[security@ericcornelissen.dev]: mailto:security@ericcornelissen.dev?subject=SECURITY%20%28pp-test-kit%29

### What to Include in a Report

Try to include as many of the following items as possible in a security report:

- An explanation of the issue
- A proof of concept exploit
- A suggested severity
- Relevant [CWE] identifiers
- The latest affected version
- The earliest affected version
- A suggested patch
- An automated regression test
- A fuzz input seed or test

[cwe]: https://cwe.mitre.org/

## Advisories

An advisory will be created only if a vulnerability affects at least one
released versions of the project. The affected versions range of an advisory
will by default include all unsupported versions of the project at the time of
disclosure.

| ID  | Date | Affected versions | Patched versions |
| :-- | :--- | :---------------- | :--------------- |
| -   | -    | -                 | -                |

_This table is ordered most to least recent._

## Acknowledgments

We would like to publicly thank the following reporters:

- _None yet_
