<!-- SPDX-License-Identifier: CC0-1.0 -->

# Changelog

All notable changes to _pp-test-kit_ will be documented in this file.

The format is based on [Keep a Changelog], and this project adheres to [Semantic
Versioning].

## [Unreleased]

- Allow access to all Well-Known Symbols in the throwing and manual API.
- Fix inconsistent behavior when `Reflect.get`ting on a wrapped object.

## [0.5.1] - 2024-09-09

- Add support for enumerable properties to the invariant and simulate APIs.
- Add support for wrapping constructors to the throwing and manual API.
- Add wrapping of return value of function calls to the throwing and manual API.

## [0.5.0] - 2024-08-16

- Add support for wrapping functions to the throwing and manual API.
- Correctly handle non-configurable properties with the throwing and manual API.
- Correctly handle read-only properties with the throwing and manual API.

## [0.4.0] - 2024-08-03

- Add detected property count to the manual API.
- Add full access path to the throwing and manual API.
- Add support for enumerable properties to the throwing and manual APIs.
- Add support for enumerable properties to the `withPollution` API.
- Don't report identical accesses multiple times with the manual API.

## [0.3.0] - 2024-07-14

- BREAKING: Change how arguments must be provided to the simulate API.
- Support simulating pollution of multiple properties.
- Add second simulate API that performs actual prototype pollution.
- Correct simulating pollution an object with a `null` prototype.
- Don't report accesses to `__proto__` in missing property access APIs.

## [0.2.0] - 2024-07-13

- BREAKING: Drop support for enumerable property pollution from the invariant
  and simulate APIs.
- Fix bug where the type of the subject was not preserved by the invariant and
  simulate APIs.
- Update simulate and invariant API to simulate pollution on nested objects too.

## [0.1.2] - 2024-06-30

- Add simulate API.
- Add call site information to the invariant API.
- Explicitly check for failure in initial run of the invariant API.
- Include enumerable property pollution in invariant API.

## [0.1.1] - 2024-06-26

- Add invariant API.
- Add call site information for throwing and manual APIs.

## [0.1.0] - 2024-06-04

- Initial release.
