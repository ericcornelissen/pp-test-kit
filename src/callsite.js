// SPDX-License-Identifier: ISC

import callsites from "callsites";

/**
 * Pretty print a call site from the `callsites` package.
 *
 * @param {import("callsites").CallSite} callSite The call site to print.
 * @returns {string} The printed call site.
 */
function printCallSite(callSite) {
	const filename = callSite.getFileName();
	const lineNumber = callSite.getLineNumber();
	const columnNumber = callSite.getColumnNumber();
	return `${filename}:${lineNumber}:${columnNumber}`;
}

/**
 * Get the call stack for the calling function.
 *
 * That is, if A calls B and B calls this function, it will return the stack up
 * to but excluding B.
 *
 * ```
 * function B() {
 *   getCallName();   // This call...
 * }
 * function A() {
 *   B();           // ...will return the call stack at this point.
 * }
 * ```
 *
 * @returns {string[]} The stack with entries as `<filepath>:<line>:<column>`.
 */
export function getStack() {
	const stack = callsites();
	stack.shift();
	stack.shift();
	return stack.map(printCallSite);
}

/**
 * Get the location at which the calling function was called.
 *
 * That is, if A calls B and B calls this function, it will return the location
 * of the call site in A.
 *
 * ```
 * function B() {
 *   getCallSite();   // This call...
 * }
 * function A() {
 *   B();             // ...will return this location.
 * }
 * ```
 *
 * @returns {string} The call site as `<filepath>:<line>:<column>`.
 */
export function getCallSite() {
	const callSite = callsites()[2];
	return printCallSite(callSite);
}
