// SPDX-License-Identifier: ISC

import * as handlers from "./handlers.js";
import * as proxy from "./proxy.js";

/** @typedef {import("./handlers.js").HasProperty} HasProperty */
/** @typedef {import("./handlers.js").onAccessCallback} onAccessCallback */
/** @typedef {import("./handlers.js").Wrapper} Wrapper */

/**
 * @typedef Options
 * @property {boolean} [strict] Consider all non-own property lookups as missing
 * if `true`, otherwise only properties missing from the entire hierarchy will
 * be considered missing.
 */

/**
 * A list of properties that are allowed because otherwise they would just
 * cause noise.
 */
const allowlist = [
	// Property referencing the object's prototype.
	"__proto__",

	// Well-Known Symbols (see https://tc39.es/ecma262/#sec-well-known-symbols)
	Symbol.asyncIterator,
	Symbol.hasInstance,
	Symbol.isConcatSpreadable,
	Symbol.iterator,
	Symbol.match,
	Symbol.matchAll,
	Symbol.replace,
	Symbol.search,
	Symbol.species,
	Symbol.split,
	Symbol.toPrimitive,
	Symbol.toStringTag,
	Symbol.unscopables,
];

/**
 * Create a wrapper for the given `subject` according the specified `options`,
 * using the `onAccess` callback for missing property lookups.
 *
 * @template T
 * @param {Object} p The parameters to create the wrapper with.
 * @param {T} p.subject The object to wrap.
 * @param {onAccessCallback} p.onAccess A callback for missing property lookups.
 * @param {Options} p.options The wrapper options.
 * @returns {T} The wrapped subject.
 * @throws {TypeError} If `subject` cannot be wrapped.
 */
export function create({ subject, onAccess, options }) {
	const has = createHasProperty(options);
	const wrap = createWrap({ has, onAccess });

	const context = { has, onAccess, path: [], wrap };
	context.proxy = proxy.create(subject, handlers.create(context));
	return context.proxy;
}

/**
 * Create the function to wrap objects recursively.
 *
 * @param {Object} p The parameters to create the wrapper with.
 * @param {HasProperty} p.has The property lookup function.
 * @param {onAccessCallback} p.onAccess A callback for missing property lookups.
 * @returns {Wrapper} The function to wrap objects recursively.
 */
function createWrap({ has, onAccess }) {
	const wrap = ({ subject, path }) => {
		const context = { has, onAccess, path, wrap };
		context.proxy = proxy.createChecked(subject, handlers.create(context));
		return context.proxy;
	};

	return wrap;
}

/**
 * Create the function to determine if an object has a given property.
 *
 * @param {Options} options The wrapper options.
 * @returns {HasProperty} The lookup function.
 */
function createHasProperty(options) {
	const strict =
		options && Object.hasOwn(options, "strict") ? options.strict : false;

	const isPresent = strict
		? (target, property) => Object.hasOwn(target, property)
		: (target, property) => {
				let found = false;
				let obj = target;
				while (obj !== null) {
					if (Object.hasOwn(obj, property)) {
						found = true;
						break;
					}

					obj = Object.getPrototypeOf(obj);
				}

				return found;
			};

	return (target, property) => {
		if (allowlist.includes(property)) {
			return true;
		}

		return isPresent(target, property);
	};
}
