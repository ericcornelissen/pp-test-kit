// SPDX-License-Identifier: ISC

/**
 * Create a `Proxy` for an object.
 *
 * @template T
 * @param {T} subject The object to proxy.
 * @param {Object} handler The proxy handler.
 * @returns {T} The proxied object.
 * @throws {TypeError} If `subject` cannot be wrapped.
 */
export function create(subject, handler) {
	return new Proxy(subject, handler);
}

/**
 * Create a `Proxy` for an object but if it cannot be wrapped it is returned as
 * is.
 *
 * @template T
 * @param {T} subject The object to proxy.
 * @param {Object} handler The proxy handler.
 * @returns {T} The proxied object.
 */
export function createChecked(subject, handler) {
	if (subject === null) {
		return subject;
	}

	switch (typeof subject) {
		case "bigint":
		case "boolean":
		case "number":
		case "string":
		case "symbol":
		case "undefined":
			return subject;
		default:
			return new Proxy(subject, handler);
	}
}
