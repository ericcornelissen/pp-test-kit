// SPDX-License-Identifier: ISC

import { getStack, getCallSite } from "./callsite.js";

/**
 * @typedef HandlerContext
 * @property {HasProperty} has A function to determine if an object has a property.
 * @property {onAccessCallback} onAccess The callback for a missing property access..
 * @property {AccessPath} path The access path to reach this handler.
 * @property {Proxy} proxy The proxy the handler belongs to.
 * @property {Wrapper} wrap A function to wrap objects recursively.
 */

/**
 * @typedef {string[]} AccessPath
 */

/**
 * @callback HasProperty
 * @param {Object} target The object to look the property up on.
 * @param {string} property The property to look up on `target`.
 * @returns {boolean} Whether `target` has the `property`.
 */

/**
 * @callback onAccessCallback
 * @param {AccessInfo} accessInfo Information about the property access.
 * @returns {void} Nothing.
 */

/**
 * @typedef AccessInfo
 * @property {string} callSite The call site where the access occurred.
 * @property {any} property The property that was accessed.
 * @property {string} propertyName The name of the accessed property.
 * @property {string} propertyPath The access path of the accessed property.
 */

/**
 * @callback Wrapper
 * @param {any} subject The object to wrap.
 * @param {string[]} path The lookup path thus far.
 * @returns {any} The wrapped `subject`.
 */

/**
 * @typedef ProxyHandler
 * @property {ProxyHandlerApply} apply The handler for the [[apply]] trap.
 * @property {ProxyHandlerConstruct} construct The handler for the [[construct]] trap.
 * @property {ProxyHandlerGet} get The handler for the [[get]] trap.
 * @property {ProxyHandlerGetPrototypeOf} getPrototypeOf The handler for the [[getPrototypeOf]] trap.
 * @property {ProxyHandlerOwnKeys} ownKeys The handler for the [[ownKeys]] trap.
 */

/**
 * Create a handler object for a `new Proxy`.
 *
 * @param {HandlerContext} context The context for the handler.
 * @returns {ProxyHandler} The handler object.
 */
export function create(context) {
	return {
		apply: apply(context),
		construct: construct(context),
		get: get(context),
		getPrototypeOf: getPrototypeOf(context),
		ownKeys: ownKeys(context),
	};
}

/**
 * @callback ProxyHandlerApply
 * @param {Function} target The target function to call.
 * @param {any} thisArgument The value of `this` provided for the call to `target`.
 * @param {any[]} argumentsList An array specifying the arguments with which `target` should be called.
 * @returns {any} The result of calling the given `target` function with the specified `this` value and arguments.
 */

/**
 * Create a callback to handle Proxy [[apply]] traps.
 *
 * @param {HandlerContext} context The context for the handler.
 * @returns {ProxyHandlerApply} The trap callback.
 */
export function apply({ path, wrap }) {
	return (target, thisArgument, argumentsList) => {
		thisArgument = wrap({
			subject: thisArgument,
			path: [...path, "([this])"],
		});
		argumentsList = argumentsList.map((argument, i) =>
			wrap({
				subject: argument,
				path: [...path, `(${"_,".repeat(i)}x)`],
			}),
		);

		const result = Reflect.apply(target, thisArgument, argumentsList);
		return wrap({
			subject: result,
			path: [...path, `()=>{}`],
		});
	};
}

/**
 * @callback ProxyHandlerConstruct
 * @param {Function} target The target function to call.
 * @param {any[]} argumentsList An array specifying the arguments with which `target` should be called.
 * @param {Function} newTarget The value of the `new.target` expression inside `target`.
 * @returns {any} A new instance of `target`, initialized by target as a constructor with the given `argumentsList`.
 */

/**
 * Create a callback to handle Proxy [[construct]] traps.
 *
 * @param {HandlerContext} context The context for the handler.
 * @returns {ProxyHandlerConstruct} The trap callback.
 */
export function construct({ path, wrap }) {
	return (target, argumentsList, newTarget) => {
		argumentsList = argumentsList.map((argument, i) =>
			wrap({
				subject: argument,
				path: [...path, `new(${"_,".repeat(i)}x)`],
			}),
		);

		const instance = Reflect.construct(target, argumentsList, newTarget);
		return wrap({
			subject: instance,
			path: [...path, "new()"],
		});
	};
}

/**
 * @callback ProxyHandlerGet
 * @param {Object} target The target object.
 * @param {any} propertyKey A string or Symbol representing the property name.
 * @param {Object} receiver The `this` value for getters.
 * @returns {any} The property value.
 */

/**
 * Flag to disable on access when we're internally trying to get a property on
 * an object. This side-effect is (_should be_) entirely unobservable.
 */
let disableOnAccessCheck = false; // eslint-disable-line top/no-top-level-variables

/**
 * Create a callback to handle Proxy [[get]] traps.
 *
 * @param {HandlerContext} context The context for the handler.
 * @returns {ProxyHandlerGet} The trap callback.
 */
export function get(context) {
	const { has, onAccess, path, wrap } = context;
	return function (target, propertyKey, receiver) {
		const propertyName = propertyKey.toString();
		const newPath = [...path, propertyName];

		if (
			!disableOnAccessCheck &&
			Object.getPrototypeOf(target) !== null &&
			!has(target, propertyKey)
		) {
			const callSite = getCallSite();
			const property = propertyKey;
			const propertyPath = newPath.join(".");
			onAccess({ callSite, property, propertyName, propertyPath });
		}

		let value;
		try {
			disableOnAccessCheck = true;
			if (receiver === context.proxy) {
				value = target[propertyKey];
			} else {
				value = Reflect.get(target, propertyKey, receiver);
			}
		} finally {
			disableOnAccessCheck = false;
		}

		const descriptor = Reflect.getOwnPropertyDescriptor(target, propertyKey);
		if (descriptor !== undefined && !descriptor.writable) {
			return value;
		}

		return wrap({
			subject: value,
			path: newPath,
		});
	};
}

/**
 * @callback ProxyHandlerGetPrototypeOf
 * @param {Object} target The target object.
 * @returns {Object} The prototype of `target`.
 */

/**
 * Create a callback to handle Proxy [[getPrototypeOf]] traps.
 *
 * @param {HandlerContext} context The context for the handler.
 * @returns {ProxyHandlerGetPrototypeOf} The trap callback.
 */
export function getPrototypeOf(context) {
	const { onAccess, path } = context;
	return (target) => {
		const prototype = Reflect.getPrototypeOf(target);
		if (prototype !== null) {
			const currentStack = getStack();
			if (context[forInCheck]?.(currentStack)) {
				const callSite = getCallSite();
				const property = Symbol.for("__forin__");
				const propertyName = "__forin__";
				const propertyPath = `for...in .${path.join(".")}`;
				onAccess({ callSite, property, propertyName, propertyPath });
			}
		}

		return prototype;
	};
}

/**
 * @callback ProxyHandlerOwnKeys
 * @param {Object} target The target object.
 * @returns {(string | symbol)[]} The own keys of `target`.
 */

/**
 * Create a callback to handle Proxy [[ownKeys]] traps.
 *
 * @param {HandlerContext} context The context for the handler.
 * @returns {ProxyHandlerOwnKeys} The trap callback.
 */
export function ownKeys(context) {
	return (target) => {
		const stackA = getStack();
		context[forInCheck] = (stackB) => stackB.every((b, i) => b === stackA[i]);

		return Reflect.ownKeys(target);
	};
}

/**
 * A unique symbol used internally for communication between the [[ownKeys]] and
 * [[getPrototypeOf]] trap handlers.
 */
export const forInCheck = Symbol(); // eslint-disable-line top/no-top-level-side-effects
