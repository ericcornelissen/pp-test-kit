// SPDX-License-Identifier: ISC

import * as assert from "node:assert/strict";

import * as wrapper from "./wrapper.js";
import { simulatePollution } from "./simulate.js";

/** @typedef {import("node:assert").AssertionError} AssertionError */
/** @typedef {import("./wrapper.js").Options} Options */

/**
 * @callback testFn
 * @param {testCtx} ctx The test context.
 */

/**
 * @typedef testCtx
 * @property {wrapFn} wrap Wraps the provided object for testing purposes.
 */

/**
 * @template T
 * @callback wrapFn
 * @param {T} subject The object to wrap.
 * @param {Options} options The wrapper options.
 * @returns {T} The wrapped object.
 * @throws {TypeError} If `subject` cannot be wrapped.
 */

/**
 * Run the provided function n+1 times. First, once to observe missing property
 * accesses. Then n times, once for each observed missing property with the
 * given property polluted. Finally, once for an enumerable property.
 *
 * This can be used ensure the behavior of the function under test is not
 * changed when a property it might look up is polluted.
 *
 * @example
 * test((ctx) => {
 *   const obj = ctx.wrap({});
 *   f(obj);
 *   // assert an invariant on `f` under pollution
 * });
 * @param {testFn} fn The function to test.
 * @throws {AssertionError} If the initial run of `fn` fails.
 * @throws {AssertionError} If running `fn` with a simulated pollution fails.
 */
export function test(fn) {
	const accesses = new Set();
	const onAccess = accesses.add.bind(accesses);

	const ctx = {
		wrap: (subject, options) => wrapper.create({ subject, onAccess, options }),
	};

	try {
		fn(ctx);
	} catch (error) {
		assert.fail(`Invariant does not hold without pollution: ${error.message}`);
	}

	for (const access of accesses) {
		runWithPollutedProperty(fn, access);
	}
	runWithPollutedEnumerable(fn);
}

/**
 * Run the provided function with the provided property as a polluted property.
 *
 * @param {testFn} fn The function to test.
 * @param {Object} params The pollution parameters.
 * @param {string} params.callSite The site where the property was accessed.
 * @param {any} params.property The property value to pollute with.
 * @param {string} params.propertyName The name of the property to pollute with.
 * @throws {AssertionError} If running `fn` with a simulated pollution fails.
 */
function runWithPollutedProperty(fn, { callSite, property, propertyName }) {
	runWithPollution(fn, {
		callSite,
		property,
		propertyDesc: `the '${propertyName}'`,
	});
}

/**
 * Run the provided function with an enumerable property as a polluted property.
 *
 * @param {testFn} fn The function to test.
 * @throws {AssertionError} If running `fn` with a simulated pollution fails.
 */
function runWithPollutedEnumerable(fn) {
	runWithPollution(fn, {
		enumerable: true,
		// Stryker disable next-line StringLiteral
		property: "__forin__",
		propertyDesc: "an enumerable",
	});
}

/**
 * Run the provided function with the provided property as a polluted property.
 *
 * @param {testFn} fn The function to test.
 * @param {Object} params The pollution parameters.
 * @param {string} [params.callSite] The site where the property was accessed.
 * @param {boolean} [params.enumerable] If the property should be enumerable.
 * @param {any} params.property The property value to pollute with.
 * @param {string} params.propertyDesc A description of `params.property`.
 * @throws {AssertionError} If running `fn` with a simulated pollution fails.
 */
function runWithPollution(
	fn,
	{ callSite, enumerable, property, propertyDesc },
) {
	const ctx = {
		wrap: (subject) =>
			simulatePollution(subject, {
				enumerable,
				property,
				value: "polluted",
			}),
	};

	try {
		fn(ctx);
	} catch (error) {
		assert.fail(
			`Invariant does not hold for ${propertyDesc} property${callSite ? ` (at ${callSite})` : ""}: ${error.message}`,
		);
	}
}
