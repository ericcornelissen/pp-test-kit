// SPDX-License-Identifier: ISC

import * as assert from "node:assert/strict";

import * as wrapper from "./wrapper.js";

/** @typedef {import("node:assert").AssertionError} AssertionError */
/** @typedef {import("./wrapper.js").AccessInfo} AccessInfo */
/** @typedef {import("./wrapper.js").Options} Options */

/**
 * The internal state to keep track of the state per wrapped object.
 *
 * @type {Map<any, Set<AccessInfo>>}
 */
// eslint-disable-next-line top/no-top-level-side-effects, top/no-top-level-variables
const STATES = new Map();

/**
 * Wrap the provided object to monitor it for missing property accesses. Users
 * must check for missing property accesses manually using the {@link check}
 * function.
 *
 * @example
 * const obj = wrap({});
 * f(obj);
 * check(obj);
 * @template T
 * @param {T} subject The object to wrap.
 * @param {Options} options The wrapper options.
 * @returns {T} The wrapped object.
 * @throws {TypeError} If `subject` cannot be wrapped.
 */
export function wrap(subject, options) {
	const state = new Set();
	const onAccess = state.add.bind(state);

	const wrapped = wrapper.create({ subject, onAccess, options });
	STATES.set(wrapped, state);

	return wrapped;
}

/**
 * Check an object wrapped using {@link wrap} for missing property accesses.
 *
 * @param {...Object} objects The wrapped objects to check.
 * @throws {AssertionError} If a missing property was accessed by any object.
 * @throws {AssertionError} If an unwrapped object is provided.
 * @throws {AssertionError} If an already checked object is provided.
 */
export function check(...objects) {
	for (const object of objects) {
		assert.ok(STATES.has(object), "No entry found for object");

		const accesses = STATES.get(object);
		STATES.delete(object);

		const actual = accesses.size;
		const expected = 0;

		const details = Array.from(accesses.values())
			.map(({ callSite, propertyPath }) => `${propertyPath} (at ${callSite})`)
			.filter((a, i, arr) => arr.indexOf(a, i + 1) === -1);

		assert.equal(
			actual,
			expected,
			`Missing property accesses detected (${details.length}):\n\t${details.join("\n\t")}`,
		);
	}
}
