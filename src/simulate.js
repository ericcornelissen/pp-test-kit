// SPDX-License-Identifier: ISC

import * as proxy from "./proxy.js";

/**
 * @typedef Options
 * @property {boolean} [enumerable=false] Whether or not the property should be
 * enumerable.
 * @property {any} property The name of the property to pollute.
 * @property {any} [value] The value to pollute with. The default value is a
 * random string.
 */

/**
 * Simulate the effect of prototype pollution with a specific property on a
 * specific object.
 *
 * @example
 * const subject = {};
 * const obj = simulatePollution(subject, { property: "foo", value: "bar" });
 * obj.foo; // is "bar"
 * @template T
 * @param {T} subject The object to wrap.
 * @param {...Options} optionsList The simulation options.
 * @returns {T} The subject with pollution simulated.
 * @throws {TypeError} If `subject` cannot be wrapped.
 * @throws {TypeError} If any of the options are invalid.
 */
export function simulatePollution(subject, ...optionsList) {
	checkOptions(...optionsList);
	const handler = createHandler(...optionsList);
	return proxy.create(subject, handler);
}

/**
 * @template T
 * @template E
 * @callback testFn
 * @returns {T}
 * @throws {E}
 */

/**
 * Run a test function under the effect of prototype pollution of a specific
 * property.
 *
 * @template T
 * @template E
 * @param {testFn<T,E>} fn The function to test.
 * @param {...Options} optionsList The simulation options.
 * @returns {T} The return value of `fn`.
 * @throws {E} If `fn` throws.
 * @throws {TypeError} If any of the options are invalid.
 */
export function withPollution(fn, ...optionsList) {
	checkOptions(...optionsList);

	const restore = [];
	for (const options of optionsList) {
		const enumerable = Object.hasOwn(options, "enumerable")
			? options.enumerable
			: false;
		const property = options.property;
		const value = Object.hasOwn(options, "value") ? options.value : "polluted";

		if (Object.hasOwn(Object.prototype, property)) {
			restore.push({
				property,
				descriptor: Object.getOwnPropertyDescriptor(Object.prototype, property),
			});
		}

		Object.defineProperty(Object.prototype, property, {
			configurable: true,
			enumerable,
			value,
			writable: true,
		});
	}

	let result;
	let error;
	try {
		result = fn();
	} catch (err) {
		error = err;
	} finally {
		const cleanup = () => {
			for (const options of optionsList) {
				delete Object.prototype[options.property];
			}

			for (const { property, descriptor } of restore) {
				Object.defineProperty(Object.prototype, property, descriptor);
			}
		};

		if (result instanceof Promise) {
			result.then(cleanup).catch(cleanup);
		} else {
			cleanup();
		}
	}

	if (error) {
		throw error;
	} else {
		return result;
	}
}

/**
 * Simulate the effect of prototype pollution with a specific property on a
 * specific object, if the given subject is an object. Otherwise the subject is
 * returned as is.
 *
 * The default value is a random string.
 *
 * @template T
 * @param {T} subject The object to wrap.
 * @param {...Options} optionsList The simulation options.
 * @returns {T} The subject with pollution simulated.
 */
function createChecked(subject, ...optionsList) {
	const handler = createHandler(...optionsList);
	return proxy.createChecked(subject, handler);
}

/**
 * Create a `Proxy` handler according to the specified parameter.
 *
 * @param {...Options} optionsList The simulation options.
 * @returns {Object} A `Proxy` handler.
 */
function createHandler(...optionsList) {
	return {
		get(target, property, _receiver) {
			if (
				!Object.hasOwn(target, property) &&
				Object.getPrototypeOf(target) !== null
			) {
				const options = optionsList.find(
					(options) => options.property === property,
				);

				if (options) {
					return Object.hasOwn(options, "value") ? options.value : "polluted";
				}
			}

			return createChecked(target[property], ...optionsList);
		},
		getPrototypeOf(target) {
			const prototype = Reflect.getPrototypeOf(target);
			if (prototype === null) {
				return prototype;
			}

			return new Proxy(prototype, {
				getOwnPropertyDescriptor(target, property) {
					const options = optionsList
						.filter((options) => options.enumerable)
						.find((options) => options.property === property);
					if (options !== undefined) {
						return {
							value: options.value,
							enumerable: true,
							configurable: true,
						};
					}

					return Reflect.getOwnPropertyDescriptor(target, property);
				},
				ownKeys(target) {
					const keys = optionsList
						.filter((options) => options.enumerable)
						.map((options) => options.property);
					return [...Reflect.ownKeys(target), ...keys];
				},
			});
		},
	};
}

/**
 * Check if the provided options are valid.
 *
 * @param {...Options} optionsList The simulation options.
 * @throws {TypeError} If any of the options are invalid.
 */
function checkOptions(...optionsList) {
	for (const options of optionsList) {
		if (!Object.hasOwn(options, "property")) {
			throw new TypeError("invalid options, missing 'property'");
		}
	}
}
