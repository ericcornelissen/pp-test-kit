// SPDX-License-Identifier: ISC

import * as assert from "node:assert/strict";

import * as wrapper from "./wrapper.js";

/** @typedef {import("node:assert").AssertionError} AssertionError */
/** @typedef {import("./wrapper.js").onAccessCallback} onAccessCallback */
/** @typedef {import("./wrapper.js").Options} Options */

/**
 * Wrap the provided object to monitor it for missing property accesses. When
 * a missing property access is detected an error will be thrown.
 *
 * The stack trace of the error thrown for a missing property access can be used
 * to locate the line of code that performed the lookup.
 *
 * @example
 * const obj = wrap({});
 * f(obj);
 * @template T
 * @param {T} subject The object to wrap.
 * @param {Options} options The wrapper options.
 * @returns {T} The wrapped object.
 * @throws {TypeError} If `subject` cannot be wrapped.
 */
export function wrap(subject, options) {
	return wrapper.create({ subject, onAccess, options });
}

/**
 * The callback for a property access.
 *
 * @type {onAccessCallback}
 */
function onAccess({ callSite, propertyPath }) {
	assert.fail(
		`Missing property lookup detected: ${propertyPath} (at ${callSite})`,
	);
}
