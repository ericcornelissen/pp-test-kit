// SPDX-License-Identifier: ISC

import * as assert from "node:assert/strict";
import { test } from "node:test";

import escapeStringRegexp from "escape-string-regexp";
import * as fc from "fast-check";

import * as testCases from "./testcases/all.js";
import * as helpers from "./helpers.js";

import * as lib from "../src/wrapper.js";

const arbitrary = {
	subject: () =>
		fc.anything({
			values: [fc.constant({})],
			withBigInt: false,
			withBoxedValues: true,
			withDate: true,
			withMap: true,
			withSet: true,
			withTypedArray: true,
			withSparseArray: true,
		}),
	options: () =>
		fc.record(
			{
				strict: fc.boolean(),
			},
			{
				requiredKeys: [],
			},
		),
};

test("Core wrapper", async (t) => {
	await t.test("subject unsupported", async (t) => {
		await t.test("direct", async (t) => {
			for (const testCase of testCases.unsupported) {
				const { subject, testName } = testCase;

				await t.test(testName, () => {
					assert.throws(() => {
						lib.create({ subject, onAccess });
					});
				});
			}
		});

		await t.test("nested", async (t) => {
			for (const testCase of testCases.unsupported) {
				const { subject, testName } = testCase;

				{
					const property = "name";
					const nestedSubject = { [property]: subject };

					await t.test(`${testName} nested in an object`, () => {
						assert.doesNotThrow(() => {
							const wrapped = lib.create({ subject: nestedSubject, onAccess });
							wrapped[property];
						});
					});
				}

				{
					const index = 0;
					const nestedSubject = [subject];

					await t.test(`${testName} nested in an array`, () => {
						assert.doesNotThrow(() => {
							const wrapped = lib.create({ subject: nestedSubject, onAccess });
							wrapped[index];
						});
					});
				}
			}
		});
	});

	await t.test("subject supported", async (t) => {
		const cases = [
			...testCases.edgeCases,
			...testCases.missing,
			...testCases.present,
			...testCases.inherited,
			...testCases.shadowed,
		];

		for (const testCase of cases) {
			const { accesses, expected, expectedPath, subject, testName } = testCase;

			await t.test(testName, () => {
				const accessed = [];
				const onAccess = accessed.push.bind(accessed);
				const options = { strict: true };

				const wrapped = lib.create({ subject, onAccess, options });
				helpers.access(wrapped, accesses);

				for (const i in accessed) {
					const actual = accessed[i];
					assert.equal(actual.property, expected[i]);
					assert.equal(actual.propertyName, expected[i].toString());
					assert.equal(
						actual.propertyPath,
						expectedPath ? expectedPath[i] : expected[i].toString(),
					);

					const location = escapeStringRegexp(helpers.access.location);
					assert.match(
						actual.callSite,
						new RegExp(`${location}:\\d+:\\d+`, "u"),
					);
				}
			});
		}

		await t.test("wrapped prototype", async (t) => {
			await t.test("Object.getPrototypeOf", () => {
				fc.assert(
					fc.property(
						arbitrary.subject(),
						arbitrary.options(),
						(subject, options) => {
							const accessed = [];
							const onAccess = accessed.push.bind(accessed);

							const wrappedSubject = lib.create({ subject, onAccess, options });
							Object.getPrototypeOf(wrappedSubject);

							assert.deepEqual(accessed, []);
						},
					),
				);
			});

			await t.test("Reflect.getPrototypeOf", () => {
				fc.assert(
					fc.property(
						arbitrary.subject(),
						arbitrary.options(),
						(subject, options) => {
							const accessed = [];
							const onAccess = accessed.push.bind(accessed);

							const wrappedSubject = lib.create({ subject, onAccess, options });
							Reflect.getPrototypeOf(wrappedSubject);

							assert.deepEqual(accessed, []);
						},
					),
				);
			});

			await t.test("Object.prototype.isPrototypeOf", () => {
				fc.assert(
					fc.property(
						arbitrary.subject(),
						arbitrary.options(),
						(subject, options) => {
							const accessed = [];
							const onAccess = accessed.push.bind(accessed);

							const wrappedSubject = lib.create({ subject, onAccess, options });
							({}).isPrototypeOf(wrappedSubject);

							assert.deepEqual(accessed, []);
						},
					),
				);
			});
		});

		await t.test("wrapped type", async (t) => {
			await t.test("instanceof", () => {
				fc.assert(
					fc.property(
						arbitrary.subject(),
						arbitrary.options(),
						(subject, options) => {
							const accessed = [];
							const onAccess = accessed.push.bind(accessed);

							const wrappedSubject = lib.create({ subject, onAccess, options });
							wrappedSubject instanceof Object;

							assert.deepEqual(accessed, []);
						},
					),
				);
			});

			await t.test("typeof wrapped", () => {
				fc.assert(
					fc.property(
						arbitrary.subject(),
						arbitrary.options(),
						(subject, options) => {
							const wrapped = lib.create({ subject, onAccess, options });
							assert.equal(typeof subject, typeof wrapped);
						},
					),
				);
			});

			await t.test("Array.isArray(wrapped)", () => {
				fc.assert(
					fc.property(
						fc.oneof(
							fc.array(fc.anything()), // .5 bias for creating arrays
							arbitrary.subject(),
						),
						arbitrary.options(),
						(subject, options) => {
							const wrapped = lib.create({ subject, onAccess, options });
							assert.equal(Array.isArray(subject), Array.isArray(wrapped));
						},
					),
				);
			});
		});

		await t.test("enumerability", async (t) => {
			await t.test("Object.keys", () => {
				const accessed = [];
				const onAccess = accessed.push.bind(accessed);

				const subject = {};
				const wrappedSubject = lib.create({ subject, onAccess });

				const actual = Object.keys(wrappedSubject);
				const expected = Object.keys(subject);

				assert.deepEqual(actual, expected);
				assert.deepEqual(accessed, []);
			});

			await t.test("Object.values", () => {
				const accessed = [];
				const onAccess = accessed.push.bind(accessed);

				const subject = {};
				const wrappedSubject = lib.create({ subject, onAccess });

				const actual = Object.values(wrappedSubject);
				const expected = Object.values(subject);

				assert.deepEqual(actual, expected);
				assert.deepEqual(accessed, []);
			});

			await t.test("Object.entries", () => {
				const accessed = [];
				const onAccess = accessed.push.bind(accessed);

				const subject = {};
				const wrappedSubject = lib.create({ subject, onAccess });

				const actual = Object.entries(wrappedSubject);
				const expected = Object.entries(subject);

				assert.deepEqual(actual, expected);
				assert.deepEqual(accessed, []);
			});

			await t.test("Object.getOwnPropertyNames", () => {
				const accessed = [];
				const onAccess = accessed.push.bind(accessed);

				const subject = {};
				const wrappedSubject = lib.create({ subject, onAccess });

				const actual = Object.getOwnPropertyNames(wrappedSubject);
				const expected = Object.getOwnPropertyNames(subject);

				assert.deepEqual(actual, expected);
				assert.deepEqual(accessed, []);
			});

			await t.test("Object.getOwnPropertySymbols", () => {
				const accessed = [];
				const onAccess = accessed.push.bind(accessed);

				const subject = {};
				const wrappedSubject = lib.create({ subject, onAccess });

				const actual = Object.getOwnPropertySymbols(wrappedSubject);
				const expected = Object.getOwnPropertySymbols(subject);

				assert.deepEqual(actual, expected);
				assert.deepEqual(accessed, []);
			});

			await t.test("Object.getOwnPropertyDescriptors", () => {
				const accessed = [];
				const onAccess = accessed.push.bind(accessed);

				const subject = {};
				const wrappedSubject = lib.create({ subject, onAccess });

				const actual = Object.getOwnPropertyDescriptors(wrappedSubject);
				const expected = Object.getOwnPropertyDescriptors(subject);

				assert.deepEqual(actual, expected);
				assert.deepEqual(accessed, []);
			});

			await t.test("Reflect.ownKeys", () => {
				const accessed = [];
				const onAccess = accessed.push.bind(accessed);

				const subject = {};
				const wrappedSubject = lib.create({ subject, onAccess });

				const actual = Reflect.ownKeys(wrappedSubject);
				const expected = Reflect.ownKeys(subject);

				assert.deepEqual(actual, expected);
				assert.deepEqual(accessed, []);
			});

			await t.test("for...in", async (t) => {
				await t.test("regular object", () => {
					const accessed = [];
					const onAccess = accessed.push.bind(accessed);

					const subject = {};
					const wrappedSubject = lib.create({ subject, onAccess });

					const actual = [];
					for (const key in wrappedSubject) {
						actual.push(key);
					}

					const expected = [];
					for (const key in subject) {
						expected.push(key);
					}

					assert.deepEqual(actual, expected);
					assert.equal(accessed.length, 1);
					assert.equal(accessed[0].property, Symbol.for("__forin__"));
					assert.equal(accessed[0].propertyName, "__forin__");
					assert.equal(accessed[0].propertyPath, "for...in .");
					assert.match(
						accessed[0].callSite,
						new RegExp(`${escapeStringRegexp(import.meta.url)}:\\d+:\\d+`, "u"),
					);
				});

				await t.test("null prototype", () => {
					const accessed = [];
					const onAccess = accessed.push.bind(accessed);

					const subject = Object.create(null);
					const wrappedSubject = lib.create({ subject, onAccess });

					const actual = [];
					for (const key in wrappedSubject) {
						actual.push(key);
					}

					const expected = [];
					for (const key in subject) {
						expected.push(key);
					}

					assert.deepEqual(actual, expected);
					assert.deepEqual(accessed, []);
				});
			});

			await t.test("Object.assign", () => {
				const accessed = [];
				const onAccess = accessed.push.bind(accessed);

				const subject = {};
				const wrappedSubject = lib.create({ subject, onAccess });

				const actual = Object.assign({}, wrappedSubject);
				const expected = Object.assign({}, subject);

				assert.deepEqual(actual, expected);
				assert.deepEqual(accessed, []);
			});

			await t.test("{...object}", () => {
				const accessed = [];
				const onAccess = accessed.push.bind(accessed);

				const subject = {};
				const wrappedSubject = lib.create({ subject, onAccess });

				const actual = { ...wrappedSubject };
				const expected = { ...subject };

				assert.deepEqual(actual, expected);
				assert.deepEqual(accessed, []);
			});
		});

		await t.test("configured properties", async (t) => {
			await t.test("read-only property", () => {
				const property = "foo";
				const subject = {};
				Object.defineProperty(subject, property, {
					value: {},
					writable: false,
				});

				const wrappedSubject = lib.create({ subject, onAccess });

				assert.doesNotThrow(() => {
					wrappedSubject[property];
				});
			});

			await t.test("non-configurable property", () => {
				const property = "foo";
				const subject = {};
				Object.defineProperty(subject, property, {
					configurable: false,
					value: {},
				});

				const wrappedSubject = lib.create({ subject, onAccess });

				assert.doesNotThrow(() => {
					wrappedSubject[property];
				});
			});

			await t.test("read-only & non-configurable property", () => {
				const property = "foo";
				const subject = {};
				Object.defineProperty(subject, property, {
					configurable: false,
					value: {},
					writable: false,
				});

				const wrappedSubject = lib.create({ subject, onAccess });

				assert.doesNotThrow(() => {
					wrappedSubject[property];
				});
			});
		});

		await t.test("call", async (t) => {
			await t.test("function call", async (t) => {
				await t.test("first argument", () => {
					const accessed = [];
					const onAccess = accessed.push.bind(accessed);

					const subject = (obj) => obj.foo;
					const wrappedSubject = lib.create({ subject, onAccess });

					wrappedSubject({});

					assert.equal(accessed.length, 1);
					assert.equal(accessed[0].property, "foo");
					assert.equal(accessed[0].propertyName, "foo");
					assert.equal(accessed[0].propertyPath, "(x).foo");
					assert.match(
						accessed[0].callSite,
						new RegExp(`${escapeStringRegexp(import.meta.url)}:\\d+:\\d+`, "u"),
					);
				});

				await t.test("second argument", () => {
					const accessed = [];
					const onAccess = accessed.push.bind(accessed);

					const subject = (_, obj) => obj.bar;
					const wrappedSubject = lib.create({ subject, onAccess });

					wrappedSubject({}, {});

					assert.equal(accessed.length, 1);
					assert.equal(accessed[0].property, "bar");
					assert.equal(accessed[0].propertyName, "bar");
					assert.equal(accessed[0].propertyPath, "(_,x).bar");
					assert.match(
						accessed[0].callSite,
						new RegExp(`${escapeStringRegexp(import.meta.url)}:\\d+:\\d+`, "u"),
					);
				});

				await t.test("thisArg", () => {
					const accessed = [];
					const onAccess = accessed.push.bind(accessed);

					const subject = {
						fn() {
							return this.baz;
						},
					};
					const wrappedSubject = lib.create({ subject, onAccess });

					wrappedSubject.fn();

					assert.equal(accessed.length, 1);
					assert.equal(accessed[0].property, "baz");
					assert.equal(accessed[0].propertyName, "baz");
					assert.equal(accessed[0].propertyPath, "fn.([this]).baz");
					assert.match(
						accessed[0].callSite,
						new RegExp(`${escapeStringRegexp(import.meta.url)}:\\d+:\\d+`, "u"),
					);
				});

				await t.test("return value", () => {
					const accessed = [];
					const onAccess = accessed.push.bind(accessed);

					const subject = () => ({});
					const wrappedSubject = lib.create({ subject, onAccess });

					const result = wrappedSubject();
					result.foobar;

					assert.equal(accessed.length, 1);
					assert.equal(accessed[0].property, "foobar");
					assert.equal(accessed[0].propertyName, "foobar");
					assert.equal(accessed[0].propertyPath, "()=>{}.foobar");
					assert.match(
						accessed[0].callSite,
						new RegExp(`${escapeStringRegexp(import.meta.url)}:\\d+:\\d+`, "u"),
					);
				});
			});

			await t.test("Reflect.apply", async (t) => {
				await t.test("first argument", () => {
					const accessed = [];
					const onAccess = accessed.push.bind(accessed);

					const subject = (obj) => obj.foo;
					const wrappedSubject = lib.create({ subject, onAccess });

					Reflect.apply(wrappedSubject, null, [{}]);

					assert.equal(accessed.length, 1);
					assert.equal(accessed[0].property, "foo");
					assert.equal(accessed[0].propertyName, "foo");
					assert.equal(accessed[0].propertyPath, "(x).foo");
					assert.match(
						accessed[0].callSite,
						new RegExp(`${escapeStringRegexp(import.meta.url)}:\\d+:\\d+`, "u"),
					);
				});

				await t.test("second argument", () => {
					const accessed = [];
					const onAccess = accessed.push.bind(accessed);

					const subject = (_, obj) => obj.bar;
					const wrappedSubject = lib.create({ subject, onAccess });

					Reflect.apply(wrappedSubject, null, [{}, {}]);

					assert.equal(accessed.length, 1);
					assert.equal(accessed[0].property, "bar");
					assert.equal(accessed[0].propertyName, "bar");
					assert.equal(accessed[0].propertyPath, "(_,x).bar");
					assert.match(
						accessed[0].callSite,
						new RegExp(`${escapeStringRegexp(import.meta.url)}:\\d+:\\d+`, "u"),
					);
				});

				await t.test("thisArg", () => {
					const accessed = [];
					const onAccess = accessed.push.bind(accessed);

					const subject = {
						fn() {
							return this.baz;
						},
					};
					const wrappedSubject = lib.create({ subject, onAccess });

					Reflect.apply(wrappedSubject.fn, {}, []);

					assert.equal(accessed.length, 1);
					assert.equal(accessed[0].property, "baz");
					assert.equal(accessed[0].propertyName, "baz");
					assert.equal(accessed[0].propertyPath, "fn.([this]).baz");
					assert.match(
						accessed[0].callSite,
						new RegExp(`${escapeStringRegexp(import.meta.url)}:\\d+:\\d+`, "u"),
					);
				});

				await t.test("return value", () => {
					const accessed = [];
					const onAccess = accessed.push.bind(accessed);

					const subject = () => ({});
					const wrappedSubject = lib.create({ subject, onAccess });

					const result = Reflect.apply(wrappedSubject, null, []);
					result.foobar;

					assert.equal(accessed.length, 1);
					assert.equal(accessed[0].property, "foobar");
					assert.equal(accessed[0].propertyName, "foobar");
					assert.equal(accessed[0].propertyPath, "()=>{}.foobar");
					assert.match(
						accessed[0].callSite,
						new RegExp(`${escapeStringRegexp(import.meta.url)}:\\d+:\\d+`, "u"),
					);
				});
			});
		});

		await t.test("construct", async (t) => {
			await t.test("new target", async (t) => {
				await t.test("first argument", () => {
					const accessed = [];
					const onAccess = accessed.push.bind(accessed);

					class subject {
						constructor(obj) {
							obj.foo;
						}
					}
					const wrappedSubject = lib.create({ subject, onAccess });

					new wrappedSubject({});

					assert.equal(accessed.length, 1);
					assert.equal(accessed[0].property, "foo");
					assert.equal(accessed[0].propertyName, "foo");
					assert.equal(accessed[0].propertyPath, "new(x).foo");
					assert.match(
						accessed[0].callSite,
						new RegExp(`${escapeStringRegexp(import.meta.url)}:\\d+:\\d+`, "u"),
					);
				});

				await t.test("second argument", () => {
					const accessed = [];
					const onAccess = accessed.push.bind(accessed);

					class subject {
						constructor(_, obj) {
							obj.bar;
						}
					}
					const wrappedSubject = lib.create({ subject, onAccess });

					new wrappedSubject({}, {});

					assert.equal(accessed.length, 1);
					assert.equal(accessed[0].property, "bar");
					assert.equal(accessed[0].propertyName, "bar");
					assert.equal(accessed[0].propertyPath, "new(_,x).bar");
					assert.match(
						accessed[0].callSite,
						new RegExp(`${escapeStringRegexp(import.meta.url)}:\\d+:\\d+`, "u"),
					);
				});

				await t.test("instance", () => {
					const accessed = [];
					const onAccess = accessed.push.bind(accessed);

					class subject {
						constructor() {}
					}
					const wrappedSubject = lib.create({ subject, onAccess });

					const instance = new wrappedSubject();
					instance.foobar;

					assert.equal(accessed.length, 1);
					assert.equal(accessed[0].property, "foobar");
					assert.equal(accessed[0].propertyName, "foobar");
					assert.equal(accessed[0].propertyPath, "new().foobar");
					assert.match(
						accessed[0].callSite,
						new RegExp(`${escapeStringRegexp(import.meta.url)}:\\d+:\\d+`, "u"),
					);
				});
			});

			await t.test("Reflect.construct", async (t) => {
				await t.test("first argument", () => {
					const accessed = [];
					const onAccess = accessed.push.bind(accessed);

					class subject {
						constructor(obj) {
							obj.foo;
						}
					}
					const wrappedSubject = lib.create({ subject, onAccess });

					Reflect.construct(wrappedSubject, [{}]);

					assert.equal(accessed.length, 1);
					assert.equal(accessed[0].property, "foo");
					assert.equal(accessed[0].propertyName, "foo");
					assert.equal(accessed[0].propertyPath, "new(x).foo");
					assert.match(
						accessed[0].callSite,
						new RegExp(`${escapeStringRegexp(import.meta.url)}:\\d+:\\d+`, "u"),
					);
				});

				await t.test("second argument", () => {
					const accessed = [];
					const onAccess = accessed.push.bind(accessed);

					class subject {
						constructor(_, obj) {
							obj.bar;
						}
					}
					const wrappedSubject = lib.create({ subject, onAccess });

					Reflect.construct(wrappedSubject, [{}, {}]);

					assert.equal(accessed.length, 1);
					assert.equal(accessed[0].property, "bar");
					assert.equal(accessed[0].propertyName, "bar");
					assert.equal(accessed[0].propertyPath, "new(_,x).bar");
					assert.match(
						accessed[0].callSite,
						new RegExp(`${escapeStringRegexp(import.meta.url)}:\\d+:\\d+`, "u"),
					);
				});

				await t.test("instance", () => {
					const accessed = [];
					const onAccess = accessed.push.bind(accessed);

					class subject {
						constructor() {}
					}
					const wrappedSubject = lib.create({ subject, onAccess });

					const instance = Reflect.construct(wrappedSubject, []);
					instance.foobar;

					assert.equal(accessed.length, 1);
					assert.equal(accessed[0].property, "foobar");
					assert.equal(accessed[0].propertyName, "foobar");
					assert.equal(accessed[0].propertyPath, "new().foobar");
					assert.match(
						accessed[0].callSite,
						new RegExp(`${escapeStringRegexp(import.meta.url)}:\\d+:\\d+`, "u"),
					);
				});
			});
		});

		await t.test("edge cases", async (t) => {
			await t.test("null prototype", () => {
				fc.assert(
					fc.property(
						arbitrary
							.subject()
							.map((obj) => Object.assign(Object.create(null), obj)),
						arbitrary.options(),
						fc.string(),
						(subject, options, property) => {
							const accessed = [];
							const onAccess = accessed.push.bind(accessed);

							const wrappedSubject = lib.create({ subject, onAccess, options });
							wrappedSubject[property];

							assert.deepEqual(accessed, []);
						},
					),
				);
			});

			await t.test("accessing __proto__", () => {
				fc.assert(
					fc.property(
						arbitrary.subject(),
						arbitrary.options(),
						(subject, options) => {
							const accessed = [];
							const onAccess = accessed.push.bind(accessed);

							const wrappedSubject = lib.create({ subject, onAccess, options });
							wrappedSubject.__proto__;

							assert.deepEqual(accessed, []);
						},
					),
				);
			});

			await t.test("Reflect.get", () => {
				const property = "foo";
				const value = "bar";

				const subject = {};
				const receiver = { [property]: value };

				Object.defineProperty(subject, property, {
					get() {
						return this[property];
					},
				});

				const wrappedSubject = lib.create({ subject, onAccess });

				const actual = Reflect.get(wrappedSubject, property, receiver);
				const expected = Reflect.get(subject, property, receiver);
				assert.deepEqual(actual, expected);
			});

			await t.test("ownKeys->getPrototypeOf", () => {
				const accessed = [];
				const onAccess = accessed.push.bind(accessed);

				const subject = {};
				const wrappedSubject = lib.create({ subject, onAccess });

				Reflect.ownKeys(wrappedSubject);
				Reflect.getPrototypeOf(wrappedSubject);

				assert.deepEqual(accessed, []);
			});
		});
	});

	await t.test("Well-Known Symbols", async (t) => {
		const testCases = [
			{
				name: "asyncIterator",
				action: async (wrapped) => {
					try {
						for await (const _ of wrapped) {
							// nothing to do
						}
					} catch (_) {
						// it does not matter if wrapped is actually (async) iterable
					}
				},
			},
			{
				name: "hasInstance",
				action: (wrapped) => {
					try {
						({}) instanceof wrapped;
					} catch (_) {
						// it does not matter if wrapper can be the RHS of instanceof
					}
				},
			},
			{
				name: "isConcatSpreadable",
				action: (wrapped) => {
					try {
						[].concat(wrapped);
					} catch (_) {
						// it does not matter if wrapped is spreadable
					}
				},
			},
			{
				name: "iterator",
				action: (wrapped) => {
					try {
						for (const _ of wrapped) {
							// nothing to do
						}
					} catch (_) {
						// it does not matter if wrapped is actually iterable
					}
				},
			},
			{
				name: "match",
				action: (wrapped) => {
					// eslint-disable-next-line regexp/prefer-regexp-exec
					"".match(wrapped);
				},
			},
			{
				name: "matchAll",
				action: (wrapped) => {
					"".matchAll(wrapped);
				},
			},
			{
				name: "replace",
				action: (wrapped) => {
					"".replace(wrapped, "");
				},
			},
			{
				name: "search",
				action: (wrapped) => {
					"".search(wrapped, "");
				},
			},
			{
				name: "species",
				action: (wrapped) => {
					// These will be accessed to but are out of scope, so we need to
					// define them.
					wrapped.flags = "g";
					wrapped.lastIndex = 0;

					RegExp.prototype[Symbol.matchAll].call(wrapped, "");
				},
			},
			{
				name: "split",
				action: (wrapped) => {
					"".split(wrapped, 1);
				},
			},
			{
				name: "toPrimitive",
				action: (wrapped) => {
					+wrapped;
				},
			},
			{
				name: "toStringTag",
				action: (wrapped) => {
					`it is: ${wrapped}`;
				},
			},
			{
				name: "unscopables",
				action: (wrapped) => {
					// test directly since we can't use `with` in ESModules
					wrapped[Symbol.unscopables];
				},
			},
		];

		for (const testCase of testCases) {
			await t.test(testCase.name, async () => {
				const accessed = [];
				const onAccess = accessed.push.bind(accessed);

				const subject = {};
				const wrapped = lib.create({ subject, onAccess });

				testCase.action(wrapped);
				assert.deepEqual(accessed, []);
			});
		}
	});
});

/**
 * A fake access listener to use when accesses can be ignored.
 */
function onAccess() {
	// Nothing to do
}
