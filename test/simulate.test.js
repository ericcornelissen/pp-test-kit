// SPDX-License-Identifier: ISC

import * as assert from "node:assert/strict";
import { test } from "node:test";

import * as fc from "fast-check";

import * as lib from "../src/simulate.js";

const arbitrary = {
	subject: () =>
		fc.anything({
			values: [fc.constant({})],
			withBigInt: false,
			withBoxedValues: true,
			withDate: true,
			withMap: true,
			withSet: true,
			withTypedArray: false,
			withSparseArray: false,
		}),
	optionsList: ({ withValues } = {}) =>
		fc.uniqueArray(
			fc.record(
				{
					enumerable: fc.boolean(),
					property: fc.string(),
					value: fc.string(),
				},
				{
					requiredKeys: ["property", ...(withValues ? ["value"] : [])],
				},
			),
			{
				minLength: 1,
				selector: (options) => options.property,
			},
		),
};

test("simulatePollution", async (t) => {
	await t.test("property missing", async (t) => {
		await t.test("with value", () => {
			fc.assert(
				fc.property(
					arbitrary.subject(),
					arbitrary.optionsList({ withValues: true }),
					(subject, optionsList) => {
						for (const options of optionsList) {
							fc.pre(!Object.hasOwn(subject, options.property));
						}

						const simulated = lib.simulatePollution(subject, ...optionsList);

						for (const options of optionsList) {
							assert.ok(!Object.hasOwn(simulated, options.property));
							assert.equal(simulated[options.property], options.value);
						}
					},
				),
			);
		});

		await t.test("without value", () => {
			fc.assert(
				fc.property(
					arbitrary.subject(),
					arbitrary.optionsList(),
					(subject, optionsList) => {
						for (const options of optionsList) {
							fc.pre(!Object.hasOwn(subject, options.property));
							delete options.value;
						}

						const simulated = lib.simulatePollution(subject, ...optionsList);

						for (const options of optionsList) {
							assert.ok(!Object.hasOwn(simulated, options.property));
							assert.equal(simulated[options.property], "polluted");
						}
					},
				),
			);
		});

		await t.test("not the polluted property", () => {
			fc.assert(
				fc.property(
					arbitrary.subject(),
					arbitrary.optionsList().map(([options]) => options),
					fc.string(),
					(subject, options, access) => {
						fc.pre(subject[access] === undefined);
						fc.pre(options.property !== access);

						const simulated = lib.simulatePollution(subject, options);

						assert.equal(simulated[access], undefined);
					},
				),
			);
		});
	});

	await t.test("property present", () => {
		fc.assert(
			fc.property(
				arbitrary.subject(),
				arbitrary.optionsList().map(([options]) => options),
				fc.string(),
				(subject, options, shadow) => {
					fc.pre(
						Object.getOwnPropertyDescriptor(subject, options.property)
							?.configurable !== false,
					);
					fc.pre(options.property !== "__proto__");
					fc.pre(options.value !== shadow);

					subject[options.property] = shadow;
					const simulated = lib.simulatePollution(subject, options);

					assert.ok(Object.hasOwn(simulated, options.property));
					assert.equal(simulated[options.property], shadow);
				},
			),
		);
	});

	await t.test("property inherited", () => {
		fc.assert(
			fc.property(arbitrary.subject(), fc.string(), (subject, value) => {
				const property = "toString";

				fc.pre(!Object.hasOwn(subject, property));

				const simulated = lib.simulatePollution(subject, { property, value });

				assert.ok(!Object.hasOwn(simulated, property));
				assert.equal(simulated[property], value);
			}),
		);
	});

	await t.test("properties mixed", async (t) => {
		await t.test("missing & present", () => {
			const missing = {
				property: "hello",
				value: "world",
			};
			const present = {
				property: "foo",
				value: "bar",
			};

			const subject = {
				[present.property]: present.value,
			};

			const simulated = lib.simulatePollution(
				subject,
				{
					property: missing.property,
					value: missing.value,
				},
				{
					property: present.property,
					value: `not ${present.value}`,
				},
			);

			assert.equal(simulated[missing.property], missing.value);
			assert.equal(simulated[present.property], present.value);
		});

		await t.test("missing & inherited", () => {
			const missing = {
				property: "hello",
				value: "world",
			};
			const inherited = {
				property: "toString",
				value: "foobaz",
			};

			const subject = {};

			const simulated = lib.simulatePollution(
				subject,
				{
					property: missing.property,
					value: missing.value,
				},
				{
					property: inherited.property,
					value: inherited.value,
				},
			);

			assert.equal(simulated[missing.property], missing.value);
			assert.equal(simulated[inherited.property], inherited.value);
		});

		await t.test("present & inherited", () => {
			const present = {
				property: "hello",
				value: "world",
			};
			const inherited = {
				property: "toString",
				value: "foobaz",
			};

			const subject = {
				[present.property]: present.value,
			};

			const simulated = lib.simulatePollution(
				subject,
				{
					property: present.property,
					value: `not ${present.value}`,
				},
				{
					property: inherited.property,
					value: inherited.value,
				},
			);

			assert.equal(simulated[present.property], present.value);
			assert.equal(simulated[inherited.property], inherited.value);
		});

		await t.test("missing, present & inherited", () => {
			const missing = {
				property: "foo",
				value: "bar",
			};
			const present = {
				property: "hello",
				value: "world",
			};
			const inherited = {
				property: "toString",
				value: "foobaz",
			};

			const subject = {
				[present.property]: present.value,
			};

			const simulated = lib.simulatePollution(
				subject,
				{
					property: missing.property,
					value: missing.value,
				},
				{
					property: present.property,
					value: `not ${present.value}`,
				},
				{
					property: inherited.property,
					value: inherited.value,
				},
			);

			assert.equal(simulated[missing.property], missing.value);
			assert.equal(simulated[present.property], present.value);
			assert.equal(simulated[inherited.property], inherited.value);
		});
	});

	await t.test("null prototype", () => {
		fc.assert(
			fc.property(
				arbitrary
					.subject()
					.map((obj) => Object.assign(Object.create(null), obj)),
				arbitrary.optionsList(),
				(subject, optionsList) => {
					for (const options of optionsList) {
						fc.pre(!Object.hasOwn(subject, options.property));
					}

					const simulated = lib.simulatePollution(subject, ...optionsList);

					for (const options of optionsList) {
						assert.ok(!Object.hasOwn(simulated, options.property));
						assert.equal(simulated[options.property], undefined);
					}
				},
			),
		);
	});

	await t.test("enumeration", async (t) => {
		await t.test("enumerable", () => {
			const subject = {};
			const options = {
				property: "foo",
				value: "bar",
				enumerable: true,
			};
			const simulated = lib.simulatePollution(subject, options);

			const got = [];
			for (const key in simulated) {
				got.push({ key, value: simulated[key] });
			}

			const found = got.find((got) => got.key === options.property);
			assert.ok(found);
			assert.equal(found.value, options.value);
		});

		await t.test("non-enumerable", () => {
			const subject = {};
			const options = {
				property: "foo",
				value: "bar",
				enumerable: false,
			};
			const simulated = lib.simulatePollution(subject, options);

			const got = [];
			for (const key in simulated) {
				got.push(key);
			}

			assert.equal(got.includes(options.property), false);
		});

		await t.test("default enumerability", () => {
			const subject = {};
			const options = {
				property: "foo",
				value: "bar",
			};
			const simulated = lib.simulatePollution(subject, options);

			const got = [];
			for (const key in simulated) {
				got.push(key);
			}

			assert.equal(got.includes(options.property), false);
		});

		await t.test("multiple pollutions", async (t) => {
			await t.test("enumerable+enumerable", () => {
				const subject = {};
				const optionsList = [
					{
						property: "foo",
						value: "bar",
						enumerable: true,
					},
					{
						property: "hello",
						value: "world",
						enumerable: true,
					},
				];
				const simulated = lib.simulatePollution(subject, ...optionsList);

				const got = [];
				for (const key in simulated) {
					got.push({ key, value: simulated[key] });
				}

				for (const options of optionsList) {
					const found = got.find((got) => got.key === options.property);
					assert.ok(found);
					assert.equal(found.value, options.value);
				}
			});

			await t.test("enumerable+not enumerable", () => {
				const subject = {};
				const enumerableOptions = {
					property: "foo",
					value: "bar",
					enumerable: true,
				};
				const nonEnumerableOptions = {
					property: "hello",
					value: "world",
					enumerable: false,
				};
				const simulated = lib.simulatePollution(
					subject,
					enumerableOptions,
					nonEnumerableOptions,
				);

				const got = [];
				for (const key in simulated) {
					got.push({ key, value: simulated[key] });
				}

				const found1 = got.find(
					(got) => got.key === enumerableOptions.property,
				);
				assert.ok(found1);
				assert.equal(found1.value, enumerableOptions.value);

				const found2 = got.find(
					(got) => got.key === nonEnumerableOptions.property,
				);
				assert.equal(found2, undefined);
			});
		});

		await t.test("enumerable prototype property", () => {
			const prototypeProperty = "hello";
			const prototype = {
				[prototypeProperty]: "world",
			};

			const subject = Object.create(prototype);
			const options = {
				enumerable: true,
				property: "foo",
				value: "bar",
			};
			const simulated = lib.simulatePollution(subject, options);

			const got = [];
			for (const key in simulated) {
				got.push(key);
			}

			assert.equal(got.includes(prototypeProperty), true);
		});

		await t.test("descriptor of enumerable polluted properties", () => {
			const subject = {};
			const optionsList = [
				{
					enumerable: true,
					property: "foo",
					value: "bar",
				},
				{
					enumerable: true,
					property: "hello",
					value: "world",
				},
			];
			const simulated = lib.simulatePollution(subject, ...optionsList);

			const prototype = Reflect.getPrototypeOf(simulated);
			for (const options of optionsList) {
				const descriptor = Reflect.getOwnPropertyDescriptor(
					prototype,
					options.property,
				);
				assert.equal(descriptor?.value, options.value);
			}
		});

		await t.test("descriptor of non-enumerable polluted properties", () => {
			const subject = {};
			const options = {
				enumerable: false,
				property: "foo",
				value: "bar",
			};
			const simulated = lib.simulatePollution(subject, options);

			const prototype = Reflect.getPrototypeOf(simulated);
			const descriptor = Reflect.getOwnPropertyDescriptor(
				prototype,
				options.property,
			);
			assert.equal(descriptor, undefined);
		});

		await t.test("descriptor of non-existent prototype properties", () => {
			const subject = {};
			const simulated = lib.simulatePollution(subject);

			const prototype = Reflect.getPrototypeOf(simulated);
			const descriptor = Reflect.getOwnPropertyDescriptor(prototype, "foo");
			assert.equal(descriptor, undefined);
		});

		await t.test("getPrototypeOf->ownKeys", () => {
			const subject = {};
			const options = {
				property: "foo",
				value: "bar",
			};
			const simulated = lib.simulatePollution(subject, options);

			const prototype = Reflect.getPrototypeOf(simulated);
			const got = Reflect.ownKeys(prototype);

			assert.equal(got.includes(options.property), false);
		});

		await t.test("null prototype", () => {
			const subject = Object.create(null);
			const options = {
				property: "foo",
				value: "bar",
				enumerable: true,
			};
			const simulated = lib.simulatePollution(subject, options);

			const got = [];
			for (const key in simulated) {
				got.push(key);
			}

			assert.equal(got.includes(options.property), false);
		});
	});

	await t.test("typeof simulated", () => {
		fc.assert(
			fc.property(
				arbitrary.subject(),
				arbitrary.optionsList(),
				(subject, optionsList) => {
					const simulated = lib.simulatePollution(subject, ...optionsList);
					assert.equal(typeof subject, typeof simulated);
				},
			),
		);
	});

	await t.test("Array.isArray(simulated)", () => {
		fc.assert(
			fc.property(
				fc.oneof(
					fc.array(fc.anything()), // .5 bias for creating arrays
					arbitrary.subject(),
				),
				arbitrary.optionsList(),
				(subject, optionsList) => {
					const simulated = lib.simulatePollution(subject, ...optionsList);
					assert.equal(Array.isArray(subject), Array.isArray(simulated));
				},
			),
		);
	});

	await t.test("incorrect usage", async (t) => {
		await t.test("missing property in options", () => {
			fc.assert(
				fc.property(
					arbitrary.subject(),
					arbitrary.optionsList(),
					(subject, optionsList) => {
						for (const options of optionsList) {
							delete options.property;
						}

						assert.throws(
							() => {
								lib.simulatePollution(subject, ...optionsList);
							},
							{
								name: "TypeError",
								message: "invalid options, missing 'property'",
							},
						);
					},
				),
			);
		});
	});
});

test("withPollution", async (t) => {
	await t.test("sync", async (t) => {
		await t.test("pollution during callback", async (t) => {
			await t.test("explicit value", () => {
				fc.assert(
					fc.property(
						arbitrary.subject(),
						arbitrary.optionsList({ withValues: true }),
						(subject, optionsList) => {
							for (const options of optionsList) {
								fc.pre(subject[options.property] === undefined);
							}

							lib.withPollution(
								() => {
									for (const options of optionsList) {
										assert.equal(subject[options.property], options.value);
									}
								},
								...optionsList,
							);
						},
					),
				);
			});

			await t.test("default value", () => {
				fc.assert(
					fc.property(
						arbitrary.subject(),
						arbitrary.optionsList(),
						(subject, optionsList) => {
							for (const options of optionsList) {
								fc.pre(subject[options.property] === undefined);
								fc.pre(options.value === undefined);
							}

							lib.withPollution(
								() => {
									for (const options of optionsList) {
										assert.equal(subject[options.property], "polluted");
									}
								},
								...optionsList,
							);
						},
					),
				);
			});
		});

		await t.test("pollution during callback, null prototype", () => {
			fc.assert(
				fc.property(
					arbitrary
						.subject()
						.map((obj) => Object.assign(Object.create(null), obj)),
					arbitrary.optionsList(),
					(subject, optionsList) => {
						for (const options of optionsList) {
							fc.pre(subject[options.property] === undefined);
						}

						lib.withPollution(
							() => {
								for (const options of optionsList) {
									assert.equal(subject[options.property], undefined);
								}
							},
							...optionsList,
						);
					},
				),
			);
		});

		await t.test("pollution after callback, without error", () => {
			fc.assert(
				fc.property(
					arbitrary.subject(),
					arbitrary.optionsList({ withValues: true }),
					(subject, optionsList) => {
						for (const options of optionsList) {
							fc.pre(subject[options.property] === undefined);
						}

						assert.doesNotThrow(() => {
							lib.withPollution(() => {}, ...optionsList);
						});

						for (const options of optionsList) {
							assert.notEqual(subject[options.property], options.value);
						}
					},
				),
			);
		});

		await t.test("pollution after callback, with error", () => {
			fc.assert(
				fc.property(
					arbitrary.subject(),
					arbitrary.optionsList({ withValues: true }),
					(subject, optionsList) => {
						for (const options of optionsList) {
							fc.pre(subject[options.property] === undefined);
						}

						assert.throws(() => {
							lib.withPollution(
								() => {
									throw new Error();
								},
								...optionsList,
							);
						});

						for (const options of optionsList) {
							assert.notEqual(subject[options.property], options.value);
						}
					},
				),
			);
		});

		await t.test("enumerable pollution", async (t) => {
			await t.test("enumerable: true", () => {
				fc.assert(
					fc.property(
						arbitrary.subject(),
						arbitrary.optionsList().map(([options]) => options),
						(subject, options) => {
							fc.pre(subject[options.property] === undefined);

							const keys = lib.withPollution(
								() => {
									const keys = [];
									for (const key in subject) {
										keys.push(key);
									}

									return keys;
								},
								{
									...options,
									enumerable: true,
								},
							);

							assert.equal(keys.includes(options.property), true);
						},
					),
				);
			});

			await t.test("default enumerability", () => {
				fc.assert(
					fc.property(
						arbitrary.subject(),
						arbitrary.optionsList().map(([options]) => options),
						(subject, options) => {
							fc.pre(!Object.hasOwn(subject, options.property));
							fc.pre(!Object.hasOwn(options, "enumerable"));

							const keys = lib.withPollution(() => {
								const keys = [];
								for (const key in subject) {
									keys.push(key);
								}

								return keys;
							}, options);

							assert.equal(keys.includes(options.property), false);
						},
					),
				);
			});
		});
	});

	await t.test("async", async (t) => {
		const delay = 1;

		await t.test("pollution during callback, immediately", async () => {
			await fc.assert(
				fc.asyncProperty(
					arbitrary.subject(),
					arbitrary.optionsList({ withValues: true }),
					async (subject, optionsList) => {
						for (const options of optionsList) {
							fc.pre(subject[options.property] === undefined);
						}

						await lib.withPollution(
							() => {
								for (const options of optionsList) {
									assert.equal(subject[options.property], options.value);
								}

								return new Promise((resolve) => setTimeout(resolve, delay));
							},
							...optionsList,
						);
					},
				),
			);
		});

		await t.test("pollution during callback, delayed", async () => {
			await fc.assert(
				fc.asyncProperty(
					arbitrary.subject(),
					arbitrary.optionsList({ withValues: true }),
					async (subject, optionsList) => {
						for (const options of optionsList) {
							fc.pre(subject[options.property] === undefined);
						}

						await lib.withPollution(
							() => {
								return new Promise((resolve) => {
									setTimeout(() => {
										for (const options of optionsList) {
											assert.equal(subject[options.property], options.value);
										}
										resolve();
									}, delay);
								});
							},
							...optionsList,
						);
					},
				),
			);
		});

		await t.test("pollution during callback, null prototype", async () => {
			await fc.assert(
				fc.asyncProperty(
					arbitrary
						.subject()
						.map((obj) => Object.assign(Object.create(null), obj)),
					arbitrary.optionsList(),
					async (subject, optionsList) => {
						for (const options of optionsList) {
							fc.pre(subject[options.property] === undefined);
						}

						await lib.withPollution(
							() => {
								return new Promise((resolve) => {
									setTimeout(() => {
										for (const options of optionsList) {
											assert.equal(subject[options.property], undefined);
										}
										resolve();
									}, delay);
								});
							},
							...optionsList,
						);
					},
				),
			);
		});

		await t.test("pollution after callback, without error", async () => {
			await fc.assert(
				fc.asyncProperty(
					arbitrary.subject(),
					arbitrary.optionsList({ withValues: true }),
					async (subject, optionsList) => {
						for (const options of optionsList) {
							fc.pre(subject[options.property] === undefined);
						}

						await assert.doesNotReject(async () => {
							await lib.withPollution(
								() => {
									return new Promise((resolve) => setTimeout(resolve, delay));
								},
								...optionsList,
							);
						});

						for (const options of optionsList) {
							assert.notEqual(subject[options.property], options.value);
						}
					},
				),
			);
		});

		await t.test("pollution after callback, with error", async () => {
			await fc.assert(
				fc.asyncProperty(
					arbitrary.subject(),
					arbitrary.optionsList({ withValues: true }),
					async (subject, optionsList) => {
						for (const options of optionsList) {
							fc.pre(subject[options.property] === undefined);
						}

						await assert.rejects(async () => {
							await lib.withPollution(
								() => {
									return new Promise((_, reject) => setTimeout(reject, delay));
								},
								...optionsList,
							);
						});

						for (const options of optionsList) {
							assert.notEqual(subject[options.property], options.value);
						}
					},
				),
			);
		});

		await t.test("enumerable pollution", async () => {
			await fc.assert(
				fc.asyncProperty(
					arbitrary.subject(),
					arbitrary.optionsList().map(([options]) => options),
					async (subject, options) => {
						fc.pre(subject[options.property] === undefined);

						const keys = await lib.withPollution(
							() => {
								return new Promise((resolve) => {
									setTimeout(() => {
										const keys = [];
										for (const key in subject) {
											keys.push(key);
										}

										resolve(keys);
									}, delay);
								});
							},
							{
								...options,
								enumerable: true,
							},
						);

						assert.ok(keys.includes(options.property));
					},
				),
			);
		});
	});

	await t.test("polluting pre-existing property", () => {
		const property = "toString";
		const value = "bar";

		lib.withPollution(
			() => {
				assert.equal({}[property], value);
			},
			{
				property,
				value,
			},
		);

		assert.equal(typeof {}[property], "function");
	});

	await t.test("polluted property is writable", () => {
		const property = "foo";
		const value = "bar";

		lib.withPollution(
			() => {
				assert.doesNotThrow(() => {
					Object.prototype[property] = "baz";
				});
			},
			{
				property,
				value,
			},
		);
	});

	await t.test("incorrect usage", async (t) => {
		await t.test("missing property in options", () => {
			fc.assert(
				fc.property(
					arbitrary.optionsList({ withValues: true }),
					(optionsList) => {
						for (const options of optionsList) {
							delete options.property;
						}

						assert.throws(
							() => {
								lib.withPollution(() => {}, ...optionsList);
							},
							{
								name: "TypeError",
								message: "invalid options, missing 'property'",
							},
						);
					},
				),
			);
		});
	});
});
