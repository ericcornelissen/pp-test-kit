// SPDX-License-Identifier: ISC

import * as assert from "node:assert/strict";
import { mock, test } from "node:test";

import * as fc from "fast-check";

import * as handlers from "../src/handlers.js";

test("apply", async (t) => {
	const arbitrary = {
		argumentsList: () => fc.array(fc.anything()),
		path: () => fc.array(fc.string()),
		returnValue: () => fc.anything(),
		target: () => fc.func(fc.anything()),
		thisArgument: () => fc.anything(),
	};

	await t.test("usage", async (t) => {
		await t.test("call", () => {
			fc.assert(
				fc.property(
					arbitrary.thisArgument(),
					arbitrary.argumentsList(),
					arbitrary.returnValue(),
					arbitrary.path(),
					(thisArgument, argumentsList, returnValue, path) => {
						const context = { path, wrap };
						const target = mock.fn(() => returnValue);

						const apply = handlers.apply(context);
						apply(target, thisArgument, argumentsList);

						assert.equal(target.mock.callCount(), 1);
						const call = target.mock.calls[0];
						assert.deepEqual(call.this, thisArgument);
						assert.equal(call.arguments.length, argumentsList.length);
						for (const i in call.arguments) {
							assert.deepEqual(call.arguments[i], argumentsList[i]);
						}
						assert.equal(call.result, returnValue);
					},
				),
			);
		});

		await t.test("result", () => {
			fc.assert(
				fc.property(
					arbitrary.thisArgument(),
					arbitrary.argumentsList(),
					arbitrary.returnValue(),
					arbitrary.path(),
					(thisArgument, argumentsList, returnValue, path) => {
						const context = { path, wrap };
						const target = mock.fn(() => returnValue);

						const apply = handlers.apply(context);
						const result = apply(target, thisArgument, argumentsList);
						assert.equal(result, returnValue);
					},
				),
			);
		});
	});

	await t.test("wrapping", async (t) => {
		await t.test("thisArgument", () => {
			fc.assert(
				fc.property(
					arbitrary.target(),
					arbitrary.thisArgument(),
					arbitrary.argumentsList(),
					arbitrary.path(),
					(target, thisArgument, argumentsList, path) => {
						const wrap = mock.fn();
						const context = { path, wrap };

						const apply = handlers.apply(context);
						apply(target, thisArgument, argumentsList);

						assert.ok(wrap.mock.callCount() >= 1);
						const call = wrap.mock.calls[0];
						assert.notEqual(call, undefined);
						const argument = call.arguments[0];
						assert.notEqual(argument, undefined);
						assert.deepEqual(argument.path.slice(0, -1), path);
						assert.equal(argument.path[argument.path.length - 1], "([this])");
					},
				),
			);
		});

		await t.test("argumentsList", () => {
			fc.assert(
				fc.property(
					arbitrary.target(),
					arbitrary.thisArgument(),
					arbitrary.argumentsList(),
					arbitrary.path(),
					(target, thisArgument, argumentsList, path) => {
						const wrap = mock.fn();
						const context = { path, wrap };

						const apply = handlers.apply(context);
						apply(target, thisArgument, argumentsList);

						assert.ok(wrap.mock.callCount() >= argumentsList.length);

						for (let i = 0; i < argumentsList.length; i++) {
							const call = wrap.mock.calls[i + 1];
							assert.notEqual(call, undefined);
							const argument = call.arguments[0];
							assert.notEqual(argument, undefined);
							assert.deepEqual(argument.path.slice(0, -1), path);
							assert.match(
								argument.path[argument.path.length - 1],
								/^\(.*?x\)$/u,
							);
						}
					},
				),
			);
		});

		await t.test("returnValue", () => {
			fc.assert(
				fc.property(
					arbitrary.target(),
					arbitrary.thisArgument(),
					arbitrary.argumentsList(),
					arbitrary.path(),
					(target, thisArgument, argumentsList, path) => {
						const wrap = mock.fn();
						const context = { path, wrap };

						const apply = handlers.apply(context);
						apply(target, thisArgument, argumentsList);

						assert.ok(wrap.mock.callCount() >= 1);
						const call = wrap.mock.calls[wrap.mock.calls.length - 1];
						assert.notEqual(call, undefined);
						const argument = call.arguments[0];
						assert.notEqual(argument, undefined);
						assert.deepEqual(argument.path.slice(0, -1), path);
						assert.equal(argument.path[argument.path.length - 1], "()=>{}");
					},
				),
			);
		});
	});
});

test("construct", async (t) => {
	const arbitrary = {
		argumentsList: () => fc.array(fc.anything()),
		newTarget: () =>
			arbitrary.returnValue().map(
				(returnValue) =>
					function () {
						return returnValue;
					},
			),
		path: () => fc.array(fc.string()),
		returnValue: () => fc.object(),
		target: () =>
			arbitrary.returnValue().map(
				(returnValue) =>
					function () {
						return returnValue;
					},
			),
	};

	await t.test("usage", async (t) => {
		await t.test("construct", () => {
			fc.assert(
				fc.property(
					arbitrary.returnValue(),
					arbitrary.argumentsList(),
					arbitrary.newTarget(),
					arbitrary.path(),
					(returnValue, argumentsList, newTarget, path) => {
						const context = { path, wrap };
						const target = mock.fn(function () {
							return returnValue;
						});

						const construct = handlers.construct(context);
						construct(target, argumentsList, newTarget);

						assert.equal(target.mock.callCount(), 1);
						const call = target.mock.calls[0];
						assert.equal(call.arguments.length, argumentsList.length);
						for (const i in call.arguments) {
							assert.deepEqual(call.arguments[i], argumentsList[i]);
						}
						assert.equal(call.result, returnValue);
					},
				),
			);
		});

		await t.test("result", () => {
			fc.assert(
				fc.property(
					arbitrary.returnValue(),
					arbitrary.argumentsList(),
					arbitrary.newTarget(),
					arbitrary.path(),
					(returnValue, argumentsList, newTarget, path) => {
						const context = { path, wrap };
						const target = mock.fn(function () {
							return returnValue;
						});

						const construct = handlers.construct(context);
						const result = construct(target, argumentsList, newTarget);
						assert.equal(result, returnValue);
					},
				),
			);
		});
	});

	await t.test("wrapping", async (t) => {
		await t.test("argumentsList", () => {
			fc.assert(
				fc.property(
					arbitrary.target(),
					arbitrary.argumentsList(),
					arbitrary.newTarget(),
					arbitrary.path(),
					(target, argumentsList, newTarget, path) => {
						const wrap = mock.fn(({ target }) => target);
						const context = { path, wrap };

						const construct = handlers.construct(context);
						construct(target, argumentsList, newTarget);

						assert.ok(wrap.mock.callCount() >= argumentsList.length);

						for (let i = 0; i < argumentsList.length; i++) {
							const call = wrap.mock.calls[i];
							assert.notEqual(call, undefined);
							const argument = call.arguments[0];
							assert.notEqual(argument, undefined);
							assert.notEqual(argument, undefined);
							assert.deepEqual(argument.path.slice(0, -1), path);
							assert.match(
								argument.path[argument.path.length - 1],
								/^new\(.*?x\)$/u,
							);
						}
					},
				),
			);
		});

		await t.test("returnValue", () => {
			fc.assert(
				fc.property(
					arbitrary.target(),
					arbitrary.argumentsList(),
					arbitrary.newTarget(),
					arbitrary.path(),
					(target, argumentsList, newTarget, path) => {
						const wrap = mock.fn(({ target }) => target);
						const context = { path, wrap };

						const construct = handlers.construct(context);
						construct(target, argumentsList, newTarget);

						assert.ok(wrap.mock.callCount() >= 1);
						const call = wrap.mock.calls[wrap.mock.calls.length - 1];
						assert.notEqual(call, undefined);
						const argument = call.arguments[0];
						assert.notEqual(argument, undefined);
						assert.deepEqual(argument.path.slice(0, -1), path);
						assert.equal(argument.path[argument.path.length - 1], "new()");
					},
				),
			);
		});
	});
});

test("get", async (t) => {
	const arbitrary = {
		has: () => fc.func(fc.boolean()),
		path: () => fc.array(fc.string()),
		propertyKey: () =>
			fc.oneof(
				fc.string(),
				fc.oneof(
					fc.constant(Symbol()),
					fc.string().map((description) => Symbol(description)),
				),
			),
		receiver: () => fc.object(),
		receivers: () =>
			fc.oneof(
				arbitrary.receiver().map((receiver) => [receiver, receiver]),
				fc.tuple(arbitrary.receiver(), arbitrary.receiver()),
			),
		target: () => fc.object(),
		value: () => fc.anything(),
	};

	await t.test("usage", async (t) => {
		await t.test("result (default receiver)", () => {
			fc.assert(
				fc.property(
					arbitrary.target(),
					arbitrary.propertyKey(),
					arbitrary.receiver(),
					arbitrary.has(),
					arbitrary.path(),
					(target, propertyKey, receiver, has, path) => {
						const context = { has, onAccess, path, wrap, proxy: receiver };

						const get = handlers.get(context);
						const result = get(target, propertyKey, receiver);
						assert.equal(result, target[propertyKey]);
					},
				),
			);
		});

		await t.test("result (custom receiver)", () => {
			fc.assert(
				fc.property(
					arbitrary.target(),
					arbitrary.propertyKey(),
					arbitrary.receiver(),
					arbitrary.receiver(),
					arbitrary.has(),
					arbitrary.path(),
					(target, propertyKey, stdReceiver, receiver, has, path) => {
						const context = { has, onAccess, path, wrap, proxy: stdReceiver };

						const get = handlers.get(context);
						const result = get(target, propertyKey, receiver);
						assert.equal(result, Reflect.get(target, propertyKey, receiver));
					},
				),
			);
		});

		await t.test("property present", () => {
			fc.assert(
				fc.property(
					arbitrary.target(),
					arbitrary.propertyKey(),
					arbitrary.receivers(),
					arbitrary.path(),
					(target, propertyKey, [proxy, receiver], path) => {
						const has = () => true;
						const onAccess = mock.fn();
						const context = { has, onAccess, path, proxy, wrap };

						const get = handlers.get(context);
						get(target, propertyKey, receiver);

						assert.equal(onAccess.mock.calls.length, 0);
					},
				),
			);
		});

		await t.test("property present (null prototype)", () => {
			fc.assert(
				fc.property(
					arbitrary
						.target()
						.map((obj) => Object.assign(Object.create(null), obj)),
					arbitrary.propertyKey(),
					arbitrary.receivers(),
					arbitrary.path(),
					(target, propertyKey, [proxy, receiver], path) => {
						const has = () => true;
						const onAccess = mock.fn();
						const context = { has, onAccess, path, proxy, wrap };

						const get = handlers.get(context);
						get(target, propertyKey, receiver);

						assert.equal(onAccess.mock.calls.length, 0);
					},
				),
			);
		});

		await t.test("property missing", () => {
			fc.assert(
				fc.property(
					arbitrary.target(),
					arbitrary.propertyKey(),
					arbitrary.receivers(),
					arbitrary.path(),
					(target, propertyKey, [proxy, receiver], path) => {
						const has = () => false;
						const onAccess = mock.fn();
						const context = { has, onAccess, path, proxy, wrap };

						const get = handlers.get(context);
						get(target, propertyKey, receiver);

						assert.equal(onAccess.mock.calls.length, 1);
						const call = onAccess.mock.calls[0];
						assert.notEqual(call, undefined);
						const argument = call.arguments[0];
						assert.notEqual(argument, undefined);
						assert.notEqual(argument.callSite, undefined);
						assert.equal(argument.property, propertyKey);
						assert.equal(argument.propertyName, propertyKey.toString());
						assert.deepEqual(
							argument.propertyPath,
							[...path, propertyKey.toString()].join("."),
						);
					},
				),
			);
		});

		await t.test("property missing (null prototype)", () => {
			fc.assert(
				fc.property(
					arbitrary
						.target()
						.map((obj) => Object.assign(Object.create(null), obj)),
					arbitrary.propertyKey(),
					arbitrary.receivers(),
					arbitrary.path(),
					(target, propertyKey, [proxy, receiver], path) => {
						const has = () => false;
						const onAccess = mock.fn();
						const context = { has, onAccess, path, proxy, wrap };

						const get = handlers.get(context);
						get(target, propertyKey, receiver);

						assert.equal(onAccess.mock.calls.length, 0);
					},
				),
			);
		});

		await t.test("property getter throws", () => {
			fc.assert(
				fc.property(
					arbitrary.target(),
					arbitrary.propertyKey(),
					arbitrary.propertyKey(),
					arbitrary.receivers(),
					arbitrary.path(),
					(target, illegalProp, missingProp, [proxy, receiver], path) => {
						fc.pre(illegalProp !== missingProp);
						fc.pre(
							Reflect.getOwnPropertyDescriptor(target, illegalProp)?.writable,
						);

						const has = (_, prop) => prop === illegalProp;
						const onAccess = mock.fn();
						const context = { has, onAccess, path, proxy, wrap };

						Object.defineProperty(target, illegalProp, {
							get() {
								throw new Error();
							},
						});

						const get = handlers.get(context);
						assert.throws(() => {
							get(target, illegalProp, receiver);
						});

						assert.equal(onAccess.mock.calls.length, 0);
						get(target, missingProp, receiver);
						assert.equal(onAccess.mock.calls.length, 1);
					},
				),
			);
		});
	});

	await t.test("wrapping", async (t) => {
		await t.test("target[property] (writable)", () => {
			fc.assert(
				fc.property(
					arbitrary.value(),
					arbitrary.target(),
					arbitrary.propertyKey(),
					arbitrary.receivers(),
					arbitrary.has(),
					arbitrary.path(),
					(value, target, propertyKey, [proxy, receiver], has, path) => {
						fc.pre(
							Reflect.getOwnPropertyDescriptor(target, propertyKey)?.writable,
						);

						const wrap = mock.fn(() => value);
						const context = { has, onAccess, path, proxy, wrap };

						const get = handlers.get(context);
						get(target, propertyKey, receiver);

						assert.equal(wrap.mock.callCount(), 1);
						const call = wrap.mock.calls[0];
						assert.notEqual(call, undefined);
						const argument = call.arguments[0];
						assert.notEqual(argument, undefined);
						assert.deepEqual(argument.subject, target[propertyKey]);
						assert.deepEqual(argument.path.slice(0, -1), path);
						assert.equal(
							argument.path[argument.path.length - 1],
							propertyKey.toString(),
						);
					},
				),
			);
		});

		await t.test("target[property] (read-only)", () => {
			fc.assert(
				fc.property(
					arbitrary.value(),
					arbitrary.target(),
					arbitrary.propertyKey(),
					arbitrary.receivers(),
					arbitrary.has(),
					arbitrary.path(),
					(value, target, propertyKey, [proxy, receiver], has, path) => {
						fc.pre(
							Reflect.getOwnPropertyDescriptor(target, propertyKey)?.writable,
						);

						Object.defineProperty(target, propertyKey, {
							value: value,
							writable: false,
						});

						const wrap = mock.fn();
						const context = { has, onAccess, path, proxy, wrap };

						const get = handlers.get(context);
						get(target, propertyKey, receiver);

						assert.equal(wrap.mock.callCount(), 0);
					},
				),
			);
		});
	});
});

test("getPrototypeOf", async (t) => {
	const arbitrary = {
		path: () => fc.array(fc.string()),
		target: () => fc.object(),
	};

	await t.test("usage", async (t) => {
		await t.test("result", () => {
			fc.assert(
				fc.property(arbitrary.target(), arbitrary.path(), (target, path) => {
					const getPrototypeOf = handlers.getPrototypeOf({ onAccess, path });
					const result = getPrototypeOf(target);
					assert.deepEqual(result, Reflect.getPrototypeOf(target));
				}),
			);
		});

		await t.test("for...in", () => {
			fc.assert(
				fc.property(arbitrary.target(), arbitrary.path(), (target, path) => {
					const onAccess = mock.fn();

					const context = { onAccess, path };
					context[handlers.forInCheck] = () => true;

					const getPrototypeOf = handlers.getPrototypeOf(context);
					getPrototypeOf(target);

					assert.equal(onAccess.mock.calls.length, 1);
					const call = onAccess.mock.calls[0];
					assert.notEqual(call, undefined);
					const argument = call.arguments[0];
					assert.notEqual(argument, undefined);
					assert.notEqual(argument.callSite, undefined);
					assert.equal(argument.property, Symbol.for("__forin__"));
					assert.equal(argument.propertyName, "__forin__");
					assert.deepEqual(
						argument.propertyPath,
						`for...in .${path.join(".")}`,
					);
				}),
			);
		});
	});
});

test("ownKeys", async (t) => {
	const arbitrary = {
		target: () => fc.object(),
	};

	await t.test("usage", async (t) => {
		await t.test("result", () => {
			fc.assert(
				fc.property(arbitrary.target(), (target) => {
					const ownKeys = handlers.ownKeys({});
					const result = ownKeys(target);
					assert.deepEqual(result, Reflect.ownKeys(target));
				}),
			);
		});

		await t.test("for...in", () => {
			fc.assert(
				fc.property(arbitrary.target(), (target) => {
					const context = {};

					const ownKeys = handlers.ownKeys(context);
					ownKeys(target);
					assert.equal(typeof context[handlers.forInCheck], "function");
				}),
			);
		});
	});
});

/**
 * A fake wrapping function to use in tests when the use is not of interest.
 *
 * @param {Object} p The parameters.
 * @param {any} p.subject The subject to wrap.
 * @returns {any} Always `context.subject`.
 */
function wrap({ subject }) {
	return subject;
}

/**
 * A fake access listener to use in tests when the use is not of interest.
 */
function onAccess() {
	// Nothing to do
}
