// SPDX-License-Identifier: ISC

/**
 * Test cases for plain arrays that cover edge cases.
 */
export const edgeCases = [
	{
		subject: [],
		accesses: [],
		expected: [],
	},
	{
		subject: [],
		accesses: ["__proto__"],
		expected: [],
	},
];

/**
 * Test cases for plain arrays where at least one missing index or property is
 * accessed.
 */
export const missing = [
	{
		subject: [],
		accesses: [0],
		expected: ["0"],
	},
	{
		subject: ["foobar"],
		accesses: [1],
		expected: ["1"],
	},
	{
		subject: ["foo", "bar"],
		accesses: [2],
		expected: ["2"],
	},
	{
		subject: ["foobar"],
		accesses: [0, 1],
		expected: ["1"],
	},
	{
		subject: ["foobar"],
		accesses: [1, 2],
		expected: ["1", "2"],
	},
	{
		subject: [["foo"]],
		accesses: [[0, 1]],
		expected: ["1"],
		expectedPath: ["0.1"],
	},
	{
		subject: [["foo"], "bar"],
		accesses: [[0, 1], 2],
		expected: ["1", "2"],
		expectedPath: ["0.1", "2"],
	},
	{
		subject: [{ foo: "bar" }],
		accesses: [[0, "foobar"], 2],
		expected: ["foobar", "2"],
		expectedPath: ["0.foobar", "2"],
	},
	{
		subject: ["foobar"],
		accesses: [1, 1],
		expected: ["1", "1"],
	},

	// non-numeric
	{
		subject: [],
		accesses: ["property"],
		expected: ["property"],
	},
	{
		subject: ["foobar"],
		accesses: ["foobar"],
		expected: ["foobar"],
	},
	{
		subject: ["foo", "bar"],
		accesses: [0, "property"],
		expected: ["property"],
	},
	{
		subject: [],
		accesses: ["property", "property"],
		expected: ["property", "property"],
	},
];

/**
 * Test cases for plain arrays where only present indices and properties are
 * accessed.
 */
export const present = [
	{
		subject: ["foobar"],
		accesses: [0],
		expected: [],
	},
];

/**
 * Test cases for plain arrays where at least one inherited index or property is
 * accessed.
 */
export const inherited = [
	{
		subject: [],
		accesses: ["at"],
		expected: ["at"],
	},
	{
		subject: [],
		accesses: ["concat"],
		expected: ["concat"],
	},
	{
		subject: [],
		accesses: ["copyWithin"],
		expected: ["copyWithin"],
	},
	{
		subject: [],
		accesses: ["entries"],
		expected: ["entries"],
	},
	{
		subject: [],
		accesses: ["every"],
		expected: ["every"],
	},
	{
		subject: [],
		accesses: ["fill"],
		expected: ["fill"],
	},
	{
		subject: [],
		accesses: ["filter"],
		expected: ["filter"],
	},
	{
		subject: [],
		accesses: ["find"],
		expected: ["find"],
	},
	{
		subject: [],
		accesses: ["findIndex"],
		expected: ["findIndex"],
	},
	{
		subject: [],
		accesses: ["findLast"],
		expected: ["findLast"],
	},
	{
		subject: [],
		accesses: ["findLastIndex"],
		expected: ["findLastIndex"],
	},
	{
		subject: [],
		accesses: ["flat"],
		expected: ["flat"],
	},
	{
		subject: [],
		accesses: ["flatMap"],
		expected: ["flatMap"],
	},
	{
		subject: [],
		accesses: ["forEach"],
		expected: ["forEach"],
	},
	{
		subject: [],
		accesses: ["includes"],
		expected: ["includes"],
	},
	{
		subject: [],
		accesses: ["indexOf"],
		expected: ["indexOf"],
	},
	{
		subject: [],
		accesses: ["join"],
		expected: ["join"],
	},
	{
		subject: [],
		accesses: ["keys"],
		expected: ["keys"],
	},
	{
		subject: [],
		accesses: ["lastIndexOf"],
		expected: ["lastIndexOf"],
	},
	{
		subject: [],
		accesses: ["map"],
		expected: ["map"],
	},
	{
		subject: [],
		accesses: ["pop"],
		expected: ["pop"],
	},
	{
		subject: [],
		accesses: ["push"],
		expected: ["push"],
	},
	{
		subject: [],
		accesses: ["reduce"],
		expected: ["reduce"],
	},
	{
		subject: [],
		accesses: ["reduceRight"],
		expected: ["reduceRight"],
	},
	{
		subject: [],
		accesses: ["reverse"],
		expected: ["reverse"],
	},
	{
		subject: [],
		accesses: ["shift"],
		expected: ["shift"],
	},
	{
		subject: [],
		accesses: ["slice"],
		expected: ["slice"],
	},
	{
		subject: [],
		accesses: ["some"],
		expected: ["some"],
	},
	{
		subject: [],
		accesses: ["sort"],
		expected: ["sort"],
	},
	{
		subject: [],
		accesses: ["splice"],
		expected: ["splice"],
	},
	{
		subject: [],
		accesses: ["toLocaleString"],
		expected: ["toLocaleString"],
	},
	{
		subject: [],
		accesses: ["toReversed"],
		expected: ["toReversed"],
	},
	{
		subject: [],
		accesses: ["toSorted"],
		expected: ["toSorted"],
	},
	{
		subject: [],
		accesses: ["toSpliced"],
		expected: ["toSpliced"],
	},
	{
		subject: [],
		accesses: ["toString"],
		expected: ["toString"],
	},
	{
		subject: [],
		accesses: ["unshift"],
		expected: ["unshift"],
	},
	{
		subject: [],
		accesses: ["values"],
		expected: ["values"],
	},
	{
		subject: [],
		accesses: ["with"],
		expected: ["with"],
	},
	{
		subject: [],
		accesses: ["hasOwnProperty"],
		expected: ["hasOwnProperty"],
	},
	{
		subject: [],
		accesses: ["isPrototypeOf"],
		expected: ["isPrototypeOf"],
	},
	{
		subject: [],
		accesses: ["propertyIsEnumerable"],
		expected: ["propertyIsEnumerable"],
	},
	{
		subject: [],
		accesses: ["toString"],
		expected: ["toString"],
	},
	{
		subject: [],
		accesses: ["valueOf"],
		expected: ["valueOf"],
	},
];
