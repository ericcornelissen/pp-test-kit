// SPDX-License-Identifier: ISC

const symbol = Symbol();

/**
 * Test cases for plain objects that cover edge cases.
 */
export const edgeCases = [
	{
		subject: {},
		accesses: [],
		expected: [],
	},
	{
		subject: Object.create(null),
		accesses: ["foobar"],
		expected: [],
	},
	{
		subject: {},
		accesses: ["__proto__"],
		expected: [],
	},
];

/**
 * Test cases for plain objects where at least one missing property is accessed.
 */
export const missing = [
	{
		subject: {},
		accesses: ["foobar"],
		expected: ["foobar"],
	},
	{
		subject: { foo: "bar" },
		accesses: ["baz"],
		expected: ["baz"],
	},
	{
		subject: { foo: "bar" },
		accesses: ["foo", "bar"],
		expected: ["bar"],
	},
	{
		subject: { foo: "bar" },
		accesses: [symbol],
		expected: [symbol],
	},
	{
		subject: { bar: "foo" },
		accesses: ["foo", "baz"],
		expected: ["foo", "baz"],
	},
	{
		subject: { foo: {} },
		accesses: [["foo", "bar"]],
		expected: ["bar"],
		expectedPath: ["foo.bar"],
	},
	{
		subject: { foo: {}, bar: "baz" },
		accesses: [["foo", "bar"], "baz"],
		expected: ["bar", "baz"],
		expectedPath: ["foo.bar", "baz"],
	},
	{
		subject: { foo: [], bar: "baz" },
		accesses: [["foo", 0], "baz"],
		expected: ["0", "baz"],
		expectedPath: ["foo.0", "baz"],
	},
	{
		subject: { foo: "bar" },
		accesses: ["bar", "bar"],
		expected: ["bar", "bar"],
	},

	// numeric
	{
		subject: {},
		accesses: [0],
		expected: ["0"],
	},
	{
		subject: { foo: "bar" },
		accesses: [1],
		expected: ["1"],
	},
	{
		subject: { foo: "bar" },
		accesses: [1, 1],
		expected: ["1", "1"],
	},
];

/**
 * Test cases for plain objects where only present properties are accessed.
 */
export const present = [
	{
		subject: { foo: "bar" },
		accesses: ["foo"],
		expected: [],
	},
	{
		subject: { [symbol]: "foobar" },
		accesses: [symbol],
		expected: [],
	},
];

/**
 * Test cases for plain objects where at least one inherited property is
 * accessed.
 */
export const inherited = [
	{
		subject: {},
		accesses: ["hasOwnProperty"],
		expected: ["hasOwnProperty"],
	},
	{
		subject: {},
		accesses: ["isPrototypeOf"],
		expected: ["isPrototypeOf"],
	},
	{
		subject: {},
		accesses: ["propertyIsEnumerable"],
		expected: ["propertyIsEnumerable"],
	},
	{
		subject: {},
		accesses: ["toLocaleString"],
		expected: ["toLocaleString"],
	},
	{
		subject: {},
		accesses: ["toString"],
		expected: ["toString"],
	},
	{
		subject: {},
		accesses: ["valueOf"],
		expected: ["valueOf"],
	},
];

/**
 * Test cases for plain objects where only present properties are accessed,
 * including at least one property which shadows an inherited property.
 */
export const shadowed = [
	{
		subject: { toString: "foobar" },
		accesses: ["toString"],
		expected: [],
	},
];
