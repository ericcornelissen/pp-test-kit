// SPDX-License-Identifier: ISC

class TestClassFoo {
	constructor() {
		this.fooProperty = "baz";
	}
}

class TestClassBar extends TestClassFoo {
	constructor() {
		super();
		this.barProperty = "foobaz";
	}
}

/**
 * Test cases for classes where at least one missing property is accessed.
 */
export const missing = [
	...[
		{
			subject: new Boolean(true),
			accesses: ["foo"],
			expected: ["foo"],
		},
		{
			subject: new Boolean(false),
			accesses: ["foo"],
			expected: ["foo"],
		},
	],
	...[
		{
			subject: /foobar/,
			accesses: ["foo"],
			expected: ["foo"],
		},
		{
			subject: new RegExp("foobar"),
			accesses: ["foo"],
			expected: ["foo"],
		},
	],
	...[
		{
			subject: new TestClassFoo(),
			accesses: ["foo"],
			expected: ["foo"],
		},
		{
			subject: new TestClassBar(),
			accesses: ["foo"],
			expected: ["foo"],
		},
	],
];

/**
 * Test cases for classes where only present properties are accessed.
 */
export const present = [
	...[
		{
			subject: /foobar/,
			accesses: ["lastIndex"],
			expected: [],
		},
		{
			subject: new RegExp("foobar"),
			accesses: ["lastIndex"],
			expected: [],
		},
	],
	...[
		{
			subject: new TestClassFoo(),
			accesses: ["fooProperty"],
			expected: [],
		},
		{
			subject: new TestClassBar(),
			accesses: ["barProperty"],
			expected: [],
		},
	],
];

/**
 * Test cases for classes where at least one inherited property is accessed.
 */
export const inherited = [
	...[
		{
			subject: new Boolean(true),
			accesses: ["toString"],
			expected: ["toString"],
		},
		{
			subject: new Boolean(false),
			accesses: ["toString"],
			expected: ["toString"],
		},
		{
			subject: new Boolean(true),
			accesses: ["valueOf"],
			expected: ["valueOf"],
		},
		{
			subject: new Boolean(false),
			accesses: ["valueOf"],
			expected: ["valueOf"],
		},
	],
	...[
		{
			subject: /foobar/,
			accesses: ["exec"],
			expected: ["exec"],
		},
		{
			subject: /foobar/,
			accesses: ["test"],
			expected: ["test"],
		},
		{
			subject: /foobar/,
			accesses: ["toString"],
			expected: ["toString"],
		},
		{
			subject: /foobar/,
			accesses: ["dotAll"],
			expected: ["dotAll"],
		},
		{
			subject: /foobar/,
			accesses: ["flags"],
			expected: ["flags"],
		},
		{
			subject: /foobar/,
			accesses: ["global"],
			expected: ["global"],
		},
		{
			subject: /foobar/,
			accesses: ["hasIndices"],
			expected: ["hasIndices"],
		},
		{
			subject: /foobar/,
			accesses: ["ignoreCase"],
			expected: ["ignoreCase"],
		},
		{
			subject: /foobar/,
			accesses: ["multiline"],
			expected: ["multiline"],
		},
		{
			subject: /foobar/,
			accesses: ["source"],
			expected: ["source"],
		},
		{
			subject: /foobar/,
			accesses: ["sticky"],
			expected: ["sticky"],
		},
		{
			subject: /foobar/,
			accesses: ["unicode"],
			expected: ["unicode"],
		},
		{
			subject: /foobar/,
			accesses: ["unicodeSets"],
			expected: ["unicodeSets"],
		},
	],
	...[
		{
			subject: new TestClassFoo(),
			accesses: ["toString"],
			expected: ["toString"],
		},
		{
			subject: new TestClassBar(),
			accesses: ["toString"],
			expected: ["toString"],
		},
	],
];
