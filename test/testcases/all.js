// SPDX-License-Identifier: ISC

import * as util from "node:util";

import * as arrays from "./arrays.js";
import * as classes from "./classes.js";
import * as objects from "./objects.js";

/**
 * @typedef TestCase
 * @property {string} testName The name of the test case.
 * @property {any} subject The object to access properties on.
 * @property {any[]} [accesses] The accesses the perform on the subject.
 * @property {any[]} [expected] The expected missing property lookups.
 */

/**
 * Add a standard test name to a test case.
 *
 * @param {TestCase} testCase The test case to add a name to.
 * @returns {TestCase} The test case with a standard test name added.
 */
function addTestName(testCase) {
	const { accesses, subject } = testCase;

	let testName;
	if (accesses) {
		testName = util.format(
			"access %s on %s",
			util.inspect(accesses),
			util.inspect(subject),
		);
	} else {
		testName = util.format("wrapping `%s`", util.inspect(subject));
	}

	return {
		...testCase,
		testName,
	};
}

/**
 * Test cases that cover edge cases.
 *
 * @type {TestCase[]}
 */
export const edgeCases = [...arrays.edgeCases, ...objects.edgeCases].map(
	addTestName,
);

/**
 * Test cases where at least one missing property is accessed.
 *
 * @type {TestCase[]}
 */
export const missing = [
	...arrays.missing,
	...classes.missing,
	...objects.missing,
].map(addTestName);

/**
 * Test cases where only present properties are accessed.
 *
 * @type {TestCase[]}
 */
export const present = [
	...arrays.present,
	...classes.present,
	...objects.present,
].map(addTestName);

/**
 * Test cases where at least one inherited property is accessed.
 *
 * @type {TestCase[]}
 */
export const inherited = [
	...arrays.inherited,
	...classes.inherited,
	...objects.inherited,
].map(addTestName);

/**
 * Test cases where only present properties are accessed, including at least one
 * property which shadows an inherited property.
 *
 * @type {TestCase[]}
 */
export const shadowed = [...objects.shadowed].map(addTestName);

/**
 * Test cases of unsupported values.
 *
 * @type {TestCase[]}
 */
export const unsupported = [
	{ subject: undefined },
	{ subject: null },
	{ subject: NaN },
	{ subject: Infinity },
	{ subject: -Infinity },
	{ subject: true },
	{ subject: false },
	{ subject: 42 },
	{ subject: 3.14 },
	{ subject: 9001n },
	{ subject: "Hello world!" },
	{ subject: Symbol() },
].map(addTestName);
