// SPDX-License-Identifier: ISC

import * as assert from "node:assert/strict";
import { test } from "node:test";

import escapeStringRegexp from "escape-string-regexp";
import * as fc from "fast-check";

import * as testCases from "./testcases/all.js";

import * as lib from "../src/invariant.js";

const arbitrary = {
	subject: () =>
		fc.anything({
			values: [fc.constant({})],
			withBigInt: true,
			withBoxedValues: true,
			withDate: true,
			withMap: true,
			withSet: true,
			withTypedArray: true,
			withSparseArray: true,
		}),
	options: () =>
		fc.record(
			{
				strict: fc.boolean(),
			},
			{
				requiredKeys: [],
			},
		),
};

test("invariant API", async (t) => {
	await t.test("property missing", async (t) => {
		for (const testCase of testCases.missing) {
			const { testName } = testCase;

			await t.test(`${testName} (default)`, () => {
				notOk(testCase);
			});

			await t.test(`${testName} (not strict)`, () => {
				notOk(testCase, { strict: false });
			});

			await t.test(`${testName} (strict)`, () => {
				notOk(testCase, { strict: true });
			});
		}
	});

	await t.test("property present", async (t) => {
		for (const testCase of testCases.present) {
			const { testName } = testCase;

			await t.test(`${testName} (default)`, () => {
				ok(testCase);
			});

			await t.test(`${testName} (not strict)`, () => {
				ok(testCase, { strict: false });
			});

			await t.test(`${testName} (strict)`, () => {
				ok(testCase, { strict: true });
			});
		}
	});

	await t.test("property inherited", async (t) => {
		for (const testCase of testCases.inherited) {
			const { testName } = testCase;

			await t.test(`${testName} (default)`, () => {
				ok(testCase);
			});

			await t.test(`${testName} (not strict)`, () => {
				ok(testCase, { strict: false });
			});

			await t.test(`${testName} (strict)`, () => {
				notOk(testCase, { strict: true });
			});
		}
	});

	await t.test("property shadowed", async (t) => {
		for (const testCase of testCases.shadowed) {
			const { testName } = testCase;

			await t.test(`${testName} (default)`, () => {
				ok(testCase);
			});

			await t.test(`${testName} (not strict)`, () => {
				ok(testCase, { strict: false });
			});

			await t.test(`${testName} (strict)`, () => {
				ok(testCase, { strict: true });
			});
		}
	});

	await t.test("null prototype", () => {
		fc.assert(
			fc.property(
				arbitrary
					.subject()
					.map((obj) => Object.assign(Object.create(null), obj)),
				arbitrary.options(),
				fc.string(),
				(subject, options, property) => {
					assert.doesNotThrow(() => {
						lib.test((ctx) => {
							const wrappedSubject = ctx.wrap(subject, options);

							assert.notEqual(wrappedSubject[property], "polluted");
						});
					});
				},
			),
		);
	});

	await t.test("enumerable", () => {
		const magicMessage = "magic message";

		assert.throws(
			() => {
				lib.test((ctx) => {
					const subject = {};
					const wrappedSubject = ctx.wrap(subject);

					const got = [];
					for (const key in wrappedSubject) {
						got.push(key);
					}

					const want = [];
					for (const key in subject) {
						want.push(key);
					}

					assert.deepEqual(got, want, magicMessage);
				});
			},
			(error) => {
				assert.ok(error instanceof assert.AssertionError);

				assert.match(
					error.message,
					new RegExp(`an enumerable property: ${magicMessage}`, "u"),
				);

				return true;
			},
		);
	});

	await t.test("invariant never holds", () => {
		const magicMessage = "magic message";

		assert.throws(
			() => {
				lib.test((_) => {
					assert.fail(magicMessage);
				});
			},
			{
				name: "AssertionError",
				message: `Invariant does not hold without pollution: ${magicMessage}`,
			},
		);
	});

	await t.test("subject unsupported", async (t) => {
		for (const testCase of testCases.unsupported) {
			const { subject, testName } = testCase;

			await t.test(testName, () => {
				assert.throws(
					() => {
						lib.test((ctx) => ctx.wrap(subject));
					},
					{
						message:
							/Cannot create proxy with a non-object as target or handler/u,
					},
				);
			});
		}
	});
});

/** @typedef {import("../src/wrapper.js").Options} Options */
/** @typedef {import("./testcases/all.js").TestCase} TestCase */

/**
 * Test that the invariant API does not find a problem for the given test case.
 *
 * @param {TestCase} testCase The test case to evaluate.
 * @param {Options} options The options object to provide to the API.
 */
function ok(testCase, options) {
	const { accesses, subject } = testCase;

	assert.doesNotThrow(() => {
		lib.test((ctx) => {
			const wrappedSubject = ctx.wrap(subject, options);

			for (let access of accesses) {
				if (!Array.isArray(access)) {
					access = [access];
				}

				let value = wrappedSubject;
				for (const property of access) {
					value = value[property];
				}

				assert.notEqual(value, "polluted");
			}
		});
	});
}

/**
 * Test that the invariant API does find a problem for the given test case.
 *
 * @param {TestCase} testCase The test case to evaluate.
 * @param {Options} options The options object to provide to the API.
 */
function notOk(testCase, options) {
	const { accesses, expected, subject } = testCase;
	const magicMessage = "magic message";

	assert.throws(
		() => {
			lib.test((ctx) => {
				const wrappedSubject = ctx.wrap(subject, options);

				for (let access of accesses) {
					if (!Array.isArray(access)) {
						access = [access];
					}

					let value = wrappedSubject;
					for (const property of access) {
						value = value[property];
					}

					const lastAccess = access[access.length - 1];
					const property =
						typeof lastAccess === "number" ? `${lastAccess}` : lastAccess;
					if (expected.includes(property)) {
						assert.notEqual(value, "polluted", magicMessage);
					}
				}
			});
		},
		(error) => {
			assert.ok(error instanceof assert.AssertionError);

			const property = expected[0].toString();
			const name = escapeStringRegexp(property);
			const location = escapeStringRegexp(import.meta.url);
			assert.match(
				error.message,
				new RegExp(
					`the '${name}' property \\(at ${location}:\\d+:\\d+\\): ${magicMessage}`,
					"u",
				),
			);

			return true;
		},
	);
}
