// SPDX-License-Identifier: ISC

import { test } from "node:test";

import * as fc from "fast-check";
import * as ppTestKit from "../../src/throwing.js";

import * as manual from "../../src/manual.js";

test("no missing properties are accessed by the manual API", () => {
	fc.assert(
		fc.property(
			fc.object(),
			fc
				.record(
					{
						strict: fc.boolean(),
					},
					{ requiredKeys: [] },
				)
				.map(ppTestKit.wrap),
			(subject, options) => {
				const wrapped = manual.wrap(subject, ppTestKit.wrap(options));
				manual.check(wrapped);
			},
		),
	);
});
