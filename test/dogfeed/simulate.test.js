// SPDX-License-Identifier: ISC

import { test } from "node:test";

import * as fc from "fast-check";
import * as ppTestKit from "../../src/manual.js";

import { simulatePollution, withPollution } from "../../src/simulate.js";

const arbitrary = {
	options: () =>
		fc.record(
			{
				enumerable: fc.boolean(),
				property: fc.string(),
				value: fc.string(),
			},
			{ requiredKeys: [] },
		),
};

test("no missing properties are accessed by simulatePollution", () => {
	fc.assert(
		fc.property(
			fc.object(),
			fc.array(arbitrary.options().map(ppTestKit.wrap)),
			(subject, optionsList) => {
				try {
					const wrapped = simulatePollution(subject, ...optionsList);
					for (const key of Object.keys(subject)) {
						wrapped[key];
					}
					for (const options of optionsList) {
						wrapped[options.property];
					}
				} catch (_) {}

				ppTestKit.check(...optionsList);
			},
		),
	);
});

test("no missing properties are accessed by withPollution", () => {
	fc.assert(
		fc.property(
			fc.array(arbitrary.options().map(ppTestKit.wrap)),
			(optionsList) => {
				try {
					withPollution(() => {}, ...optionsList);
				} catch (_) {}

				ppTestKit.check(...optionsList);
			},
		),
	);
});
