// SPDX-License-Identifier: ISC

import { test } from "node:test";

import * as fc from "fast-check";
import * as ppTestKit from "../../src/manual.js";

import { test as invariantTest } from "../../src/invariant.js";

test("no missing properties are accessed by the invariant API", () => {
	fc.assert(
		fc.property(
			fc.object(),
			fc
				.record(
					{
						strict: fc.boolean(),
					},
					{ requiredKeys: [] },
				)
				.map(ppTestKit.wrap),
			(subject, options) => {
				invariantTest((ctx) => {
					ctx.wrap(subject, options);
				});

				ppTestKit.check(options);
			},
		),
	);
});
