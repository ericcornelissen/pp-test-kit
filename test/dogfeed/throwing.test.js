// SPDX-License-Identifier: ISC

import { test } from "node:test";

import * as fc from "fast-check";
import * as ppTestKit from "../../src/manual.js";

import * as throwing from "../../src/throwing.js";

test("no missing properties are accessed by the throwing API", () => {
	fc.assert(
		fc.property(
			fc.object(),
			fc
				.record(
					{
						strict: fc.boolean(),
					},
					{ requiredKeys: [] },
				)
				.map(ppTestKit.wrap),
			(subject, options) => {
				throwing.wrap(subject, ppTestKit.wrap(options));
			},
		),
	);
});
