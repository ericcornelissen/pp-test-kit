// SPDX-License-Identifier: ISC

import * as assert from "node:assert/strict";
import { test } from "node:test";

import escapeStringRegexp from "escape-string-regexp";
import * as fc from "fast-check";

import * as testCases from "./testcases/all.js";
import * as helpers from "./helpers.js";

import * as lib from "../src/throwing.js";

const arbitrary = {
	subject: () =>
		fc.anything({
			values: [fc.constant({})],
			withBigInt: false,
			withBoxedValues: true,
			withDate: true,
			withMap: true,
			withSet: true,
			withTypedArray: true,
			withSparseArray: true,
		}),
	options: () =>
		fc.record(
			{
				strict: fc.boolean(),
			},
			{
				requiredKeys: [],
			},
		),
};

test("throwing API", async (t) => {
	await t.test("property missing", async (t) => {
		for (const testCase of testCases.missing) {
			const { testName } = testCase;

			await t.test(`${testName} (default)`, () => {
				notOk(testCase);
			});

			await t.test(`${testName} (not strict)`, () => {
				notOk(testCase, { strict: false });
			});

			await t.test(`${testName} (strict)`, () => {
				notOk(testCase, { strict: true });
			});
		}
	});

	await t.test("property present", async (t) => {
		for (const testCase of testCases.present) {
			const { testName } = testCase;

			await t.test(`${testName} (default)`, () => {
				ok(testCase);
			});

			await t.test(`${testName} (not strict)`, () => {
				ok(testCase, { strict: false });
			});

			await t.test(`${testName} (strict)`, () => {
				ok(testCase, { strict: true });
			});
		}
	});

	await t.test("property inherited", async (t) => {
		for (const testCase of testCases.inherited) {
			const { testName } = testCase;

			await t.test(`${testName} (default)`, () => {
				ok(testCase);
			});

			await t.test(`${testName} (not strict)`, () => {
				ok(testCase, { strict: false });
			});

			await t.test(`${testName} (strict)`, () => {
				notOk(testCase, { strict: true });
			});
		}
	});

	await t.test("property shadowed", async (t) => {
		for (const testCase of testCases.shadowed) {
			const { testName } = testCase;

			await t.test(`${testName} (default)`, () => {
				ok(testCase);
			});

			await t.test(`${testName} (not strict)`, () => {
				ok(testCase, { strict: false });
			});

			await t.test(`${testName} (strict)`, () => {
				ok(testCase, { strict: true });
			});
		}
	});

	await t.test("for...in", () => {
		fc.assert(
			fc.property(
				arbitrary.subject(),
				arbitrary.options(),
				(subject, options) => {
					const wrappedSubject = lib.wrap(subject, options);

					assert.throws(
						() => {
							for (const _ in wrappedSubject) {
								// Nothing to do
							}
						},
						(error) => {
							assert.ok(error instanceof assert.AssertionError);
							assert.match(error.message, /for\.\.\.in \./u);

							return true;
						},
					);
				},
			),
		);
	});

	await t.test("null prototype", () => {
		fc.assert(
			fc.property(
				arbitrary
					.subject()
					.map((obj) => Object.assign(Object.create(null), obj)),
				arbitrary.options(),
				fc.string(),
				(subject, options, property) => {
					const wrappedSubject = lib.wrap(subject, options);

					assert.doesNotThrow(() => {
						wrappedSubject[property];
					});
				},
			),
		);
	});

	await t.test("accessing __proto__", () => {
		fc.assert(
			fc.property(
				arbitrary.subject(),
				arbitrary.options(),
				(subject, options) => {
					const wrappedSubject = lib.wrap(subject, options);

					assert.doesNotThrow(() => {
						wrappedSubject.__proto__;
					});
				},
			),
		);
	});

	await t.test("nested access", () => {
		const subject = { foo: {} };
		const wrappedSubject = lib.wrap(subject);

		assert.throws(
			() => {
				wrappedSubject.foo.bar;
			},
			(error) => {
				assert.ok(error instanceof assert.AssertionError);
				assert.match(error.message, /foo\.bar \(.+?\)/u);

				return true;
			},
		);
	});

	await t.test("call", () => {
		fc.assert(
			fc.property(
				arbitrary.options(),
				fc.array(fc.constant({}), { minLength: 1, maxLength: 42 }),
				(options, args) => {
					const subject = (...args) => args[args.length - 1].foo;
					const wrappedSubject = lib.wrap(subject, options);

					assert.throws(
						() => {
							wrappedSubject(...args);
						},
						(error) => {
							assert.ok(error instanceof assert.AssertionError);

							const location = escapeStringRegexp(import.meta.url);
							assert.match(
								error.message,
								new RegExp(
									`(.+?)\.foo \\(at ${location}:\\d+:\\d+\\)(\n|$)`,
									"u",
								),
							);

							return true;
						},
					);
				},
			),
		);
	});

	await t.test("construct", () => {
		fc.assert(
			fc.property(
				arbitrary.options(),
				fc.array(fc.constant({}), { minLength: 1, maxLength: 42 }),
				(options, args) => {
					class subject {
						constructor(...args) {
							args[args.length - 1].foo;
						}
					}
					const wrappedSubject = lib.wrap(subject, options);

					assert.throws(
						() => {
							new wrappedSubject(...args);
						},
						(error) => {
							assert.ok(error instanceof assert.AssertionError);

							const location = escapeStringRegexp(import.meta.url);
							assert.match(
								error.message,
								new RegExp(
									`(.+?)\.foo \\(at ${location}:\\d+:\\d+\\)(\n|$)`,
									"u",
								),
							);

							return true;
						},
					);
				},
			),
		);
	});

	await t.test("subject unsupported", async (t) => {
		for (const testCase of testCases.unsupported) {
			const { subject, testName } = testCase;

			await t.test(testName, () => {
				assert.throws(
					() => {
						lib.wrap(subject);
					},
					{
						name: "TypeError",
						message:
							"Cannot create proxy with a non-object as target or handler",
					},
				);
			});
		}
	});
});

/** @typedef {import("../src/wrapper.js").Options} Options */
/** @typedef {import("./testcases/all.js").TestCase} TestCase */

/**
 * Test that the throwing API does not find a problem for the given test case.
 *
 * @param {TestCase} testCase The test case to evaluate.
 * @param {Options} options The options object to provide to the API.
 */
function ok(testCase, options) {
	const { accesses, subject } = testCase;
	const wrappedSubject = lib.wrap(subject, options);

	assert.doesNotThrow(() => {
		helpers.access(wrappedSubject, accesses);
	});
}

/**
 * Test that the throwing API does find a problem for the given test case.
 *
 * @param {TestCase} testCase The test case to evaluate.
 * @param {Options} options The options object to provide to the API.
 */
function notOk(testCase, options) {
	const { accesses, expected, subject } = testCase;
	const wrappedSubject = lib.wrap(subject, options);

	assert.throws(
		() => {
			helpers.access(wrappedSubject, accesses);
		},
		(error) => {
			assert.ok(error instanceof assert.AssertionError);

			const property = expected[0].toString();
			const name = escapeStringRegexp(property);
			const location = escapeStringRegexp(helpers.access.location);
			assert.match(
				error.message,
				new RegExp(`${name} \\(at ${location}:\\d+:\\d+\\)`, "u"),
			);

			return true;
		},
	);
}
