/**
 * Perform a list of recursive accesses on a given object.
 *
 * @param {any} subject The object to access properties on.
 * @param {any[]} accesses The accesses the perform on the subject.
 */
export function access(subject, accesses) {
	for (let access of accesses) {
		if (!Array.isArray(access)) {
			access = [access];
		}

		let tmp = subject;
		for (const property of access) {
			tmp = tmp[property];
		}
	}
}

access.location = import.meta.url;
