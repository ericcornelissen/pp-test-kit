// SPDX-License-Identifier: ISC

import * as assert from "node:assert/strict";
import { test } from "node:test";

import escapeStringRegexp from "escape-string-regexp";
import * as fc from "fast-check";

import * as testCases from "./testcases/all.js";
import * as helpers from "./helpers.js";

import * as lib from "../src/manual.js";

const arbitrary = {
	subject: () =>
		fc.anything({
			values: [fc.constant({})],
			withBigInt: false,
			withBoxedValues: true,
			withDate: true,
			withMap: true,
			withSet: true,
			withTypedArray: true,
			withSparseArray: true,
		}),
	options: () =>
		fc.record(
			{
				strict: fc.boolean(),
			},
			{
				requiredKeys: [],
			},
		),
};

test("manual API", async (t) => {
	await t.test("property missing", async (t) => {
		for (const testCase of testCases.missing) {
			const { testName } = testCase;

			await t.test(`${testName} (default)`, () => {
				notOk(testCase);
			});

			await t.test(`${testName} (not strict)`, () => {
				notOk(testCase, { strict: false });
			});

			await t.test(`${testName} (strict)`, () => {
				notOk(testCase, { strict: true });
			});
		}
	});

	await t.test("property present", async (t) => {
		for (const testCase of testCases.present) {
			const { testName } = testCase;

			await t.test(`${testName} (default)`, () => {
				ok(testCase);
			});

			await t.test(`${testName} (not strict)`, () => {
				ok(testCase, { strict: false });
			});

			await t.test(`${testName} (strict)`, () => {
				ok(testCase, { strict: true });
			});
		}
	});

	await t.test("property inherited", async (t) => {
		for (const testCase of testCases.inherited) {
			const { testName } = testCase;

			await t.test(`${testName} (default)`, () => {
				ok(testCase);
			});

			await t.test(`${testName} (not strict)`, () => {
				ok(testCase, { strict: false });
			});

			await t.test(`${testName} (strict)`, () => {
				notOk(testCase, { strict: true });
			});
		}
	});

	await t.test("property shadowed", async (t) => {
		for (const testCase of testCases.shadowed) {
			const { testName } = testCase;

			await t.test(`${testName} (default)`, () => {
				ok(testCase);
			});

			await t.test(`${testName} (not strict)`, () => {
				ok(testCase, { strict: false });
			});

			await t.test(`${testName} (strict)`, () => {
				ok(testCase, { strict: true });
			});
		}
	});

	await t.test("for...in", () => {
		fc.assert(
			fc.property(
				arbitrary.subject(),
				arbitrary.options(),
				(subject, options) => {
					const wrappedSubject = lib.wrap(subject, options);
					for (const _ in wrappedSubject) {
						// Nothing to do
					}

					assert.throws(
						() => {
							lib.check(wrappedSubject);
						},
						(error) => {
							assert.ok(error instanceof assert.AssertionError);
							assert.match(error.message, /for\.\.\.in \./u);

							return true;
						},
					);
				},
			),
		);
	});

	await t.test("null prototype", () => {
		fc.assert(
			fc.property(
				arbitrary
					.subject()
					.map((obj) => Object.assign(Object.create(null), obj)),
				arbitrary.options(),
				fc.string(),
				(subject, options, property) => {
					const wrappedSubject = lib.wrap(subject, options);
					wrappedSubject[property];

					assert.doesNotThrow(() => {
						lib.check(wrappedSubject);
					});
				},
			),
		);
	});

	await t.test("accessing __proto__", () => {
		fc.assert(
			fc.property(
				arbitrary.subject(),
				arbitrary.options(),
				(subject, options) => {
					const wrappedSubject = lib.wrap(subject, options);
					wrappedSubject.__proto__;

					assert.doesNotThrow(() => {
						lib.check(wrappedSubject);
					});
				},
			),
		);
	});

	await t.test("same property", async (t) => {
		await t.test("same location", () => {
			const wrappedSubject = lib.wrap({});

			for (const _ in [1, 2]) {
				wrappedSubject.foo;
			}

			assert.throws(
				() => {
					lib.check(wrappedSubject);
				},
				(error) => {
					assert.ok(error instanceof assert.AssertionError);
					assert.match(
						error.message,
						/Missing property accesses detected \(1\):/u,
					);

					return true;
				},
			);
		});

		await t.test("different location", () => {
			const wrappedSubject = lib.wrap({});

			wrappedSubject.foo;
			wrappedSubject.foo;

			assert.throws(
				() => {
					lib.check(wrappedSubject);
				},
				(error) => {
					assert.ok(error instanceof assert.AssertionError);
					assert.match(
						error.message,
						/Missing property accesses detected \(2\):/u,
					);

					return true;
				},
			);
		});
	});

	await t.test("nested access", () => {
		const subject = { foo: {} };
		const wrappedSubject = lib.wrap(subject);

		wrappedSubject.foo.bar;

		assert.throws(
			() => {
				lib.check(wrappedSubject);
			},
			(error) => {
				assert.ok(error instanceof assert.AssertionError);
				assert.match(error.message, /foo\.bar \(.+?\)/u);

				return true;
			},
		);
	});

	await t.test("call", () => {
		fc.assert(
			fc.property(
				arbitrary.options(),
				fc.array(fc.constant({}), { minLength: 1, maxLength: 42 }),
				(options, args) => {
					const subject = (...args) => args[args.length - 1].foo;
					const wrappedSubject = lib.wrap(subject, options);

					wrappedSubject(...args);

					assert.throws(
						() => {
							lib.check(wrappedSubject);
						},
						(error) => {
							assert.ok(error instanceof assert.AssertionError);

							const location = escapeStringRegexp(import.meta.url);
							assert.match(
								error.message,
								new RegExp(
									`(.+?)\.foo \\(at ${location}:\\d+:\\d+\\)(\n|$)`,
									"u",
								),
							);

							return true;
						},
					);
				},
			),
		);
	});

	await t.test("construct", () => {
		fc.assert(
			fc.property(
				arbitrary.options(),
				fc.array(fc.constant({}), { minLength: 1, maxLength: 42 }),
				(options, args) => {
					class subject {
						constructor(...args) {
							args[args.length - 1].foo;
						}
					}
					const wrappedSubject = lib.wrap(subject, options);

					new wrappedSubject(...args);

					assert.throws(
						() => {
							lib.check(wrappedSubject);
						},
						(error) => {
							assert.ok(error instanceof assert.AssertionError);

							const location = escapeStringRegexp(import.meta.url);
							assert.match(
								error.message,
								new RegExp(
									`(.+?)\.foo \\(at ${location}:\\d+:\\d+\\)(\n|$)`,
									"u",
								),
							);

							return true;
						},
					);
				},
			),
		);
	});

	await t.test("incorrect usage", async (t) => {
		await t.test("subject unsupported", async (t) => {
			for (const testCase of testCases.unsupported) {
				const { subject, testName } = testCase;

				await t.test(testName, () => {
					assert.throws(
						() => {
							lib.wrap(subject);
						},
						{
							name: "TypeError",
							message:
								"Cannot create proxy with a non-object as target or handler",
						},
					);
				});
			}
		});

		await t.test("checking a wrapped object twice", () => {
			const wrapped = lib.wrap({});
			lib.check(wrapped);

			assert.throws(
				() => {
					lib.check(wrapped);
				},
				{
					name: "AssertionError",
					message: "No entry found for object",
				},
			);
		});

		await t.test("checking a non-wrapped object", () => {
			assert.throws(
				() => {
					lib.check({});
				},
				{
					name: "AssertionError",
					message: "No entry found for object",
				},
			);
		});

		await t.test("checking the unwrapped object", () => {
			const subject = {};
			lib.wrap(subject);

			assert.throws(
				() => {
					lib.check(subject);
				},
				{
					name: "AssertionError",
					message: "No entry found for object",
				},
			);
		});
	});

	await t.test("check() edge cases", async (t) => {
		await t.test("check after wrapped subject is modified", () => {
			const subject = {};
			const wrappedSubject = lib.wrap(subject);

			wrappedSubject.foo = "bar";

			assert.doesNotThrow(() => {
				lib.check(wrappedSubject);
			});
		});

		await t.test("check after original subject is modified", () => {
			const subject = {};
			const wrappedSubject = lib.wrap(subject);

			subject.foo = "bar";

			assert.doesNotThrow(() => {
				lib.check(wrappedSubject);
			});
		});

		await t.test("multiple wrappers", async (t) => {
			await t.test("access on A, check B", () => {
				const subjectA = {};
				const wrappedSubjectA = lib.wrap(subjectA);

				const subjectB = {};
				const wrappedSubjectB = lib.wrap(subjectB);

				wrappedSubjectA.foo;

				assert.doesNotThrow(() => {
					lib.check(wrappedSubjectB);
				});
			});

			await t.test("access on B, check A", () => {
				const subjectA = {};
				const wrappedSubjectA = lib.wrap(subjectA);

				const subjectB = {};
				const wrappedSubjectB = lib.wrap(subjectB);

				wrappedSubjectB.foo;

				assert.doesNotThrow(() => {
					lib.check(wrappedSubjectA);
				});
			});
		});
	});
});

/** @typedef {import("../src/wrapper.js").Options} Options */
/** @typedef {import("./testcases/all.js").TestCase} TestCase */

/**
 * Test that the manual API does not find a problem for the given test case.
 *
 * @param {TestCase} testCase The test case to evaluate.
 * @param {Options} options The options object to provide to the API.
 */
function ok(testCase, options) {
	const { accesses, subject } = testCase;

	const wrappedSubject = lib.wrap(subject, options);
	helpers.access(wrappedSubject, accesses);

	assert.doesNotThrow(() => {
		lib.check(wrappedSubject);
	});
}

/**
 * Test that the manual API does find a problem for the given test case.
 *
 * @param {TestCase} testCase The test case to evaluate.
 * @param {Options} options The options object to provide to the API.
 */
function notOk(testCase, options) {
	const { accesses, expected, subject } = testCase;

	const wrappedSubject = lib.wrap(subject, options);
	helpers.access(wrappedSubject, accesses);

	assert.throws(
		() => {
			lib.check(wrappedSubject);
		},
		(error) => {
			assert.ok(error instanceof assert.AssertionError);

			for (const property of expected) {
				const name = escapeStringRegexp(property.toString());
				const location = escapeStringRegexp(helpers.access.location);
				assert.match(
					error.message,
					new RegExp(`${name} \\(at ${location}:\\d+:\\d+\\)(\n|$)`, "u"),
				);
			}

			return true;
		},
	);
}
