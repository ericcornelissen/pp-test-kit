// SPDX-License-Identifier: MIT-0

import * as assert from "node:assert";
import { test } from "node:test";

import * as ppTestKit from "../../src/simulate.js";

import { insecure, secure } from "../lib.js";

// This first test is expected to fail because it will find that the function
// under tests behaves differently when the 'introduce' property is polluted.
test("the insecure function does not survive simulated pollution", () => {
	const options = {};

	// Simulate pollution of a specific property on an input to the function under
	// test.
	const simulatedOptions = ppTestKit.simulatePollution(options, {
		property: "introduce",
		value: true,
	});

	// Call the function under test with the object on which prototype pollution
	// is simulated.
	const result = insecure(simulatedOptions);

	// Assert that the function behaves as it would without pollution.
	assert.equal(result, insecure(options));
});

// This next test is identical to the one above except that function under test
// is secure.
test("the secure function does survive simulated pollution", () => {
	const options = {};
	const simulatedOptions = ppTestKit.simulatePollution(options, {
		property: "introduce",
		value: true,
	});

	const result = secure(simulatedOptions);
	assert.equal(result, secure(options));
});
