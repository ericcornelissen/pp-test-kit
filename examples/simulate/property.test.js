// SPDX-License-Identifier: MIT-0

import * as assert from "node:assert";
import { test } from "node:test";

import * as fc from "fast-check";
import * as ppTestKit from "../../src/simulate.js";

import { insecure, secure } from "../lib.js";

// This first test is expected to fail because it will find that the function
// under tests may try to access properties not defined on the object it is
// given.
test("the insecure function does not survive simulated pollution", () => {
	fc.assert(
		fc.property(
			// It is best to tell fast-check how to generate objects for the function
			// under test so that it can generate as many variants as possible.
			fc.record(
				{
					// The structure for the records to create.
					introduce: fc.boolean(),
					quote: fc.boolean(),
				},
				{
					// Allow records with some of the properties missing.
					requiredKeys: [],
				},
			),

			// Similarly, it is best to tell fast-check what properties to pollute.
			fc.oneof(fc.constant("introduce"), fc.constant("quote")),

			// It is also best to use random values because this can help find cases
			// where the value of the pollution matters.
			fc.boolean(),

			(options, property, value) => {
				// Simulate pollution of a random property on an argument to the
				// function under test.
				const simulatedOptions = ppTestKit.simulatePollution(options, {
					property,
					value,
				});

				// Call the function under test with the object on which prototype
				// pollution is simulated.
				const result = insecure(simulatedOptions);

				// Assert that the function behaves as it would without pollution.
				assert.equal(result, insecure(options));
			},
		),
	);
});

// This next test is identical to the one above except that function under test
// is secure.
test("the secure function accesses no missing properties", () => {
	fc.assert(
		fc.property(
			fc.record(
				{
					introduce: fc.boolean(),
					quote: fc.boolean(),
				},
				{
					requiredKeys: [],
				},
			),
			fc.oneof(fc.constant("introduce"), fc.constant("quote")),
			fc.boolean(),

			(options, property, value) => {
				const simulatedOptions = ppTestKit.simulatePollution(options, {
					property,
					value,
				});
				const result = secure(simulatedOptions);
				assert.equal(result, secure(options));
			},
		),
	);
});
