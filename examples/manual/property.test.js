// SPDX-License-Identifier: MIT-0

import { test } from "node:test";

import * as fc from "fast-check";
import * as ppTestKit from "../../src/manual.js";

import { insecure, secure } from "../lib.js";

test("function-based", async (t) => {
	// This first test is expected to fail because it will find that the function
	// under tests may try to access properties not defined on the object it is
	// given.
	await t.test("the insecure function does access missing properties", () => {
		fc.assert(
			fc.property(
				// It is best to tell fast-check how to generate objects for the function
				// under test so that it can generate as many variants as possible.
				fc.record(
					{
						// The structure for the records to create.
						introduce: fc.boolean(),
						quote: fc.boolean(),
					},
					{
						// Allow records with some of the properties missing.
						requiredKeys: [],
					},
				),
				(options) => {
					// Wrap the function under test using the test kit so that any missing
					// property accesses on any of its arguments is detected.
					const fut = ppTestKit.wrap(insecure);

					// Call the wrapped function under test with any arguments so that missing
					// property accesses will be detected.
					fut(options);

					// Assert no missing property lookups occurred. Since the function under
					// test is insecure this will throw an error for this test.
					ppTestKit.check(fut);
				},
			),
		);
	});

	// This next test is identical to the one above except that function under test
	// is secure.
	await t.test("the secure function accesses no missing properties", () => {
		fc.assert(
			fc.property(
				// It is best to tell fast-check how to generate objects for the function
				// under test so that it can generate as many variants as possible.
				fc.record(
					{
						// The structure for the records to create.
						introduce: fc.boolean(),
						quote: fc.boolean(),
					},
					{
						// Allow records with some of the properties missing.
						requiredKeys: [],
					},
				),
				(options) => {
					// Wrap the function under test using the test kit so that any missing
					// property accesses on any of its arguments is detected.
					const fut = ppTestKit.wrap(secure);

					// Call the wrapped function under test with any arguments so that missing
					// property accesses will be detected.
					fut(options);

					// Assert no missing property lookups occurred. Since the function under
					// test is insecure this will throw an error for this test.
					ppTestKit.check(fut);
				},
			),
		);
	});
});

test("function-based", async (t) => {
	// This first test is expected to fail because it will find that the function
	// under tests may try to access properties not defined on the object it is
	// given.
	await t.test("the insecure function does access missing properties", () => {
		fc.assert(
			fc.property(
				// It is best to tell fast-check how to generate objects for the function
				// under test so that it can generate as many variants as possible.
				fc
					.record(
						{
							// The structure for the records to create.
							introduce: fc.boolean(),
							quote: fc.boolean(),
						},
						{
							// Allow records with some of the properties missing.
							requiredKeys: [],
						},
					)
					// All generated records should be wrapped by the test kit.
					.map(ppTestKit.wrap),
				(options) => {
					// Call the function under test with the wrapped object so that missing
					// property accesses may be detected.
					insecure(options);

					// Assert no missing property lookups occurred. Since the function under
					// test is insecure this will throw an error for this test.
					ppTestKit.check(options);
				},
			),
		);
	});

	// This next test is identical to the one above except that function under test
	// is secure.
	await t.test("the secure function accesses no missing properties", () => {
		fc.assert(
			fc.property(
				fc
					.record(
						{ introduce: fc.boolean(), quote: fc.boolean() },
						{ requiredKeys: [] },
					)
					.map(ppTestKit.wrap),
				(options) => {
					secure(options);
					ppTestKit.check(options);
				},
			),
		);
	});
});
