// SPDX-License-Identifier: MIT-0

import { test } from "node:test";

import * as ppTestKit from "../../src/throwing.js";

import { insecure, secure } from "../lib.js";

test("function-based", async (t) => {
	// This first test is expected to fail because it will find that the function
	// under tests may try to access properties not defined on the object it is
	// given.
	await t.test("the insecure function does access missing properties", () => {
		// Wrap the function under test using the test kit so that any missing
		// property accesses on any of its arguments is detected.
		const fut = ppTestKit.wrap(insecure);

		// Call the wrapped function under test with any arguments so that missing
		// property accesses will be reported. Since the function under test is
		// insecure this will throw an error for this test.
		fut({});
	});

	// This next test is identical to the one above except that function under
	// test is secure.
	await t.test("the secure function accesses no missing properties", () => {
		const fut = ppTestKit.wrap(secure);
		fut({});
	});
});

test("argument-based", async (t) => {
	// This first test is expected to fail because it will find that the function
	// under tests may try to access properties not defined on the object it is
	// given.
	await t.test("the insecure function does access missing properties", () => {
		// Wrap the options object using the test kit so that any missing property
		// accesses on the option are detected.
		const options = ppTestKit.wrap({});

		// Call the function under test with the wrapped object so that missing
		// property accesses will be reported. Since the function under test is
		// insecure this will throw an error for this test.
		insecure(options);
	});

	// This next test is identical to the one above except that function under
	// test is secure.
	await t.test("the secure function accesses no missing properties", () => {
		const options = ppTestKit.wrap({});
		secure(options);
	});
});
