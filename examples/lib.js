// SPDX-License-Identifier: MIT-0

/*

This file provides a fictional API for the purposes of demonstrating the
pp-test-kit functionality on test suites.

*/

/**
 * Create a string. This function's behavior may be changed through prototype
 * pollution.
 *
 * @param {object?} options The options for this function.
 * @returns {string} The string.
 */
export function insecure(options) {
	let string = "foobar";

	if (options.introduce) {
		string = `The string is: ${string}`;

		if (options.quote) {
			string = `'${string}'`;
		}
	}

	return string;
}

/**
 * Create a string.
 *
 * @param {object?} options The options for this function.
 * @returns {string} The string.
 */
export function secure(options) {
	let string = "foobar";

	if (options && Object.hasOwn(options, "introduce") && options.introduce) {
		string = `The string is: ${string}`;

		if (options && Object.hasOwn(options, "quote") && options.quote) {
			string = `'${string}'`;
		}
	}

	return string;
}
