// SPDX-License-Identifier: MIT-0

import * as assert from "node:assert";
import { test } from "node:test";

import * as ppTestKit from "../../src/invariant.js";

import { insecure, secure } from "../lib.js";

test("the invariant does not hold for the insecure function", () => {
	// Run everything in a callback to the test kit's test function.
	ppTestKit.test((ctx) => {
		// Wrap the options object using the context provided to the callback. This
		// will both serve to monitor it for missing property accesses as well as
		// simulate pollution.
		const options = ctx.wrap({});

		// Call the function under test with the wrapped object so that missing
		// property accesses can be detected and running in a polluted context can
		// be simulated.
		const result = insecure(options);

		// Assert an invariant on the result of the function that should hold
		// regardless of whether or not pollution occurred.
		assert.equal(result, "foobar");
	});
});

// This next test is identical to the one above except that function under test
// is secure.
test("the invariant does hold for the secure function", () => {
	ppTestKit.test((ctx) => {
		const options = ctx.wrap({});
		const result = secure(options);
		assert.equal(result, "foobar");
	});
});
