// Configuration file for StrykerJS (https://stryker-mutator.io/)

export default {
	coverageAnalysis: "perTest",
	inPlace: false,
	timeoutMS: 10000,

	mutate: ["src/**/*.js"],

	testRunner: "tap",
	tap: {
		testFiles: ["test/*.test.js"],
		forceBail: true,
	},

	incremental: true,
	incrementalFile: ".cache/stryker-incremental.json",

	reporters: ["clear-text", "html", "progress"],
	htmlReporter: {
		fileName: "_reports/mutation/index.html",
	},

	thresholds: {
		high: 100,
		low: 100,
		break: 100,
	},

	tempDirName: ".temp/stryker",
	cleanTempDir: true,
};
